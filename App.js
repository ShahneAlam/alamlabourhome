import React, {useEffect} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
} from 'react-native';
import StackNavigator from './src/Navigator/StackNavigator';
import {Provider} from 'react-redux';
import store from './src/redux/store';
import PushNotification from 'react-native-push-notification';
import NetInfo from '@react-native-community/netinfo';
//import * as actions from './src/redux/actions';
import * as actions from './src/redux/actions';
import DeviceInfo from 'react-native-device-info';

const App = () => {
  const pushNotificationMessage = ({title, message}) =>
    PushNotification.localNotification({
      title,
      message,
    });
  useEffect(() => {
    //  alert(JSON.stringify(DeviceInfo));
    //   console.log(JSON.stringify(DeviceInfo, null, 2));
    const netInfoSubscribe = NetInfo.addEventListener(state =>
      store.dispatch(actions.SetNetInfo(state)),
    );
    PushNotification.configure({
      onRegister: ({token}) => {
        //  console.log(token);
        store.dispatch(
          actions.SetDeviceInfo({
            id: DeviceInfo.getDeviceId(),
            token: token.toString(),
            model: DeviceInfo.getModel(),
            os: Platform.OS,
            // device_token: DeviceInfo.getDeviceToken,
          }),
        );
      },

      onNotification: notification => {
        console.log('notification');
        EventRegister.emit('myCustomEvent', 'it works!!!');
        //  console.log(JSON.stringify(notification, null, 2));
        pushNotificationMessage(notification);
        const {data = {}} = notification;
        const {type = ''} = data;
        // if (type === 'bookingcancel') {
        //   alert(notification.message);
        // }
      },
      onAction: function (notification) {
        console.log('ACTION:', notification.action);
        console.log('NOTIFICATION:', notification);

        // process the action
      },
      onRegistrationError: function (err) {
        console.error(err.message, err);
      },
      permissions: {
        alert: true,
        badge: true,
        sound: true,
      },
      senderID: '893170431923',
      popInitialNotification: true,
      requestPermissions: true,
    });

    PushNotification.localNotificationSchedule({
      //... You can use all the options from localNotifications
      message: 'My Notification Message', // (required)
      date: new Date(Date.now() + 60 * 1000), // in 60 secs
      allowWhileIdle: false, // (optional) set notification to work while on doze, default: false
    });
    return () => {
      netInfoSubscribe();
    };
  }, []);
  return (
    <Provider store={store}>
      <StackNavigator />
    </Provider>
  );
};

export default App;
