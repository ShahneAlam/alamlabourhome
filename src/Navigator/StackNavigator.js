import React, {useRef} from 'react';
import {
  CardStyleInterpolators,
  createStackNavigator,
} from '@react-navigation/stack';
import {NavigationContainer} from '@react-navigation/native';
import TabNavigator from './TabNavigator';
import Splash from '../screens/Splash';
import Language from '../screens/AuthScreen/Language';
import OnBoarding from '../screens/AuthScreen/OnBoarding';
import Login from '../screens/AuthScreen/Login';
import Otp from '../screens/AuthScreen/Otp';
import Ragister from '../screens/AuthScreen/Ragister';
import Home from '../screens/Home';
import CallList from '../screens/CallList';
import Details from '../screens/Details';
import Success from '../screens/Success';
import MyBooking from '../screens/MyBooking';
import Rating from '../screens/Worker/Rating';
import Subscription from '../screens/Subscription';
import SubscriptionPlan from '../screens/SubscriptionPlan';
import Payment from '../screens/Payment';
import PaymentDone from '../screens/PaymentDone';
import PostJob from '../screens/PostJob';
import Success2 from '../screens/Success2';
import MyAccount from '../screens/MyAccount';
import MyJobs from '../screens/MyJobs';
import XyzJob from '../screens/XyzJob';
import SelectTrade from '../screens/Worker/SelectTrade';
import AddAddress from '../screens/Worker/AddAddress';
import WorkerHome from '../screens/Worker/WorkerHome';
import DailyJob from '../screens/DailyJob';
import WorkerMyBooking from '../screens/Worker/WorkerMyBooking';
import WorkerProfile from '../screens/Worker/WorkerProfile';
import WorkerMyAccount from '../screens/Worker/WorkerMyAccount';
import WorkerEditProfile from '../screens/Worker/WorkerEditProfile';
import WorkerHelp from '../screens/Worker/WorkerHelp';
import Plan from '../screens/Plan';
import VideoTutorial from '../screens/VideoTutorial';
import Ragister2 from '../screens/Ragister2';
import RegisterContractor from '../screens/Contractor/RegisterContractor';
import HomeContractor from '../screens/Contractor/HomeContractor';
import PaymentContractor from '../screens/Contractor/PaymentContractor';
import CategoriesContractor from '../screens/Contractor/CategoriesContractor';
import jobDetailsContractor from '../screens/Contractor/JobDetailsContractor';
import OfferMadeContractor from '../screens/Contractor/OfferMadeContractor';
import OfferMadeContractorDetails from '../screens/Contractor/OfferMadeContractorDetails';
import MyProjectsContractor from '../screens/Contractor/MyProjectsContractor';
import ProjectsCompletedDetailsConstractor from '../screens/Contractor/ProjectsCompletedDetailsConstractor';
import MyProjectsContractorXYZJobs from '../screens/Contractor/MyProjectsContractorXYZJobs';
import RatingContractor from '../screens/Contractor/RatingContractor';
import MyAccountContractor from '../screens/Contractor/MyAccountContractor';
import TermsContractor from '../screens/Contractor/TermsContractor';
import PrivacyContractor from '../screens/Contractor/PrivacyContractor';
import HelpContractor from '../screens/Contractor/HelpContractor';
import MyProfileContractor from '../screens/Contractor/MyProfileContractor';
import SubscriptionPlanContractor from '../screens/Contractor/SubscriptionPlanContractor';
import PaymentContractorSubscriptionPlan from '../screens/Contractor/PaymentContractorSubscriptionPlan';
import PostJobContractor from '../screens/Contractor/PostJobContractor';
import SuccessContractor from '../screens/Contractor/SuccessContractor';
import ReviewJobContractor from '../screens/Contractor/ReviewJobContractor';
import ProjectTypeSearchContractor from '../screens/Contractor/ProjectTypeSearchContractor';
import SelectUserTypeContractor from '../screens/Contractor/SelectUserTypeContractor';
import SelectTradeContractor from '../screens/Contractor/SelectTradeContractor';
import AutoCompleteGoogle from '../screens/Worker/AutoCompleteGoogle';
import DailyJobViewAll from '../screens/Worker/DailyJobViewAll';
import ProfileData from '../screens/Worker/ProfileData';
import UserRegisterData from '../screens/UserRegisterData';
import WorkerPrivacy from '../screens/Worker/WorkerPrivacy';
import WorkerTerm from '../screens/Worker/WorkerTerm';
import FilterUserScreen from '../screens/FilterUserScreen';
import MyProfileUser from '../screens/MyProfileUser';
import UserPrivacy from '../screens/UserPrivacy';
import UserHelp from '../screens/UserHelp';

const Stack = createStackNavigator();

const StackNavigator = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator
        initialRouteName={'Splash'}
        screenOptions={{headerShown: false}}>
        <Stack.Screen
          name="Splash"
          component={Splash}
          options={{
            cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
          }}
        />
        <Stack.Screen
          name="Language"
          component={Language}
          options={{
            cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
          }}
        />
        <Stack.Screen
          name="OnBoarding"
          component={OnBoarding}
          options={{
            cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
          }}
        />
        <Stack.Screen
          name="Login"
          component={Login}
          options={{
            cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
          }}
        />
        <Stack.Screen
          name="Otp"
          component={Otp}
          options={{
            cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
          }}
        />
        <Stack.Screen
          name="Ragister"
          component={Ragister}
          options={{
            cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
          }}
        />
        <Stack.Screen
          name="Home"
          component={Home}
          options={{
            cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
          }}
        />
        <Stack.Screen
          name="CallList"
          component={CallList}
          options={{
            cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
          }}
        />
        <Stack.Screen
          name="Details"
          component={Details}
          options={{
            cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
          }}
        />
        <Stack.Screen
          name="Success"
          component={Success}
          options={{
            cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
          }}
        />
        <Stack.Screen
          name="MyBooking"
          component={MyBooking}
          options={{
            cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
          }}
        />
        <Stack.Screen
          name="Rating"
          component={Rating}
          options={{
            cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
          }}
        />
        <Stack.Screen
          name="Subscription"
          component={Subscription}
          options={{
            cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
          }}
        />
        <Stack.Screen
          name="SubscriptionPlan"
          component={SubscriptionPlan}
          options={{
            cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
          }}
        />
        <Stack.Screen
          name="Payment"
          component={Payment}
          options={{
            cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
          }}
        />
        <Stack.Screen
          name="PaymentDone"
          component={PaymentDone}
          options={{
            cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
          }}
        />
        <Stack.Screen
          name="PostJob"
          component={PostJob}
          options={{
            cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
          }}
        />
        <Stack.Screen
          name="Success2"
          component={Success2}
          options={{
            cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
          }}
        />
        <Stack.Screen
          name="MyAccount"
          component={MyAccount}
          options={{
            cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
          }}
        />
        <Stack.Screen
          name="TabNavigator"
          component={TabNavigator}
          options={{
            cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
          }}
        />
        <Stack.Screen
          name="MyJobs"
          component={MyJobs}
          options={{
            cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
          }}
        />
        <Stack.Screen
          name="XyzJob"
          component={XyzJob}
          options={{
            cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
          }}
        />
        <Stack.Screen
          name="SelectTrade"
          component={SelectTrade}
          options={{
            cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
          }}
        />
        <Stack.Screen
          name="AddAddress"
          component={AddAddress}
          options={{
            cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
          }}
        />
        <Stack.Screen
          name="WorkerHome"
          component={WorkerHome}
          options={{
            cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
          }}
        />
        <Stack.Screen
          name="DailyJob"
          component={DailyJob}
          options={{
            cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
          }}
        />
        <Stack.Screen
          name="WorkerMyBooking"
          component={WorkerMyBooking}
          options={{
            cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
          }}
        />
        <Stack.Screen
          name="WorkerMyAccount"
          component={WorkerMyAccount}
          options={{
            cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
          }}
        />
        <Stack.Screen
          name="WorkerProfile"
          component={WorkerProfile}
          options={{
            cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
          }}
        />
        <Stack.Screen
          name="WorkerEditProfile"
          component={WorkerEditProfile}
          options={{
            cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
          }}
        />
        <Stack.Screen
          name="WorkerHelp"
          component={WorkerHelp}
          options={{
            cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
          }}
        />
        <Stack.Screen
          name="Plan"
          component={Plan}
          options={{
            cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
          }}
        />
        <Stack.Screen
          name="Ragister2"
          component={Ragister2}
          options={{
            cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
          }}
        />
        <Stack.Screen
          name="VideoTutorial"
          component={VideoTutorial}
          options={{
            cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
          }}
        />
        <Stack.Screen
          name="RegisterContractor"
          component={RegisterContractor}
          options={{
            cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
          }}
        />
        <Stack.Screen
          name="HomeContractor"
          component={HomeContractor}
          options={{
            cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
          }}
        />
        <Stack.Screen
          name="PaymentContractor"
          component={PaymentContractor}
          options={{
            cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
          }}
        />
        <Stack.Screen
          name="CategoriesContractor"
          component={CategoriesContractor}
          options={{
            cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
          }}
        />
        <Stack.Screen
          name="jobDetailsContractor"
          component={jobDetailsContractor}
          options={{
            cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
          }}
        />
        <Stack.Screen
          name="OfferMadeContractor"
          component={OfferMadeContractor}
          options={{
            cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
          }}
        />
        <Stack.Screen
          name="OfferMadeContractorDetails"
          component={OfferMadeContractorDetails}
          options={{
            cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
          }}
        />
        <Stack.Screen
          name="MyProjectsContractor"
          component={MyProjectsContractor}
          options={{
            cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
          }}
        />
        <Stack.Screen
          name="ProjectsCompletedDetailsConstractor"
          component={ProjectsCompletedDetailsConstractor}
          options={{
            cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
          }}
        />
        <Stack.Screen
          name="MyProjectsContractorXYZJobs"
          component={MyProjectsContractorXYZJobs}
          options={{
            cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
          }}
        />
        <Stack.Screen
          name="RatingContractor"
          component={RatingContractor}
          options={{
            cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
          }}
        />
        <Stack.Screen
          name="MyAccountContractor"
          component={MyAccountContractor}
          options={{
            cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
          }}
        />
        <Stack.Screen
          name="TermsContractor"
          component={TermsContractor}
          options={{
            cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
          }}
        />
        <Stack.Screen
          name="PrivacyContractor"
          component={PrivacyContractor}
          options={{
            cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
          }}
        />
        <Stack.Screen
          name="HelpContractor"
          component={HelpContractor}
          options={{
            cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
          }}
        />
        <Stack.Screen
          name="MyProfileContractor"
          component={MyProfileContractor}
          options={{
            cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
          }}
        />
        <Stack.Screen
          name="SubscriptionPlanContractor"
          component={SubscriptionPlanContractor}
          options={{
            cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
          }}
        />
        <Stack.Screen
          name="PaymentContractorSubscriptionPlan"
          component={PaymentContractorSubscriptionPlan}
          options={{
            cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
          }}
        />
        <Stack.Screen
          name="PostJobContractor"
          component={PostJobContractor}
          options={{
            cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
          }}
        />
        <Stack.Screen
          name="SuccessContractor"
          component={SuccessContractor}
          options={{
            cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
          }}
        />
        <Stack.Screen
          name="ReviewJobContractor"
          component={ReviewJobContractor}
          options={{
            cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
          }}
        />
        <Stack.Screen
          name="ProjectTypeSearchContractor"
          component={ProjectTypeSearchContractor}
          options={{
            cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
          }}
        />
        <Stack.Screen
          name="SelectUserTypeContractor"
          component={SelectUserTypeContractor}
          options={{
            cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
          }}
        />
        <Stack.Screen
          name="SelectTradeContractor"
          component={SelectTradeContractor}
          options={{
            cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
          }}
        />
        <Stack.Screen
          name="AutoCompleteGoogle"
          component={AutoCompleteGoogle}
          options={{
            cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
          }}
        />
        <Stack.Screen
          name="DailyJobViewAll"
          component={DailyJobViewAll}
          options={{
            cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
          }}
        />
        <Stack.Screen
          name="ProfileData"
          component={ProfileData}
          options={{
            cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
          }}
        />
        <Stack.Screen
          name="UserRegisterData"
          component={UserRegisterData}
          options={{
            cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
          }}
        />
        <Stack.Screen
          name="WorkerPrivacy"
          component={WorkerPrivacy}
          options={{
            cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
          }}
        />
        <Stack.Screen
          name="WorkerTerm"
          component={WorkerTerm}
          options={{
            cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
          }}
        />
        <Stack.Screen
          name="FilterUserScreen"
          component={FilterUserScreen}
          options={{
            cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
          }}
        />
        <Stack.Screen
          name="MyProfileUser"
          component={MyProfileUser}
          options={{
            cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
          }}
        />
        <Stack.Screen
          name="UserPrivacy"
          component={UserPrivacy}
          options={{
            cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
          }}
        />
        <Stack.Screen
          name="UserHelp"
          component={UserHelp}
          options={{
            cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
          }}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
};
export default StackNavigator;
