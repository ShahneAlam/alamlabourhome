import React, {useState, useEffect} from 'react';
import {
  View,
  Image,
  ImageBackground,
  StyleSheet,
  Text,
  TouchableOpacity,
  ScrollView,
  FlatList,
  Modal,
  Dimensions,
} from 'react-native';
//import Banner from '../home/Banner';
//import DailyJob from './DailyJob';
import Plan from './Plan';
import VideoTutorial from './VideoTutorial';
const {width} = Dimensions.get('window');
import {Api, LocalStorage} from '../services/Api';
import * as actions from '../redux/actions';
import {useDispatch} from 'react-redux';
import Geolocation from 'react-native-geolocation-service';
const height = Dimensions.get('window');
import stringsoflanguages from '../language';

const Home = ({navigation, route}) => {
  const dispatch = useDispatch();
  const [userDetail, setUserDetail] = useState('');
  const [banner, setBanner] = useState([]);
  const [job, setJob] = useState([]);
  const [subscription, setSubscription] = useState([]);
  const [activeSubscription, setActiveSubscription] = useState([]);
  const [videoTutorial, setvideoTutorial] = useState([]);
  const [modalOpen, setModalOpen] = useState(false);
  const [modalOpenWaste, setModalOpenWaste] = useState(false);

  const {_home, _success} = stringsoflanguages;
  let colors = [
    '#FF8A804d',
    '#EDE7F6',
    '#E1F5FE',
    '#F3E5F5',
    '#FFF8E1',
    '#FBE9E7',
    '#FCE4EC',
    '#E8EAF6',
  ];
  const [data, setData] = useState([
    {
      id: '1',
      title: '3 Months plan',
      subtitle: '₹300',
    },
    {
      id: '2',
      title: '3 Months plan',
      subtitle: '₹300',
    },
    {
      id: '3',
      title: '3 Months plan',
      subtitle: '₹300',
    },
  ]);
  useEffect(() => {
    tradeHandlerPress();
  }, []);

  const tradeHandlerPress = async () => {
    const response = await Api.HomepageCustomerApi({});
    //alert(JSON.stringify(response, null, 2));
    // console.log(JSON.stringify(response.home, null, 2));
    const {status = false} = response;
    const {
      user_detail,
      job_person = [],
      banner = [],
      job = [],
      subscription = [],
      active_subscription = [],
      video_tutorial = [],
    } = response.home;
    // alert(JSON.stringify(job, null, 2));

    if (status) {
      setUserDetail(user_detail);
      setBanner(banner);
      setJob(job);
      dispatch(
        actions.CustomerHomeJob(job),
        actions.activeSubscriptionPlanCustomer(active_subscription),
        actions.VideoTutorialCustomer(video_tutorial),
      );
      setSubscription(subscription);
      setActiveSubscription(active_subscription);
      setvideoTutorial(video_tutorial);
    } else {
      alert('Something went wrong');
    }
  };

  useEffect(() => {
    Geolocation.getCurrentPosition(
      position => {
        //   alert(JSON.stringify(position.coords, null, 2));
        //  setRegion(position);
        //    console.log(position, null, 2);
      },
      error => {
        // See error code charts below.
        console.log(error.code, error.message);
      },
      {
        enableHighAccuracy: false,
        timeout: 20000,
        maximumAge: 10000,
        forceLocationManager: false,
      },
    );

    Geolocation.watchPosition(
      position => {
        // alert(position.coords.latitude);
        // this.setState({
        // latitude: position.coords.latitude,
        // longitude: position.coords.longitude,
        // coordinates: this.state.coordinates.concat({
        // latitude: position.coords.latitude,
        // longitude: position.coords.longitude
        // })
        // });
      },
      error => {
        console.log(error);
      },
      {
        showLocationDialog: true,
        enableHighAccuracy: true,
        timeout: 20000,
        maximumAge: 0,
        distanceFilter: 0,
      },
    );
  }, []);
  const images = [
    {
      source: require('../images/banner.png'),
    },
    {
      source: require('../images/banner.png'),
    },
    {
      source: require('../images/banner.png'),
    },
  ];
  useEffect(() => {
    // alert(JSON.stringify(userDetail, null, 2));
  }, []);
  const Banner = () => {
    return (
      <View style={{marginTop: 2}}>
        <ScrollView horizontal>
          {banner.map((item, index) => {
            return (
              <Image
                key={index}
                source={{uri: item.image_url}}
                style={{
                  width: width - 30,
                  height: 150,
                  marginHorizontal: 15,
                  resizeMode: 'contain',
                }}
              />
            );
          })}
        </ScrollView>
      </View>
    );
  };
  return (
    <View style={styles.container}>
      <View
        style={{
          width: '65%',
          marginTop: 10,
          marginLeft: 10,
        }}>
        <Text style={styles.text} numberOfLines={1}>
          {userDetail.name}
        </Text>
      </View>
      <View
        style={{
          marginTop: 3,
          flexDirection: 'row',
          width: '65%',
          marginLeft: 15,
          alignItems: 'center',
        }}>
        <Image style={styles.image} source={require('../images/pin.png')} />
        <Text style={styles.subtext} numberOfLines={1}>
          {_home.subTitle}
        </Text>
      </View>
      <View style={{flexDirection: 'row', marginBottom: 5}}>
        <TouchableOpacity
          onPress={() => navigation.navigate('FilterUserScreen')}
          style={styles.search}>
          <Image
            style={styles.subimage}
            source={require('../images/search.png')}
          />
        </TouchableOpacity>
        <View style={styles.search2}>
          <Image
            style={styles.subimage}
            source={require('../images/notification.png')}
          />
        </View>
      </View>

      <ScrollView>
        <Banner />
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            width: '100%',
          }}>
          <Text style={styles.sub2text}>{_home.dailyJob}</Text>
          <TouchableOpacity
            onPress={() => navigation.navigate('DailyJobViewAll')}
            // onPress={() => navigation.navigate('UserRegisterData')}
            style={{
              flexDirection: 'row',
              // marginLeft: 'auto',
              marginHorizontal: 15,
            }}>
            <Text style={styles.sub3text}>{_home.view}</Text>
            <Image
              style={styles.videoimg}
              source={require('../images/video.png')}
            />
          </TouchableOpacity>
        </View>
        <View>
          <FlatList
            style={{
              width: '95%',
              alignSelf: 'center',
            }}
            numColumns={3}
            keyExtractor={item => item.id}
            data={job}
            renderItem={({item, index}) => (
              <TouchableOpacity
                style={[
                  stylesDailyJobs.touch,
                  {backgroundColor: colors[index % colors.length]},
                ]}
                onPress={() =>
                  navigation.navigate('CallList', {
                    id: item.id,
                    header: item.name,
                  })
                }>
                <Image
                  source={{uri: item.image_url}}
                  style={stylesDailyJobs.images}
                />
                <Text style={stylesDailyJobs.title}>{item.name}</Text>
              </TouchableOpacity>
            )}
          />
        </View>
        <Text style={styles.sub2text}>Contractual Job</Text>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-around',
            width: '95%',
            alignSelf: 'center',
          }}>
          <TouchableOpacity
            style={styles.contract}
            onPress={() => setModalOpen(true)}>
            <Image
              style={styles.contractimg}
              source={require('../images/Group.png')}
            />
            <Text style={styles.contracttext}>{_home.postJob}</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.contract}
            onPress={() => setModalOpen(true)}>
            <Image
              style={styles.contractimg}
              source={require('../images/gg4.png')}
            />
            <Text style={styles.contracttext}>{_home.findWorker}</Text>
          </TouchableOpacity>
        </View>
        <View style={{flexDirection: 'row'}}>
          <Text style={styles.sub2text}>{_home.subscriptionPlan}</Text>
          <View
            style={{
              flexDirection: 'row',
              marginLeft: 'auto',
              marginHorizontal: 15,
            }}>
            <Text style={styles.sub3text}>{_home.show}</Text>
            <Image
              style={styles.videoimg}
              source={require('../images/video.png')}
            />
          </View>
        </View>
        <View>
          <ScrollView horizontal>
            <FlatList
              numColumns={3}
              keyExtractor={item => item.id}
              data={data}
              renderItem={({item, index}) => (
                <View style={stylesSubscriptionPlan.box}>
                  <View style={stylesSubscriptionPlan.subbox}>
                    <Text style={stylesSubscriptionPlan.plantext}>
                      {item.title}
                    </Text>
                    <Text style={stylesSubscriptionPlan.subplantext}>
                      {item.subtitle}
                    </Text>
                  </View>
                  <View style={{flexDirection: 'row'}}>
                    <Image
                      style={stylesSubscriptionPlan.tickimg}
                      source={require('../images/tick.png')}
                    />
                    <Text style={stylesSubscriptionPlan.ticktext}>
                      Lorem Ipsum is simply
                    </Text>
                  </View>
                  <View style={{flexDirection: 'row'}}>
                    <Image
                      style={stylesSubscriptionPlan.tickimg}
                      source={require('../images/tick.png')}
                    />
                    <Text style={stylesSubscriptionPlan.ticktext}>
                      Lorem Ipsum is simply text
                    </Text>
                  </View>
                  <View style={{flexDirection: 'row'}}>
                    <Image
                      style={stylesSubscriptionPlan.tickimg}
                      source={require('../images/cross.png')}
                    />
                    <Text style={stylesSubscriptionPlan.ticktext}>
                      Lorem Ipsum is simply
                    </Text>
                  </View>
                  <TouchableOpacity
                    onPress={() => setModalOpenWaste(true)}
                    style={stylesSubscriptionPlan.touch}

                    // onPress={() => navigation.navigate('Payment')}
                  >
                    <Text style={stylesSubscriptionPlan.touchtext}>
                      Buy Now
                    </Text>
                  </TouchableOpacity>
                </View>
              )}
            />
          </ScrollView>
        </View>
        <View style={styles.call}>
          <Image
            style={{width: 25, height: 25, marginHorizontal: 10}}
            source={require('../images/info.png')}
          />
          <Text style={styles.info}>{_home.freeCalls}</Text>
        </View>
        <Text style={styles.texted}>{_home.activePlan}</Text>
        <View style={styles.month}>
          <Text style={styles.monthtext}>{_home.month}</Text>
          <View style={{flexDirection: 'row'}}>
            <Image
              style={{
                width: 14,
                height: 14,
                marginHorizontal: 20,
                marginTop: 10,
              }}
              source={require('../images/tick.png')}
            />
            <Text style={styles.submonth}>{_home.calls}</Text>
          </View>
          <View style={{flexDirection: 'row', justifyContent: 'space-evenly'}}>
            <Text style={styles.sub2month}>{_home.callLeft}</Text>
            <Text style={styles.sub2month}>{_home.purchased}</Text>
            <Text style={styles.sub2month}>{_home.valid}</Text>
          </View>
        </View>

        <View style={{flexDirection: 'row'}}>
          <Text style={styles.sub2text}>{_home.videoTutorial}</Text>
          <View
            style={{
              flexDirection: 'row',
              marginLeft: 'auto',
              marginHorizontal: 15,
            }}>
            <Text style={styles.sub3text}>{_home.show}</Text>
            <Image
              style={styles.videoimg}
              source={require('../images/video.png')}
            />
          </View>
        </View>
        <VideoTutorial />
      </ScrollView>
      <Modal
        visible={modalOpen}
        transparent={true}
        onRequestClose={() => setModalOpen(false)}>
        <View style={styles.modal_View}>
          <View style={styles.mdtop}>
            {/* <View style={styles.mdtop_1}>
            <Text style={styles.mdTopText}>Assign Driver:</Text>
          </View> */}
            <Image
              style={{
                width: 60,
                height: 60,
                alignSelf: 'center',
                marginTop: 20,
              }}
              source={require('../images/rupees.png')}
            />
            <Text style={styles.subscriptiontext}>{_success.subscription}</Text>
            <Text style={styles.subSubscriptiontext}>
              {_success.subscriptionTitle}
            </Text>

            <Text
              style={{
                fontSize: 30,
                marginVertical: 10,
                color: '#000',
                alignSelf: 'center',
                fontWeight: '900',
              }}>
              COMMING SOON....
            </Text>

            <TouchableOpacity
              activeOpacity={0.8}
              style={styles.mdbottomView}
              onPress={() => setModalOpen(false)}
              //  onPress={() => navigation.navigate('SubscriptionPlan')}
            >
              <Text style={styles.mdBottomText}>{_success.plan}</Text>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>

      <Modal
        visible={modalOpenWaste}
        transparent={true}
        onRequestClose={() => setModalOpenWaste(false)}>
        <View style={styles.modal_View}>
          <View style={styles.mdtop}>
            {/* <View style={styles.mdtop_1}>
            <Text style={styles.mdTopText}>Assign Driver:</Text>
          </View> */}
            <Image
              style={{
                width: 60,
                height: 60,
                alignSelf: 'center',
                marginTop: 20,
              }}
              source={require('../images/rupees.png')}
            />
            <Text style={styles.subscriptiontext}>{_success.subscription}</Text>
            <Text style={styles.subSubscriptiontext}>
              {_success.subscriptionTitle}
            </Text>

            <Text
              style={{
                fontSize: 30,
                marginVertical: 10,
                color: '#000',
                alignSelf: 'center',
                fontWeight: '900',
              }}>
              COMMING SOON....
            </Text>

            <TouchableOpacity
              activeOpacity={0.8}
              style={styles.mdbottomView}
              onPress={() => setModalOpenWaste(false)}
              //  onPress={() => navigation.navigate('SubscriptionPlan')}
            >
              <Text style={styles.mdBottomText}>{_success.plan}</Text>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
    </View>
  );
};
export default Home;

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#FFFFFF',
    flex: 1,
  },
  image: {
    width: 10,
    height: 15,
    resizeMode: 'contain',
  },
  text: {
    fontFamily: 'Avenir',
    fontWeight: 'bold',
    fontSize: 18,
    color: '#454545',
    marginHorizontal: 5,
  },
  subtext: {
    marginLeft: 5,
    fontFamily: 'Avenir-Medium',
    fontWeight: '500',
    fontSize: 14,
    color: '#606E87',
  },
  search: {
    width: 30,
    height: 30,
    backgroundColor: '#FFFFFF',
    borderRadius: 8,
    marginLeft: 'auto',
    marginTop: -40,
    elevation: 5,
  },
  search2: {
    width: 30,
    height: 30,
    backgroundColor: '#FFFFFF',
    borderRadius: 8,
    marginTop: -40,
    elevation: 5,
    marginLeft: 20,
    marginHorizontal: 20,
  },
  subimage: {
    width: 15,
    height: 15,
    alignSelf: 'center',
    marginTop: 7,
  },
  dot: {
    backgroundColor: '#1F2430',
    width: 8,
    height: 8,
    borderRadius: 4,
    marginLeft: 4,
    marginRight: 4,
    marginTop: 3,
    marginBottom: 3,
  },
  activeDot: {
    backgroundColor: '#7863E3',
    width: 8,
    height: 8,
    borderRadius: 4,
    marginLeft: 4,
    marginRight: 4,
    marginTop: 3,
    marginBottom: 3,
  },
  sub2image: {
    resizeMode: 'contain',
    height: 120,
    width: 320,
    marginHorizontal: 40,
    borderRadius: 25,
    marginTop: 20,
  },
  sub2text: {
    fontFamily: 'Avenir',
    fontWeight: 'bold',
    fontSize: 18,
    color: '#454545',
    marginHorizontal: 15,
    marginTop: 20,
  },
  sub3text: {
    fontFamily: 'Avenir-Medium',
    fontWeight: '500',
    fontSize: 14,
    color: '#6CBDFF',
    marginTop: 20,
  },
  videoimg: {
    width: 6,
    height: 10,
    marginLeft: 5,
    marginTop: 25,
  },
  color: {
    width: 83,
    height: 90,
    backgroundColor: '#FF8A804d',
    borderRadius: 10,
    marginBottom: 20,
  },
  color1: {
    width: 83,
    height: 90,
    backgroundColor: '#EDE7F6',
    borderRadius: 10,
  },
  color2: {
    width: 83,
    height: 90,
    backgroundColor: '#E1F5FE',
    borderRadius: 10,
  },
  color3: {
    width: 83,
    height: 90,
    backgroundColor: '#F3E5F5',
    borderRadius: 10,
  },

  colorcom: {
    width: 83,
    height: 90,
    backgroundColor: '#FFF8E1',
    borderRadius: 10,
  },
  colorcom1: {
    width: 83,
    height: 90,
    backgroundColor: '#FBE9E7',
    borderRadius: 10,
  },
  colorcom2: {
    width: 83,
    height: 90,
    backgroundColor: '#FCE4EC',
    borderRadius: 10,
  },
  colorcom3: {
    width: 83,
    height: 90,
    backgroundColor: '#E8EAF6',
    borderRadius: 10,
  },
  air: {
    width: 35,
    height: 35,
    alignSelf: 'center',
    marginTop: 15,
  },
  servicetext: {
    fontFamily: 'Avenir-Heavy',
    fontWeight: 'bold',
    fontSize: 10,
    color: '#454545',
    textAlign: 'center',
    marginTop: 8,
  },
  texted: {
    fontFamily: 'Avenir',
    fontWeight: 'bold',
    fontSize: 18,
    color: '#454545',
    marginTop: 20,
    marginHorizontal: 15,
  },
  contract: {
    width: '47%',
    height: 120,
    backgroundColor: '#fff',
    borderRadius: 10,
    elevation: 5,
    marginTop: 10,
    marginBottom: 10,
    // marginHorizontal: 10,
  },
  contractimg: {
    width: 50,
    height: 50,
    marginTop: 10,
    marginHorizontal: 10,
  },
  contracttext: {
    fontFamily: 'Avenir-Heavy',
    fontWeight: 'bold',
    fontSize: 15,
    color: '#454545',
    marginHorizontal: 10,
    marginTop: 20,
  },
  call: {
    padding: 10,
    marginHorizontal: 15,
    borderColor: '#00C853',
    borderWidth: 1,
    backgroundColor: '#00C8534d',
    borderRadius: 8,
    marginTop: 20,
    flexDirection: 'row',
  },
  info: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 14,
    fontWeight: '600',
    fontWeight: 'bold',
    color: '#00C853',
  },
  month: {
    backgroundColor: '#454545',
    borderRadius: 10,
    marginTop: 10,
    marginHorizontal: 15,
  },
  monthtext: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 18,
    fontWeight: 'bold',
    color: '#ffffff',
    marginTop: 10,
    marginHorizontal: 20,
  },
  submonth: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 15,
    fontWeight: 'bold',
    color: '#ffffff',
    marginTop: 5,
    marginLeft: -15,
  },
  sub2month: {
    fontFamily: 'Avenir-Medium',
    fontSize: 14,
    fontWeight: 'bold',
    color: '#ffffff',
    marginTop: 20,
    marginHorizontal: 20,
    textAlign: 'center',
    marginBottom: 10,
  },
  video: {
    width: 140,
    height: 100,
    marginHorizontal: 20,
    marginTop: 10,
    backgroundColor: '#0000004d',
    borderRadius: 15,
    elevation: 5,
  },
  play: {
    width: 30,
    height: 30,
    alignSelf: 'center',
    marginTop: 35,
  },
  playtext: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 12,
    fontWeight: 'bold',
    color: '#333333',
    marginTop: 5,
    marginHorizontal: 25,
  },
  subplaytext: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 11,
    fontWeight: 'bold',
    color: '#3333334d',
    marginTop: 5,
    marginHorizontal: 27,
    marginBottom: 20,
  },
  modal_View: {
    backgroundColor: '#000000aa',
    justifyContent: 'center',
    flex: 1,
  },
  mdtop: {
    backgroundColor: '#FFFFFF',
    // marginTop: height / 3,
    // marginTop: 200,
    //alignSelf: 'center',

    marginHorizontal: 20,
    borderRadius: 20,
  },
  subscriptiontext: {
    fontFamily: 'Avenir-Heavy',
    fontWeight: 'bold',
    fontSize: 20,
    color: '#333333',
    textAlign: 'center',
    marginTop: 10,
  },
  subSubscriptiontext: {
    fontFamily: 'Avenir-Medium',
    fontWeight: '500',
    fontSize: 16,
    color: '#3333334d',
    textAlign: 'center',
    marginTop: 10,
  },
  mdbottomView: {
    backgroundColor: '#6CBDFF',
    borderRadius: 20,
    padding: 10,
    marginTop: 40,
    // marginHorizontal: 25,
    width: 120,
    height: 40,
    marginBottom: 20,
    alignSelf: 'center',
  },
  mdBottomText: {
    fontFamily: 'Avenir-Heavy',
    fontWeight: 'bold',
    fontSize: 16,
    color: '#FFFFFF',
    textAlign: 'center',
  },
  topv4_Style: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 20,
    marginHorizontal: 20,
    justifyContent: 'space-between',
  },
});

const stylesDailyJobs = StyleSheet.create({
  container: {
    backgroundColor: '#F4F4F4',
    flex: 1,
  },
  sub2text: {
    fontFamily: 'Avenir',
    fontWeight: 'bold',
    fontSize: 18,
    color: '#454545',
    marginBottom: 20,
    marginHorizontal: 20,
    marginTop: 20,
  },
  sub3text: {
    fontFamily: 'Avenir-Medium',
    fontWeight: '500',
    fontSize: 14,
    color: '#6CBDFF',
    marginTop: 20,
  },
  videoimg: {
    width: 6,
    height: 10,
    marginLeft: 5,
    marginTop: 25,
  },
  touch: {
    width: '30.4%',
    height: 90,
    borderRadius: 10,
    marginVertical: 10,
    marginHorizontal: 5,
  },
  images: {
    width: 35,
    height: 35,
    alignSelf: 'center',
    marginTop: 15,
  },
  title: {
    fontFamily: 'Avenir-Heavy',
    fontWeight: 'bold',
    fontSize: 10,
    color: '#454545',
    textAlign: 'center',
    marginTop: 8,
  },
});

const stylesSubscriptionPlan = StyleSheet.create({
  container: {
    backgroundColor: '#F4F4F4',
    flex: 1,
  },
  sub2text: {
    fontFamily: 'Avenir',
    fontWeight: 'bold',
    fontSize: 18,
    color: '#454545',
    marginBottom: 20,
    marginHorizontal: 20,
    marginTop: 20,
  },
  sub3text: {
    fontFamily: 'Avenir-Medium',
    fontWeight: '500',
    fontSize: 14,
    color: '#6CBDFF',
    marginTop: 20,
  },
  videoimg: {
    width: 6,
    height: 10,
    marginLeft: 5,
    marginTop: 25,
  },
  box: {
    width: 208,
    backgroundColor: '#fff',
    borderRadius: 10,
    elevation: 5,
    marginTop: 10,
    marginBottom: 10,
    marginHorizontal: 15,
  },
  subbox: {
    height: 70,
    backgroundColor: '#ECEFF1',
    borderTopEndRadius: 10,
    borderTopLeftRadius: 10,
  },
  plantext: {
    fontFamily: 'Avenir-Heavy',
    fontWeight: 'bold',
    fontSize: 14,
    color: '#333333',
    textAlign: 'center',
    marginTop: 10,
  },
  subplantext: {
    fontFamily: 'Avenir-Heavy',
    fontWeight: 'bold',
    fontSize: 20,
    color: '#333333',
    textAlign: 'center',
    marginTop: 10,
  },
  tickimg: {
    width: 14,
    height: 14,
    marginHorizontal: 20,
    marginTop: 10,
  },
  ticktext: {
    fontFamily: 'Avenir-Medium',
    fontWeight: '500',
    fontSize: 12,
    color: '#8A8A8A',
    marginTop: 8,
    marginLeft: -5,
  },
  touch: {
    width: 100,
    height: 30,
    borderRadius: 25,
    backgroundColor: '#6CBDFF',
    marginVertical: 20,
    alignSelf: 'center',
  },
  touchtext: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 16,
    fontWeight: 'bold',
    alignSelf: 'center',
    color: '#ffffff',
    marginTop: 3,
  },
  images: {
    width: 35,
    height: 35,
    alignSelf: 'center',
    marginTop: 15,
  },
  title: {
    fontFamily: 'Avenir-Heavy',
    fontWeight: 'bold',
    fontSize: 10,
    color: '#454545',
    textAlign: 'center',
    marginTop: 8,
  },
});
