import React, {useState} from 'react';
import {
  Dimensions,
  Image,
  Modal,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {StatusBarDark} from '../Custom/CustomStatusBar';
import {Header, MainView} from '../Custom/CustomView';
import {RadioButton} from 'react-native-paper';
import stringsoflanguages from '../language';

const {height, width} = Dimensions.get('window');
const PaymentDone = ({navigation}) => {
  const {_success} = stringsoflanguages;
  const [modalOpen, setModalOpen] = useState(true);
  const [checked, setChecked] = useState('first');

  return (
    <Modal
      visible={modalOpen}
      transparent={true}
      onRequestClose={() => setModalOpen(false)}>
      <View style={styles.modal_View}>
        <View style={styles.mdtop}>
          {/* <View style={styles.mdtop_1}>
            <Text style={styles.mdTopText}>Assign Driver:</Text>
          </View> */}
          <Image
            style={{width: 70, height: 70, alignSelf: 'center', marginTop: 20}}
            source={require('../images/mark.png')}
          />
          <Text style={styles.text}>{_success.payment}</Text>

          <TouchableOpacity
            activeOpacity={0.8}
            style={styles.mdbottomView}
            onPress={() => navigation.navigate('PostJob')}>
            <Text style={styles.mdBottomText}>{_success.post}</Text>
          </TouchableOpacity>
        </View>
      </View>
    </Modal>
  );
};

export default PaymentDone;

const styles = StyleSheet.create({
  modal_View: {
    backgroundColor: '#000000aa',
    flex: 1,
  },
  mdtop: {
    backgroundColor: '#FFFFFF',
    marginTop: height / 3,
    marginHorizontal: 20,
    borderRadius: 20,
  },
  text: {
    fontFamily: 'Avenir-Medium',
    fontWeight: '500',
    fontSize: 22,
    color: '#333333',
    textAlign: 'center',
    marginTop: 10,
  },
  subtext: {
    fontFamily: 'Avenir-Medium',
    fontWeight: '500',
    fontSize: 16,
    color: '#3333334d',
    textAlign: 'center',
    marginTop: 10,
  },
  mdbottomView: {
    backgroundColor: '#6CBDFF',
    borderRadius: 20,
    padding: 10,
    marginTop: 40,
    // marginHorizontal: 25,
    width: 160,
    height: 40,
    marginBottom: 20,
    alignSelf: 'center',
  },
  mdBottomText: {
    fontFamily: 'Avenir-Heavy',
    fontWeight: 'bold',
    fontSize: 16,
    color: '#FFFFFF',
    textAlign: 'center',
  },
  topv4_Style: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 20,
    marginHorizontal: 20,
    justifyContent: 'space-between',
  },
  text2_Style: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 20,
    color: '#9F9F9F',
    fontWeight: 'bold',
  },
  subtext2_Style: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 14,
    color: '#9F9F9F',
    fontWeight: 'bold',
  },
  line: {
    borderColor: '#00000020',
    borderWidth: 0.5,
    marginTop: 15,
  },
});
