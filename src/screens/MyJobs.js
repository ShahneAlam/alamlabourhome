import React, {useEffect, useState} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  Image,
  Dimensions,
  TouchableOpacity,
  useWindowDimensions,
} from 'react-native';
import {StatusBarDark} from '../Custom/CustomStatusBar';
import {Header2} from '../Custom/CustomView';
import {TabView, SceneMap, TabBar} from 'react-native-tab-view';
import Dash from 'react-native-dash';
import {useNavigation} from '@react-navigation/native';
import stringsoflanguages from '../language';

const OpenJobs = () => {
  const {_MyProjectsContractor} = stringsoflanguages;
  const navigation = useNavigation();
  return (
    <View style={{flex: 1, backgroundColor: '#F4F4F4'}}>
      <ScrollView>
        <TouchableOpacity onPress={() => navigation.navigate('XyzJob')}>
          <View style={styles.container}>
            <Text style={styles.text}>{_MyProjectsContractor.xyzJob}</Text>
            <Text style={styles.subText}>
              {_MyProjectsContractor.loremtext}
            </Text>
            <DashLine />
            <View
              style={{flexDirection: 'row', justifyContent: 'space-evenly'}}>
              <View>
                <Text style={styles.date}>
                  {_MyProjectsContractor.startDate}
                </Text>
                <Text style={styles.subDate}>12/12/2021</Text>
              </View>

              <View>
                <Text style={styles.date}>
                  {_MyProjectsContractor.duration}
                </Text>
                <Text style={styles.subDate}>
                  {_MyProjectsContractor.shortTerm}
                </Text>
              </View>

              <View>
                <Text style={styles.date}>{_MyProjectsContractor.budget}</Text>
                <Text style={styles.subDate}>₹ 5000</Text>
              </View>
            </View>
            <DashLine />

            <View
              style={{flexDirection: 'row', justifyContent: 'space-evenly'}}>
              <View>
                <Text style={styles.date}>{_MyProjectsContractor.project}</Text>
                <Text style={styles.subDate}>
                  {_MyProjectsContractor.interior}
                </Text>
              </View>

              <View>
                <Text style={styles.date}>
                  {_MyProjectsContractor.jobLocation}
                </Text>
                <Text style={styles.subDate}>
                  {_MyProjectsContractor.address}
                </Text>
              </View>
            </View>
          </View>
        </TouchableOpacity>

        <View style={styles.container}>
          <Text style={styles.text}>{_MyProjectsContractor.xyzJob}</Text>
          <Text style={styles.subText}>{_MyProjectsContractor.loremtext}</Text>
          <DashLine />
          <View style={{flexDirection: 'row', justifyContent: 'space-evenly'}}>
            <View>
              <Text style={styles.date}>{_MyProjectsContractor.startDate}</Text>
              <Text style={styles.subDate}>12/12/2021</Text>
            </View>

            <View>
              <Text style={styles.date}>{_MyProjectsContractor.duration}</Text>
              <Text style={styles.subDate}>
                {_MyProjectsContractor.shortTerm}
              </Text>
            </View>

            <View>
              <Text style={styles.date}>{_MyProjectsContractor.budget}</Text>
              <Text style={styles.subDate}>₹ 5000</Text>
            </View>
          </View>
          <DashLine />

          <View style={{flexDirection: 'row', justifyContent: 'space-evenly'}}>
            <View>
              <Text style={styles.date}>{_MyProjectsContractor.project}</Text>
              <Text style={styles.subDate}>
                {_MyProjectsContractor.interior}
              </Text>
            </View>

            <View>
              <Text style={styles.date}>
                {_MyProjectsContractor.jobLocation}
              </Text>
              <Text style={styles.subDate}>
                {_MyProjectsContractor.address}
              </Text>
            </View>
          </View>
        </View>
      </ScrollView>
    </View>
  );
};
const AwardedJobs = () => {
  const {_MyProjectsContractor} = stringsoflanguages;
  return (
    <View style={{flex: 1, backgroundColor: '#F4F4F4'}}>
      <View style={styles.container}>
        <View style={{flexDirection: 'row'}}>
          <View>
            <Text style={styles.booking}>{_MyProjectsContractor.xyzJob}</Text>
            <Text style={styles.subBooking}>
              {_MyProjectsContractor.loremtext}
            </Text>
          </View>
          <Image
            style={{
              width: 20,
              height: 25,
              marginLeft: 'auto',
              marginHorizontal: 10,
            }}
            source={require('../images/medal.png')}
          />
        </View>
        <DashLine />
        <View style={{flexDirection: 'row', justifyContent: 'space-evenly'}}>
          <View>
            <Text style={styles.date}>{_MyProjectsContractor.startDate}</Text>
            <Text style={styles.subDate}>12/12/2021</Text>
          </View>

          <View>
            <Text style={styles.date}>{_MyProjectsContractor.duration}</Text>
            <Text style={styles.subDate}>
              {_MyProjectsContractor.shortTerm}
            </Text>
          </View>

          <View>
            <Text style={styles.date}>{_MyProjectsContractor.budget}</Text>
            <Text style={styles.subDate}>₹ 5000</Text>
          </View>
        </View>
        <DashLine />
        <View style={{flexDirection: 'row', justifyContent: 'space-evenly'}}>
          <View>
            <Text style={styles.date}>{_MyProjectsContractor.project}</Text>
            <Text style={styles.subDate}>{_MyProjectsContractor.interior}</Text>
          </View>

          <View>
            <Text style={styles.date}>{_MyProjectsContractor.jobLocation}</Text>
            <Text style={styles.subDate}>{_MyProjectsContractor.address}</Text>
          </View>
        </View>
        <DashLine />
        <View style={{flexDirection: 'row'}}>
          <View>
            <Text style={styles.booking}>{_MyProjectsContractor.otp}</Text>
            <Text style={styles.subBooking}>
              {_MyProjectsContractor.otpTitle}
            </Text>
          </View>
          <Text style={styles.otptext}>3456</Text>
        </View>
      </View>
    </View>
  );
};

const CancelledJobs = () => (
  <View style={{flex: 1, backgroundColor: '#F4F4F4'}}></View>
);

const CompletedJobs = () => (
  <View style={{flex: 1, backgroundColor: '#F4F4F4'}}></View>
);

const renderScene = SceneMap({
  first: OpenJobs,
  second: AwardedJobs,
  third: CancelledJobs,
  fourth: CompletedJobs,
});

const MyJobs = () => {
  const {_MyProjectsContractor} = stringsoflanguages;
  const layout = useWindowDimensions();
  const [index, setIndex] = React.useState(0);
  const [routes] = React.useState([
    {key: 'first', title: _MyProjectsContractor.openJobs},
    {key: 'second', title: _MyProjectsContractor.awardJobs},
    {key: 'third', title: _MyProjectsContractor.cancel},
    {key: 'fourth', title: _MyProjectsContractor.completedJob},
  ]);
  return (
    <View style={{backgroundColor: '#fff', flex: 1}}>
      <StatusBarDark />
      <Header2
        // onPress={() => navigation.goBack()}
        title={_MyProjectsContractor.myJobs}
      />
      <TabView
        navigationState={{index, routes}}
        renderScene={renderScene}
        onIndexChange={setIndex}
        initialLayout={{width: layout.width}}
        renderTabBar={props => (
          <TabBar
            style={styles.style}
            labelStyle={styles.labelStyle}
            tabStyle={{}}
            scrollEnabled={false}
            activeColor={'#fff'}
            inactiveColor={'#fff'}
            inactiveOpacity={0.5}
            {...props}
            indicatorStyle={styles.indicatorStyle}
          />
        )}
      />
    </View>
  );
};

export default MyJobs;

const styles = StyleSheet.create({
  container: {
    padding: 10,
    marginHorizontal: 30,
    backgroundColor: '#fff',
    borderRadius: 8,
    elevation: 5,
    marginTop: 20,
  },
  style: {backgroundColor: '#F2AD4B'},
  labelStyle: {
    fontSize: 8,
    fontFamily: 'Avenir-Medium',
    fontWeight: '500',
  },
  indicatorStyle: {backgroundColor: '#fff', height: 3},
  text: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 14,
    fontWeight: 'bold',
    color: '#fff',
    marginHorizontal: 20,
  },
  text: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 18,
    fontWeight: 'bold',
    marginHorizontal: 10,
  },
  subText: {
    fontFamily: 'Avenir-Medium',
    fontSize: 12,
    fontWeight: '500',
    marginHorizontal: 10,
    marginTop: 5,
    color: '#454545',
  },
  date: {
    fontFamily: 'Avenir-Normal',
    fontSize: 12,
    fontWeight: '400',
    marginHorizontal: 20,
    marginTop: 5,
    color: '#8A94A3',
  },
  subDate: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 13,
    fontWeight: 'bold',
    marginHorizontal: 20,
    marginTop: 5,
    color: '#454545',
  },
  booking: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 14,
    fontWeight: 'bold',
    color: '#454545',
  },
  subBooking: {
    fontFamily: 'Avenir-Medium',
    fontSize: 12,
    fontWeight: '500',
    color: '#A5A5A5',
    marginTop: 10,
  },
  otptext: {
    fontFamily: 'Avenir',
    fontSize: 22,
    fontWeight: 'bold',
    color: '#333333',
    marginHorizontal: 50,
    marginTop: 20,
  },
});
const DashLine = props => (
  <View style={{margin: 10}}>
    <Dash dashColor={'#C8C8D3'} dashLength={5} dashThickness={1} {...props} />
  </View>
);
