import React, {useEffect, useState} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  FlatList,
  Text,
  useColorScheme,
  View,
  Image,
  Linking,
  Dimensions,
  TouchableOpacity,
  useWindowDimensions,
} from 'react-native';
import {StatusBarDark} from '../Custom/CustomStatusBar';
import {Header, Header2} from '../Custom/CustomView';
import {TabView, SceneMap, TabBar} from 'react-native-tab-view';
import Dash from 'react-native-dash';
import stringsoflanguages from '../language';
import {Api} from '../services/Api';
import {useDispatch, useSelector} from 'react-redux';
import MaterialTabs from 'react-native-material-tabs';
import {useNavigation} from '@react-navigation/native';

const MyBooking = () => {
  const navigation = useNavigation();
  const {user_id} = useSelector(store => store);
  const [ongoing, setOngoing] = useState([]);
  const [selectedTab, setSelectedTab] = useState(0);
  const [ongoingList, setOngoingList] = useState([]);
  const [completedList, setCompletedList] = useState([]);

  const {_myBooking} = stringsoflanguages;

  useEffect(() => {
    GetJobsHandler();
  }, []);

  const GetJobsHandler = async () => {
    const body = {
      user_id: user_id,
    };
    const response = await Api.GetUserOngoingBookingApi(body);
    // alert(JSON.stringify(response.data, null, 2));
    //  console.log(JSON.stringify(response.data, null, 2));
    const {status = false, data = []} = response;
    setOngoingList(data);
  };

  useEffect(() => {
    GetCompleteHandler();
  }, []);

  const GetCompleteHandler = async () => {
    const body = {
      user_id: user_id,
    };
    const response = await Api.GetUserCompleteBookingApi(body);
    // alert(JSON.stringify(response.data, null, 2));
    console.log(JSON.stringify(response.data, null, 2));
    const {status = false, data = []} = response;
    setCompletedList(data);
  };

  const categorySelect = index => {
    setSelectedTab(index);
    if (index == 0) {
      // setOngoingList();
    } else if (index == 1) {
      //  setShortlist('shortlist');
    }
  };

  const renderItemCompleted = ({item, index}) => {
    const {_myBooking} = stringsoflanguages;
    return (
      <View style={{flex: 1, backgroundColor: '#fff'}}>
        <View
          style={{
            padding: 10,
            marginHorizontal: 20,
            backgroundColor: '#fff',
            elevation: 5,
            marginTop: 20,
            marginBottom: 3,
            borderRadius: 10,
          }}>
          <View style={{flexDirection: 'row'}}>
            <View>
              <Text style={styles.booking}>
                {_myBooking.booking}
                {item.id}
              </Text>
              <Text style={styles.subBooking}>{item.booking_date_time}</Text>
            </View>
          </View>
          <View style={styles.lineView}></View>
          <View style={{flexDirection: 'row'}}>
            <View style={{flexDirection: 'row'}}>
              <Image
                style={styles.imagebox}
                source={require('../images/photo.png')}
              />
            </View>
            <View
              style={{
                flexDirection: 'column',
                marginLeft: 10,
                width: '65%',
              }}>
              <Text style={styles.inputtxt}>{item.worker_name}</Text>

              <Text style={styles.subtext}>{item.trade_name}</Text>
              <View style={{flexDirection: 'row'}}>
                <Image
                  style={styles.sub3image}
                  source={require('../images/star.png')}
                />
                <Image
                  style={styles.sub4image}
                  source={require('../images/star.png')}
                />
                <Image
                  style={styles.sub5image}
                  source={require('../images/star.png')}
                />
              </View>
            </View>
          </View>
          <View style={styles.lineView}></View>
          <TouchableOpacity
          // onPress={() => navigation.navigate('RatingHome')}
          >
            <Text style={styles.ratetext}>{_myBooking.rate}</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  };

  const renderItemOngoing = ({item, index}) => {
    const {_myBooking} = stringsoflanguages;
    return (
      <View style={{flex: 1, backgroundColor: '#fff'}}>
        <View
          style={{
            padding: 15,
            marginHorizontal: 20,
            backgroundColor: '#fff',
            elevation: 5,
            marginTop: 15,
            borderRadius: 10,
            marginBottom: 8,
          }}>
          <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
            <View style={{width: '85%'}}>
              <Text style={styles.booking}>
                {_myBooking.booking}
                {item.id}
              </Text>
              <Text style={styles.subBooking}>{item.booking_date_time}</Text>
            </View>
            <TouchableOpacity
              onPress={() => {
                let phoneNumber = item.worker_mobile;
                if (Platform.OS === 'android') {
                  phoneNumber = `tel:${phoneNumber}`;
                } else {
                  phoneNumber = `telprompt:${phoneNumber}`;
                }
                Linking.openURL(phoneNumber);
              }}
              style={{marginTop: 5, marginRight: 6}}>
              <Image
                style={{
                  resizeMode: 'contain',
                  width: 25,
                  height: 25,
                }}
                source={require('../images/phone.png')}
              />
            </TouchableOpacity>
          </View>
          <View style={styles.lineView}></View>

          <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
            <View style={{flexDirection: 'row'}}>
              <View style={{flexDirection: 'row'}}>
                <Image
                  style={styles.imagebox}
                  source={require('../images/photo.png')}
                />
              </View>
              <View
                style={{
                  flexDirection: 'column',
                  marginLeft: 10,
                  width: '65%',
                }}>
                <Text style={styles.inputtxt}>{item.worker_name}</Text>

                <Text style={styles.subtext}>{item.trade_name}</Text>
                <View style={{flexDirection: 'row'}}>
                  <Image
                    style={styles.sub3image}
                    source={require('../images/star.png')}
                  />
                  <Image
                    style={styles.sub4image}
                    source={require('../images/star.png')}
                  />
                  <Image
                    style={styles.sub5image}
                    source={require('../images/star.png')}
                  />
                </View>
              </View>
            </View>
            <View style={{flexDirection: 'column'}}>
              <Image
                style={{
                  width: 25,
                  height: 25,
                  alignSelf: 'center',
                  resizeMode: 'contain',
                }}
                source={require('../images/share.png')}
              />
              <Text style={styles.location}>{_myBooking.location}</Text>
            </View>
          </View>
          <View style={styles.lineView}></View>
          <View
            style={{
              flexDirection: 'row',
              width: '100%',
            }}>
            <View style={{width: '81%'}}>
              <Text style={styles.booking}>{_myBooking.otp}</Text>
              <Text style={styles.subBooking}>{_myBooking.otpTitle}</Text>
            </View>
            <Text style={styles.otptext}>{item.otp}</Text>
          </View>
        </View>
      </View>
    );
  };

  return (
    <View style={{backgroundColor: '#fff', flex: 1}}>
      <StatusBarDark />
      <Header2
        // onPress={() => navigation.goBack()}
        title={_myBooking.title}
      />
      <MaterialTabs
        items={[_myBooking.ongoing, _myBooking.completed]}
        selectedIndex={selectedTab}
        onChange={index => categorySelect(index)}
        barColor="#F2AD4B"
        indicatorColor="#FFF"
        activeTextColor="#FFF"
        inactiveTextColor="#FFF"
        indicatorHeight={4}
      />

      <ScrollView showsVerticalScrollIndicator={false}>
        {selectedTab == 1 && (
          <FlatList
            style={{width: '100%', marginTop: 3}}
            data={completedList}
            showsVerticalScrollIndicator={false}
            renderItem={renderItemCompleted}
          />
        )}
        {selectedTab == 0 && (
          <FlatList
            style={{width: '100%', marginTop: 3}}
            data={ongoingList}
            showsVerticalScrollIndicator={false}
            renderItem={renderItemOngoing}
          />
        )}
      </ScrollView>
    </View>
  );
};

export default MyBooking;

const styles = StyleSheet.create({
  lineView: {
    marginVertical: 10,
    width: '100%',
    height: 0.8,
    backgroundColor: '#00000050',
  },
  container: {
    padding: 10,
    width: '100%',
    backgroundColor: '#F2AD4B',
    marginTop: 10,
  },
  style: {backgroundColor: '#F2AD4B'},
  labelStyle: {
    fontSize: 14,
    fontFamily: 'Avenir-Medium',
    fontWeight: '500',
  },
  indicatorStyle: {backgroundColor: '#fff', height: 3},
  text: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 14,
    fontWeight: 'bold',
    color: '#fff',
    marginHorizontal: 20,
  },
  imagebox: {
    // marginTop: 40,
    // marginHorizontal: 20,
    height: 50,
    width: 50,
    borderRadius: 25,
  },
  inputtxt: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 14,
    fontWeight: 'bold',
    color: '#080040',
  },
  sub3image: {
    height: 14,
    width: 14,
    resizeMode: 'contain',
  },
  sub4image: {
    height: 14,
    width: 14,
    resizeMode: 'contain',
    marginHorizontal: 5,
  },
  sub5image: {
    height: 14,
    width: 14,
    resizeMode: 'contain',
  },
  subtext: {
    fontFamily: 'Avenir-Normal',
    fontSize: 12,
    fontWeight: '400',
    color: '#8A94A3',
  },
  touch: {
    width: 150,
    height: 35,
    borderRadius: 25,
    backgroundColor: '#6DD400',
    marginTop: 20,
    // marginBottom: 10,
    // alignSelf: 'center',
  },
  touch2: {
    width: 150,
    height: 35,
    borderRadius: 25,
    backgroundColor: '#6CBDFF',
    marginTop: 20,
    marginBottom: 10,
    // alignSelf: 'center',
  },
  touchtext: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 16,
    fontWeight: 'bold',
    alignSelf: 'center',
    color: '#ffffff',
    marginTop: 5,
  },
  booking: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 14,
    fontWeight: 'bold',
    color: '#454545',
  },
  subBooking: {
    fontFamily: 'Avenir-Medium',
    fontSize: 12,
    fontWeight: '500',
    color: '#A5A5A5',
    marginTop: 5,
  },
  location: {
    fontFamily: 'Avenir-Medium',
    fontSize: 10,
    fontWeight: '500',
    color: '#333333',
    textAlign: 'center',
  },
  otptext: {
    marginTop: -5,
    alignSelf: 'flex-start',
    fontFamily: 'Avenir',
    fontSize: 22,
    fontWeight: 'bold',
    color: '#333333',
  },
  ratetext: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 16,
    fontWeight: 'bold',
    color: '#F2AD4B',
    textAlign: 'center',
  },
  subratetext: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 15,
    fontWeight: 'bold',
    color: '#333333',
  },
});
const DashLine = props => (
  <View style={{margin: 10}}>
    <Dash dashColor={'#C8C8D3'} dashLength={5} dashThickness={1} {...props} />
  </View>
);
