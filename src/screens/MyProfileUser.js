import React, {useState, useEffect} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  Image,
  Dimensions,
  TouchableOpacity,
  TextInput,
  FlatList,
  ImageBackground,
} from 'react-native';
import {StatusBarDark} from '../Custom/CustomStatusBar';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import AppHeader from '../Custom/CustomAppHeader';
import {Api} from '../services/Api';
import {useIsFocused} from '@react-navigation/native';
import {useDispatch, useSelector, useStore} from 'react-redux';

const MyProfileUser = ({navigation, route}) => {
  const {user_id} = useSelector(store => store);
  const isFocused = useIsFocused();
  const [list, setList] = useState([]);
  const [profileData, setProfileData] = useState('');
  useEffect(() => {
    GetJobsHandler();
  }, [isFocused]);

  const GetJobsHandler = async () => {
    const body = {
      user_id: user_id,
    };
    const response = await Api.GetUserProfileRegisterDataApi(body);
    // alert(JSON.stringify(response, null, 2));
    // console.log(JSON.stringify(response, null, 2));
    const {status = false, user, address_list = []} = response;
    if (status == true) {
      setProfileData(user);
      setList(user.address_list);
    } else {
      alert('Something went wrong');
    }
  };
  const JobRequestslRender = ({item, index}) => {
    return (
      <>
        <View style={{width: '100%', alignSelf: 'center'}}>
          <Text
            style={[styles.detaiTextOffCss, {color: '#2A3B56', marginTop: 5}]}>
            1:- {item.location}
          </Text>

          <Text
            style={[styles.detaiTextOffCss, {color: '#2A3B56', marginTop: 5}]}>
            2:-Address Type {item.address_type}
          </Text>

          <Text
            style={[
              styles.detaiTextOffCss,
              {color: '#2A3B56', marginTop: 5, marginBottom: 10},
            ]}>
            3:- Area{item.area}
          </Text>
        </View>
      </>
    );
  };

  useEffect(() => {
    const unsubscribe = navigation.addListener('focus', () => {});
    return unsubscribe;
  }, [navigation]);

  return (
    <View style={styles.continer}>
      <StatusBarDark bg="#FFF" />
      <AppHeader
        backOnClick={() => {
          navigation.goBack();
        }}
        backIcon={require('../images/e-remove.png')}
        title={'My Profile'}
        // search={require('../../images/edit_icon.png')}
        // searchIcontintColor={'#000'}
        // searchOnClick={() => {
        //    navigation.navigate('WorkerEditProfile');
        // }}
      />
      <ScrollView
        contentContainerStyle={{flexGrow: 1}}
        showsVerticalScrollIndicator={false}>
        <View style={{paddingHorizontal: 25}}>
          <View style={{paddingVertical: 20, alignItems: 'center'}}>
            <View style={styles.userImgViewOffCss}>
              <Image
                style={styles.userImgOffCss}
                source={{uri: profileData.profile}}
              />
            </View>
            <Text style={styles.userNameTextOffCss}> {profileData.name}</Text>
            <Text style={styles.containersubtext}>
              {profileData.trade_name}
            </Text>
          </View>
          <View style={styles.detaiViewOffCss}>
            <Text style={styles.detaiTextOffCss}>My Name</Text>
            <Text style={[styles.detaiTextOffCss, {color: '#2A3B56'}]}>
              {profileData.name}
            </Text>
          </View>
          <View style={styles.detaiViewOffCss}>
            <Text style={styles.detaiTextOffCss}>Phone Number</Text>
            <Text style={[styles.detaiTextOffCss, {color: '#2A3B56'}]}>
              {profileData.mobile}
            </Text>
          </View>
          <View style={styles.detaiViewOffCss}>
            <Text style={styles.detaiTextOffCss}>Email</Text>
            <Text style={[styles.detaiTextOffCss, {color: '#2A3B56'}]}>
              {profileData.email}
            </Text>
          </View>

          {/* <View style={{paddingVertical: 15}}>
            <Text style={styles.detaiTextOffCss}>Address</Text>
            <FlatList
              style={{width: '95%', alignSelf: 'center'}}
              data={list}
              showsHorizontalScrollIndicator={false}
              renderItem={JobRequestslRender}
            />
          </View> */}
        </View>
      </ScrollView>
    </View>
  );
};

export default MyProfileUser;

const styles = StyleSheet.create({
  continer: {
    flex: 1,
    backgroundColor: '#FFFFFF',
  },
  userImgViewOffCss: {
    height: hp(16),
    width: wp(32),
    borderRadius: wp(32) / 2,
    justifyContent: 'center',
    alignItems: 'center',
    marginVertical: 7,
  },
  userImgOffCss: {
    height: '100%',
    width: '100%',
    resizeMode: 'contain',
  },
  userNameTextOffCss: {
    fontFamily: 'Avenir-Heavy',
    fontSize: hp(2.2),
    fontWeight: '900',
    color: '#2A3B56',
    paddingVertical: 5,
    letterSpacing: 0.7,
  },
  containersubtext: {
    fontFamily: 'Avenir-Medium',
    fontWeight: '500',
    fontSize: hp(1.65),
    color: '#8A94A3',
  },
  detaiViewOffCss: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingVertical: 15,
  },
  detaiTextOffCss: {
    fontSize: hp(1.9),
    fontFamily: 'Avenir-Meduim',
    fontWeight: '500',
    color: '#8A94A3',
  },
  photoVideoImgOffCss: {
    width: wp(26),
    height: hp(12),
    resizeMode: 'contain',
    borderRadius: 8,
    justifyContent: 'center',
    alignItems: 'center',
    // width: wp(27.80), height: hp(13.70), resizeMode: 'contain', borderRadius: 8
  },
});
