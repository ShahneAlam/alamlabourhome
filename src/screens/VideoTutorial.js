import React, {useState} from 'react';
import {
  View,
  Image,
  ImageBackground,
  StyleSheet,
  Text,
  TouchableOpacity,
  ScrollView,
  FlatList,
  SafeAreaView,
} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {useNavigation} from '@react-navigation/native';
import stringsoflanguages from '../language';

const VideoTutorial = () => {
  const {_HomeContractor} = stringsoflanguages;
  const navigation = useNavigation();
  const [videoTutorialData, setVideoTutorialData] = useState([
    {title: _HomeContractor.quotation, timeing: _HomeContractor.min},
    {title: _HomeContractor.quotation, timeing: _HomeContractor.min},
    {title: _HomeContractor.quotation, timeing: _HomeContractor.min},
    {title: _HomeContractor.quotation, timeing: _HomeContractor.min},
  ]);

  const videosTutorialList = ({item, index}) => {
    return (
      <>
        <View style={{width: '24.5%'}}>
          <View style={{width: wp(40), marginLeft: 15}}>
            <TouchableOpacity activeOpacity={0.8}>
              <ImageBackground
                style={styles.videoIconOffCss}
                source={require('../images/Group2.png')}>
                <Image
                  style={styles.playIconOffCss}
                  source={require('../images/play.png')}
                />
              </ImageBackground>
            </TouchableOpacity>
            <Text style={styles.playtext}>{item.title}</Text>
            <Text style={styles.subplaytext}>{item.timeing}</Text>
          </View>
        </View>
      </>
    );
  };

  return (
    <View style={styles.container}>
      <View style={{marginBottom: 10}}>
        <FlatList
          horizontal
          data={videoTutorialData}
          renderItem={videosTutorialList}
          showsHorizontalScrollIndicator={false}
        />
      </View>
    </View>
  );
};
export default VideoTutorial;

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#F4F4F4',
    flex: 1,
  },
  videoIconOffCss: {
    width: 140,
    height: 100,
    // marginHorizontal: 10,
    marginTop: 10,
    backgroundColor: '#0000004d',
    borderRadius: 15,
    elevation: 5,
  },
  playIconOffCss: {
    width: 30,
    height: 30,
    alignSelf: 'center',
    marginTop: 35,
    resizeMode: 'contain',
  },
  playtext: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 12,
    fontWeight: 'bold',
    color: '#333333',
    marginTop: 5,
    marginHorizontal: 5,
  },
  subplaytext: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 11,
    fontWeight: 'bold',
    color: '#3333334d',
    marginTop: 5,
    marginHorizontal: 5,
    marginBottom: 10,
  },
});
