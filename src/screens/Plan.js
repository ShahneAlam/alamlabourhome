import React, {useEffect, useState} from 'react';
import {
  View,
  Image,
  ImageBackground,
  StyleSheet,
  Text,
  TouchableOpacity,
  ScrollView,
  FlatList,
  Dimensions,
  SafeAreaView,
} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import {useSelector} from 'react-redux';

const Plan = () => {
  const {subscriptionPlan} = useSelector(store => store);
  const navigation = useNavigation();
  const [data, setData] = useState([
    {
      id: '1',
      title: _SubscriptionPlanContractor.monthPlan,
      subtitle: '₹300',
    },
    {
      id: '2',
      title: _SubscriptionPlanContractor.monthPlan,
      subtitle: '₹300',
    },
    {
      id: '3',
      title: _SubscriptionPlanContractor.monthPlan,
      subtitle: '₹300',
    },
  ]);
  useEffect(() => {
    //  alert(JSON.stringify(subscriptionPlan, null, 2));
  }, []);
  return (
    <View style={styles.container}>
      <ScrollView horizontal>
        <FlatList
          numColumns={3}
          keyExtractor={item => item.id}
          data={data}
          renderItem={({item, index}) => (
            <SafeAreaView style={styles.box}>
              <View style={styles.subbox}>
                <Text style={styles.plantext}>{item.title}</Text>
                <Text style={styles.subplantext}>{item.subtitle}</Text>
              </View>
              <View style={{flexDirection: 'row'}}>
                <Image
                  style={styles.tickimg}
                  source={require('../images/tick.png')}
                />
                <Text style={styles.ticktext}>
                  {_SubscriptionPlanContractor.loremTitle}
                </Text>
              </View>
              <View style={{flexDirection: 'row'}}>
                <Image
                  style={styles.tickimg}
                  source={require('../images/tick.png')}
                />
                <Text style={styles.ticktext}>
                  {_SubscriptionPlanContractor.loremSubtitle}
                </Text>
              </View>
              <View style={{flexDirection: 'row'}}>
                <Image
                  style={styles.tickimg}
                  source={require('../images/cross.png')}
                />
                <Text style={styles.ticktext}>
                  {_SubscriptionPlanContractor.loremTitle}
                </Text>
              </View>
              <TouchableOpacity
                style={styles.touch}
                onPress={() => navigation.navigate('Payment')}>
                <Text style={styles.touchtext}>
                  {_SubscriptionPlanContractor.buy}
                </Text>
              </TouchableOpacity>
            </SafeAreaView>
          )}
        />
      </ScrollView>
    </View>
  );
};
export default Plan;

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#F4F4F4',
    flex: 1,
  },
  sub2text: {
    fontFamily: 'Avenir',
    fontWeight: 'bold',
    fontSize: 18,
    color: '#454545',
    marginBottom: 20,
    marginHorizontal: 20,
    marginTop: 20,
  },
  sub3text: {
    fontFamily: 'Avenir-Medium',
    fontWeight: '500',
    fontSize: 14,
    color: '#6CBDFF',
    marginTop: 20,
  },
  videoimg: {
    width: 6,
    height: 10,
    marginLeft: 5,
    marginTop: 25,
  },
  box: {
    width: 208,
    height: 210,
    backgroundColor: '#fff',
    borderRadius: 10,
    elevation: 5,
    marginTop: 20,
    marginBottom: 10,
    marginHorizontal: 20,
  },
  subbox: {
    height: 70,
    backgroundColor: '#ECEFF1',
    borderTopEndRadius: 10,
    borderTopLeftRadius: 10,
  },
  plantext: {
    fontFamily: 'Avenir-Heavy',
    fontWeight: 'bold',
    fontSize: 14,
    color: '#333333',
    textAlign: 'center',
    marginTop: 10,
  },
  subplantext: {
    fontFamily: 'Avenir-Heavy',
    fontWeight: 'bold',
    fontSize: 20,
    color: '#333333',
    textAlign: 'center',
    marginTop: 10,
  },
  tickimg: {
    width: 14,
    height: 14,
    marginHorizontal: 20,
    marginTop: 10,
  },
  ticktext: {
    fontFamily: 'Avenir-Medium',
    fontWeight: '500',
    fontSize: 12,
    color: '#8A8A8A',
    marginTop: 8,
    marginLeft: -5,
  },
  touch: {
    width: 100,
    height: 30,
    borderRadius: 25,
    backgroundColor: '#6CBDFF',
    marginTop: 20,
    marginBottom: 10,
    alignSelf: 'center',
  },
  touchtext: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 16,
    fontWeight: 'bold',
    alignSelf: 'center',
    color: '#ffffff',
    marginTop: 3,
  },
  images: {
    width: 35,
    height: 35,
    alignSelf: 'center',
    marginTop: 15,
  },
  title: {
    fontFamily: 'Avenir-Heavy',
    fontWeight: 'bold',
    fontSize: 10,
    color: '#454545',
    textAlign: 'center',
    marginTop: 8,
  },
});
