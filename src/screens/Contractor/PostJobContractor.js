import React, {useEffect, useState} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  Image,
  Dimensions,
  TouchableOpacity,
  TextInput,
  Modal,
  ImageBackground,
} from 'react-native';
import {StatusBarDark} from '../../Custom/CustomStatusBar';
import {Dropdown} from 'react-native-material-dropdown-v2-fixed';
import {Header} from '../../Custom/CustomView';
import {launchCamera, launchImageLibrary} from 'react-native-image-picker';
import {RadioButton, TouchableRipple} from 'react-native-paper';
const {height, width} = Dimensions.get('window');

let data = [
  {
    value: 'I am a worker',
  },
  {
    value: 'I am a contractor',
  },
  {
    value: 'I am a customer',
  },
];

const PostJobContractor = ({navigation}) => {
  const [checked, setChecked] = useState('first');
  const [tick, setTick] = useState('third');
  return (
    <View style={{backgroundColor: '#F4F4F4', flex: 1}}>
      <StatusBarDark />
      <Header onPress={() => navigation.goBack()} title={'Post Job'} />
      <ScrollView>
        <Text style={styles.text}>Let’s start with a strong title</Text>
        <View style={styles.box}>
          <Text style={styles.jobHeadingText}>Job Heading</Text>
        </View>
        <Text style={styles.text}>Choose from template</Text>
        <Dropdown
          style={styles.drops}
          underlineColor="transparent"
          label="Select Job Description"
          icon="cheveron-down"
          iconColor="#0000004d"
          icon={require('../../images/drop.png')}
          data={data}
        />
        <View style={styles.boxes}>
          <TextInput
            style={styles.textinput}
            placeholder="Write your own Job Description"
          />
        </View>
        <View style={styles.box}>
          <View style={{flexDirection: 'row'}}>
            <Text>Job Start Date</Text>
            <Image
              style={{width: 20, height: 20, marginLeft: 'auto'}}
              source={require('../../images/calendar.png')}
            />
          </View>
        </View>
        <Text style={styles.text}>How long will your work take?</Text>
        <View
          style={{flexDirection: 'row', alignSelf: 'center', marginTop: 10}}>
          <View style={styles.boxsd}>
            <View style={{alignSelf: 'center', marginTop: 5}}>
              <RadioButton
                value="first"
                status={checked === 'first' ? 'checked' : 'unchecked'}
                onPress={() => setChecked('first')}
                uncheckedColor={'#69707F'}
                color={'#7BAAED'}
              />
            </View>
            <Text style={styles.subtext}>Short term work</Text>
            <Text style={styles.sub2text}>Less than 30hrs/week</Text>
            <Text style={styles.sub3text}>Less than 3 months</Text>
          </View>

          <View style={styles.boxsdLonger}>
            <View style={{alignSelf: 'center', marginTop: 5}}>
              <RadioButton
                value="second"
                status={checked === 'second' ? 'checked' : 'unchecked'}
                onPress={() => setChecked('second')}
                uncheckedColor={'#69707F'}
                color={'#7BAAED'}
              />
            </View>
            <Text style={styles.subtext}>Longer term work</Text>
            <Text style={styles.sub2text}>More than 30 hrs/week</Text>
            <Text style={styles.sub3text}>3+ months</Text>
          </View>
        </View>
        <Text style={styles.text}>Job Location</Text>
        <TouchableOpacity style={styles.touchButton}>
          <Text style={styles.touchText}>+ Add New</Text>
        </TouchableOpacity>
        <View style={styles.home}>
          <View style={{flexDirection: 'row'}}>
            <View style={{marginTop: 10}}>
              <RadioButton
                value="third"
                status={tick === 'third' ? 'checked' : 'unchecked'}
                onPress={() => setTick('third')}
                uncheckedColor={'#69707F'}
                color={'#7BAAED'}
              />
            </View>
            <Text style={styles.hometext}>Home</Text>
          </View>
          <Text style={styles.home2text}>
            21/C9, 2nd Floor, Sector-7, Rohini Opp Metro Pillor No. 400, New
            Delhi - 110085 (IN)
          </Text>
        </View>

        <View style={styles.home}>
          <View style={{flexDirection: 'row'}}>
            <View style={{marginTop: 10}}>
              <RadioButton
                value="fourth"
                status={tick === 'fourth' ? 'checked' : 'unchecked'}
                onPress={() => setTick('fourth')}
                uncheckedColor={'#69707F'}
                color={'#7BAAED'}
              />
            </View>
            <Text style={styles.hometext}>Work</Text>
          </View>
          <Text style={styles.home2text}>
            21/C9, 2nd Floor, Sector-7, Rohini Opp Metro Pillor No. 400, New
            Delhi - 110085 (IN)
          </Text>
        </View>

        <TouchableRipple
          style={styles.box}
          rippleColor="#00000010"
          //  onPress={() => navigation.navigate('ProjectTypeSearchContractor')}
        >
          <View style={{flexDirection: 'row'}}>
            <Text>Select Project Type</Text>
            <Image
              style={{width: 10, height: 16, marginLeft: 'auto'}}
              source={require('../../images/arrow.png')}
            />
          </View>
        </TouchableRipple>
        <Text style={styles.text}>Tell us about your budget</Text>
        <Text style={styles.sub4text}>
          This will help us match you to within your range.
        </Text>
        <View style={styles.box}>
          <Text>Area in sq ft</Text>
        </View>

        <View style={styles.boxes}>
          <TextInput style={styles.textinput} placeholder="Specification" />
        </View>
        <View style={styles.box}>
          <Text>Enter Project Cost</Text>
        </View>
        <Text style={styles.text}>Upload Images/videos of job related</Text>
        <Image
          style={styles.imageBox}
          source={require('../../images/choose-file.png')}
        />

        <TouchableOpacity
          style={styles.touch}
          onPress={() => navigation.navigate('SuccessContractor')}>
          <Text style={styles.touchtext}>Review & Submit</Text>
        </TouchableOpacity>
      </ScrollView>
    </View>
  );
};

export default PostJobContractor;

const styles = StyleSheet.create({
  jobHeadingText: {
    fontFamily: 'Avenir-Medium',
    fontSize: 16,
    fontWeight: '500',
    color: '#454545',
    textAlign: 'justify',
  },
  text: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 16,
    fontWeight: 'bold',
    marginHorizontal: 20,
    marginTop: 16,
    color: '#454545',
  },
  hometext: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 16,
    fontWeight: 'bold',
    marginHorizontal: 10,
    color: '#454545',
  },
  box: {
    padding: 15,
    marginHorizontal: 20,
    backgroundColor: '#fff',
    borderRadius: 15,
    elevation: 5,
    marginTop: 15,
  },
  drops: {
    width: '90%',
    height: 60,
    backgroundColor: '#fff',
    marginTop: 15,
    borderRadius: 20,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    borderBottomWidth: 0,
    elevation: 2,
    alignSelf: 'center',
  },
  textinput: {
    marginHorizontal: 10,
  },
  boxes: {
    height: 150,
    width: '90%',
    borderRadius: 20,
    backgroundColor: '#fff',
    elevation: 2,
    marginTop: 20,
    alignSelf: 'center',
  },
  boxsd: {
    width: '43%',
    borderRadius: 20,
    backgroundColor: '#fff',
    elevation: 2,
    marginRight: 6,
  },
  boxsdLonger: {
    width: '43%',
    borderRadius: 20,
    backgroundColor: '#fff',
    elevation: 2,
    marginLeft: 6,
  },
  subtext: {
    textAlign: 'center',
    fontFamily: 'Avenir-Medium',
    fontSize: 14,
    fontWeight: '500',
    color: '#454545',
  },
  sub2text: {
    textAlign: 'center',
    fontFamily: 'Avenir-Medium',
    fontSize: 9,
    fontWeight: '500',
    color: '#4545454d',
    marginTop: 5,
    marginHorizontal: 20,
  },
  sub3text: {
    textAlign: 'center',
    fontFamily: 'Avenir-Medium',
    fontSize: 10,
    lineHeight: 15,
    fontWeight: '500',
    color: '#4545454d',
    marginHorizontal: 20,
    marginBottom: 15,
  },
  touchButton: {
    width: 90,
    height: 30,
    backgroundColor: '#F2AD4B',
    borderRadius: 8,
    marginTop: -20,
    marginLeft: 'auto',
    marginHorizontal: 20,
  },
  touchText: {
    color: '#fff',
    textAlign: 'center',
    marginTop: 5,
    fontWeight: 'bold',
  },
  home: {
    padding: 10,
    marginHorizontal: 20,
    backgroundColor: '#fff',
    borderRadius: 15,
    elevation: 2,
    marginTop: 15,
  },
  home2text: {
    fontFamily: 'Avenir-Medium',
    fontSize: 11,
    fontWeight: '500',
    marginHorizontal: 45,
    lineHeight: 20,
    marginTop: -20,
  },
  sub4text: {
    fontFamily: 'Avenir-Normal',
    fontSize: 14,
    fontWeight: '400',
    color: '#454545',
    marginHorizontal: 20,
    marginTop: 5,
  },
  imageBox: {
    width: 162,
    height: 115,
    borderRadius: 15,
    backgroundColor: '#fff',
    marginTop: 15,
    marginHorizontal: 20,
  },
  touch: {
    padding: 15,
    marginHorizontal: 20,
    borderRadius: 25,
    backgroundColor: '#6CBDFF',
    marginVertical: 20,
  },
  touchtext: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 16,
    fontWeight: 'bold',
    alignSelf: 'center',
    color: '#ffffff',
  },
});
