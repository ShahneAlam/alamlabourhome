import React, {useEffect, useState} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  Image,
  Dimensions,
  TouchableOpacity,
} from 'react-native';
import {StatusBarDark} from '../../Custom/CustomStatusBar';
import {HeaderLight} from '../../Custom/CustomView';
import {TouchableRipple} from 'react-native-paper';
import stringsoflanguages from '../../language';

const MyAccountContractor = ({navigation}) => {
  const {_MyAccountContractor} = stringsoflanguages;
  return (
    <View style={{backgroundColor: '#fff', flex: 1}}>
      <StatusBarDark />
      <HeaderLight />
      <View
        style={{
          width: '90%',
          alignSelf: 'center',
          backgroundColor: '#fff',
          elevation: 5,
          marginTop: -70,
          borderRadius: 10,
        }}>
        <Image
          style={{
            width: 80,
            height: 80,
            alignSelf: 'center',
            borderRadius: 40,
            marginTop: -40,
          }}
          source={require('../../images/pic.png')}
        />
        <View
          style={{
            width: '85%',
            alignSelf: 'center',
            marginBottom: 20,
          }}>
          <Text style={styles.text}>{_MyAccountContractor.name}</Text>
          <Text style={styles.subtext}>+91 9876543212</Text>
        </View>
      </View>
      <TouchableRipple
        rippleColor="#00000007"
        onPress={() => navigation.navigate('MyProfileContractor')}>
        <View style={styles.rowMain}>
          <View style={styles.twoDataView}>
            <Image
              style={{width: 20, height: 20}}
              source={require('../../images/profile.png')}
            />
            <Text style={styles.sub2text}>{_MyAccountContractor.profile}</Text>
          </View>
          <View style={{width: 40}}>
            <Image
              style={{
                width: 10,
                height: 15,
                alignSelf: 'flex-end',
              }}
              source={require('../../images/arrow.png')}
            />
          </View>
        </View>
      </TouchableRipple>
      <TouchableRipple
        rippleColor="#00000007"
        onPress={() => navigation.navigate('SubscriptionPlanContractor')}>
        <View style={styles.rowMain}>
          <View style={styles.twoDataView}>
            <Image
              style={{width: 20, height: 20}}
              source={require('../../images/rupe.png')}
            />
            <Text style={styles.sub2text}>
              {_MyAccountContractor.Subscription}
            </Text>
          </View>
          <View style={{width: 40}}>
            <Image
              style={{
                width: 10,
                height: 15,
                alignSelf: 'flex-end',
              }}
              source={require('../../images/arrow.png')}
            />
          </View>
        </View>
      </TouchableRipple>
      <TouchableRipple
        rippleColor="#00000007"
        onPress={() => navigation.navigate('HelpContractor')}>
        <View style={styles.rowMain}>
          <View style={styles.twoDataView}>
            <Image
              style={{width: 20, height: 22}}
              source={require('../../images/help.png')}
            />
            <Text style={styles.sub2text}>{_MyAccountContractor.Help}</Text>
          </View>
          <View style={{width: 40}}>
            <Image
              style={{
                width: 10,
                height: 15,
                alignSelf: 'flex-end',
              }}
              source={require('../../images/arrow.png')}
            />
          </View>
        </View>
      </TouchableRipple>
      <TouchableRipple
        rippleColor="#00000007"
        // onPress={() => navigation.navigate('TermsContractor')}
      >
        <View style={styles.rowMain}>
          <View style={styles.twoDataView}>
            <Image
              style={{width: 20, height: 20}}
              source={require('../../images/rate.png')}
            />
            <Text style={styles.sub2text}>{_MyAccountContractor.rateApp}</Text>
          </View>

          <View style={{width: 40}}>
            <Image
              style={{
                width: 10,
                height: 15,
                alignSelf: 'flex-end',
              }}
              source={require('../../images/arrow.png')}
            />
          </View>
        </View>
      </TouchableRipple>
      <TouchableRipple
        rippleColor="#00000007"
        onPress={() => navigation.navigate('PrivacyContractor')}>
        <View style={styles.rowMain}>
          <View style={styles.twoDataView}>
            <Image
              style={{width: 20, height: 26}}
              source={require('../../images/privacy.png')}
            />
            <Text style={styles.subsstext}>{_MyAccountContractor.privacy}</Text>
          </View>
          <View style={{width: 40}}>
            <Image
              style={{
                width: 10,
                height: 15,
                alignSelf: 'flex-end',
              }}
              source={require('../../images/arrow.png')}
            />
          </View>
        </View>
      </TouchableRipple>
      <TouchableRipple
        rippleColor="#00000007"
        onPress={() => navigation.navigate('TermsContractor')}>
        <View style={styles.rowMain}>
          <View style={styles.twoDataView}>
            <Image
              style={{width: 20, height: 20}}
              source={require('../../images/terms.png')}
            />
            <Text style={styles.sub2text}>{_MyAccountContractor.terms}</Text>
          </View>
          <View style={{width: 40}}>
            <Image
              style={{
                width: 10,
                height: 15,
                alignSelf: 'flex-end',
              }}
              source={require('../../images/arrow.png')}
            />
          </View>
        </View>
      </TouchableRipple>

      <TouchableOpacity
        style={styles.touch}
        onPress={() => navigation.replace('HomeContractor')}>
        <Text style={styles.touchtext}>{_MyAccountContractor.SignOut}</Text>
      </TouchableOpacity>
    </View>
  );
};

export default MyAccountContractor;

const styles = StyleSheet.create({
  twoDataView: {
    flexDirection: 'row',
    width: '80%',
    marginLeft: 20,
  },
  rowMain: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 20,
    justifyContent: 'space-between',
    width: '94%',
  },
  container: {
    width: '100%',
    height: 203,
    backgroundColor: '#6CBDFF',
  },
  text: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 18,
    fontWeight: 'bold',
    color: '#454545',
    textAlign: 'center',
    marginTop: 5,
  },
  subtext: {
    fontFamily: 'Avenir-Medium',
    fontSize: 14,
    fontWeight: '500',
    color: '#454545',
    textAlign: 'center',
    marginTop: 5,
  },
  sub2text: {
    fontFamily: 'Avenir-Medium',
    fontSize: 14,
    fontWeight: '500',
    color: '#454545',
    marginLeft: 10,
  },
  subsstext: {
    fontFamily: 'Avenir-Medium',
    fontSize: 14,
    fontWeight: '500',
    color: '#454545',
    marginLeft: 10,
    marginTop: 5,
  },
  touch: {
    padding: 15,
    marginHorizontal: 30,
    borderRadius: 25,
    backgroundColor: '#F1F1F1',
    marginTop: 60,
  },
  touchtext: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 16,
    fontWeight: 'bold',
    alignSelf: 'center',
    color: '#2A3B56',
  },
});
