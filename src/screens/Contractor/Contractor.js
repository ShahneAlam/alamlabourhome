import React, {useEffect, useState} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  Image,
  Dimensions,
  TouchableOpacity,
} from 'react-native';
import {StatusBarDark} from '../../Custom/CustomStatusBar';
import {HeaderLight} from '../../Custom/CustomView';

const Contractor = ({navigation}) => {
  return (
    <View style={{backgroundColor: '#fff', flex: 1}}>
      <StatusBarDark />
      <HeaderLight />
      <View
        style={{
          width: 340,
          height: 120,
          backgroundColor: '#fff',
          elevation: 5,
          marginTop: -70,
          borderRadius: 10,
          marginHorizontal: 25,
        }}>
        <Image
          style={{width: 80, height: 80, alignSelf: 'center', marginTop: -40}}
          source={require('../../images/pic.png')}
        />
        <Text style={styles.text}>Rahul Malhotra</Text>
        <Text style={styles.subtext}>+91 9876543212</Text>
      </View>
      <View style={{flexDirection: 'row'}}>
        <Image
          style={{width: 20, height: 20, marginTop: 30, marginHorizontal: 30}}
          source={require('../../images/profile.png')}
        />
        <Text style={styles.sub2text}>My Profile</Text>
        <TouchableOpacity>
          <Image
            style={{
              width: 10,
              height: 15,
              marginTop: 30,
              marginHorizontal: 220,
            }}
            source={require('../../images/arrow.png')}
          />
        </TouchableOpacity>
      </View>

      <View style={{flexDirection: 'row'}}>
        <Image
          style={{width: 15, height: 20, marginTop: 30, marginHorizontal: 32}}
          source={require('../../images/address.png')}
        />
        <Text style={styles.sub2text}>Manage Address</Text>
        <TouchableOpacity>
          <Image
            style={{
              width: 10,
              height: 15,
              marginTop: 30,
              marginHorizontal: 175,
            }}
            source={require('../../images/arrow.png')}
          />
        </TouchableOpacity>
      </View>

      <View style={{flexDirection: 'row'}}>
        <Image
          style={{width: 20, height: 20, marginTop: 30, marginHorizontal: 30}}
          source={require('../../images/help.png')}
        />
        <Text style={styles.sub2text}>Help</Text>
        <TouchableOpacity>
          <Image
            style={{
              width: 10,
              height: 15,
              marginTop: 30,
              marginHorizontal: 250,
            }}
            source={require('../../images/arrow.png')}
          />
        </TouchableOpacity>
      </View>

      <View style={{flexDirection: 'row'}}>
        <Image
          style={{width: 20, height: 20, marginTop: 30, marginHorizontal: 30}}
          source={require('../../images/rate.png')}
        />
        <Text style={styles.sub2text}>Rate The App</Text>
        <TouchableOpacity>
          <Image
            style={{
              width: 10,
              height: 15,
              marginTop: 30,
              marginHorizontal: 195,
            }}
            source={require('../../images/arrow.png')}
          />
        </TouchableOpacity>
      </View>

      <View style={{flexDirection: 'row'}}>
        <Image
          style={{width: 15, height: 20, marginTop: 30, marginHorizontal: 32}}
          source={require('../../images/privacy.png')}
        />
        <Text style={styles.sub2text}>Privacy Policy</Text>
        <TouchableOpacity>
          <Image
            style={{
              width: 10,
              height: 15,
              marginTop: 30,
              marginHorizontal: 190,
            }}
            source={require('../../images/arrow.png')}
          />
        </TouchableOpacity>
      </View>

      <View style={{flexDirection: 'row'}}>
        <Image
          style={{width: 20, height: 20, marginTop: 30, marginHorizontal: 30}}
          source={require('../../images/terms.png')}
        />
        <Text style={styles.sub2text}>Terms & Conditions</Text>
        <TouchableOpacity>
          <Image
            style={{
              width: 10,
              height: 15,
              marginTop: 30,
              marginHorizontal: 150,
            }}
            source={require('../../images/arrow.png')}
          />
        </TouchableOpacity>
      </View>
      <TouchableOpacity
        style={styles.touch}
        onPress={() => navigation.navigate('Success2')}>
        <Text style={styles.touchtext}>Sign Out</Text>
      </TouchableOpacity>
    </View>
  );
};

export default Contractor;

const styles = StyleSheet.create({
  container: {
    width: '100%',
    height: 203,
    backgroundColor: '#6CBDFF',
  },
  text: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 18,
    fontWeight: 'bold',
    color: '#454545',
    textAlign: 'center',
    marginTop: 5,
  },
  subtext: {
    fontFamily: 'Avenir-Medium',
    fontSize: 14,
    fontWeight: '500',
    color: '#454545',
    textAlign: 'center',
    marginTop: 5,
  },
  sub2text: {
    fontFamily: 'Avenir-Medium',
    fontSize: 14,
    fontWeight: '500',
    color: '#454545',
    marginTop: 30,
    marginLeft: -10,
  },
  touch: {
    padding: 15,
    marginHorizontal: 30,
    borderRadius: 25,
    backgroundColor: '#F6F4F4',
    marginTop: 60,
  },
  touchtext: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 16,
    fontWeight: 'bold',
    alignSelf: 'center',
    color: '#2A3B56',
  },
});
