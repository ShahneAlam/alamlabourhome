import React, {useEffect, useState} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  Image,
  Dimensions,
  BackHandler,
  TouchableOpacity,
} from 'react-native';
import {StatusBarDark} from '../../Custom/CustomStatusBar';
import {Header} from '../../Custom/CustomView';
import {RadioButton} from 'react-native-paper';
const {height} = Dimensions.get('window');
import Dialog, {DialogContent} from 'react-native-popup-dialog';

const PaymentContractorSubscriptionPlan = ({navigation}) => {
  const [visible, setVisible] = useState(false);
  const [checked, setChecked] = useState('first');

  const handlePaymentSuccessful = () => {
    navigation.replace('ReviewJobContractor');
    setVisible(false);
  };

  function handleBackButtonClick() {
    setVisible(false);
    return true;
  }

  useEffect(() => {
    BackHandler.addEventListener('hardwareBackPress', handleBackButtonClick);
    return () => {
      BackHandler.removeEventListener(
        'hardwareBackPress',
        handleBackButtonClick,
      );
    };
  }, []);
  return (
    <View style={{backgroundColor: '#F4F4F4', flex: 1}}>
      <StatusBarDark />
      <Header onPress={() => navigation.goBack()} title={'Payment'} />
      <ScrollView>
        <View style={styles.container}>
          <View
            style={{
              marginVertical: 20,
              flexDirection: 'row',
              justifyContent: 'space-between',
            }}>
            <View>
              <Text style={styles.text}>Amount Payable</Text>
              <Text style={styles.subtext}>(Incl. of all taxes)</Text>
            </View>
            <Text style={styles.sub2text}>₹5000</Text>
          </View>
        </View>
        <View style={styles.subContainer}>
          <Text style={styles.sub3text}>Payment Options</Text>
        </View>

        <View style={styles.topv4_Style}>
          <View>
            <Text style={styles.text2_Style}>Debit Cards</Text>
          </View>
          <RadioButton
            value="first"
            status={checked === 'first' ? 'checked' : 'unchecked'}
            onPress={() => setChecked('first')}
            uncheckedColor={'#69707F'}
            color={'#7BAAED'}
          />
        </View>

        <View style={styles.topv4_Style}>
          <View>
            <Text style={styles.text2_Style}>Credit Cards</Text>
          </View>
          <RadioButton
            value="second"
            status={checked === 'second' ? 'checked' : 'unchecked'}
            onPress={() => setChecked('second')}
            uncheckedColor={'#69707F'}
            color={'#7BAAED'}
          />
        </View>

        <View style={styles.topv4_Style}>
          <View>
            <Text style={styles.text2_Style}>Net Banking</Text>
          </View>
          <RadioButton
            value="third"
            status={checked === 'third' ? 'checked' : 'unchecked'}
            onPress={() => setChecked('third')}
            uncheckedColor={'#69707F'}
            color={'#7BAAED'}
          />
        </View>

        <TouchableOpacity
          style={styles.touch}
          //  onPress={() => navigation.navigate('PaymentDone')}
          onPress={() => setVisible(true)}>
          <Text style={styles.touchtext}>PAY NOW</Text>
        </TouchableOpacity>

        <Dialog
          visible={visible}
          onTouchOutside={() => {
            setVisible({visible: true});
          }}>
          <DialogContent>
            <View style={{width: 250, backgroundColor: '#FFF'}}>
              <Image
                style={{
                  height: 73,
                  width: 73,
                  marginTop: 20,
                  alignSelf: 'center',
                }}
                source={require('../../images/mark.png')}
              />
              <Text style={styles.rejectTextss}>Payment Successful</Text>
              <TouchableOpacity
                onPress={() => handlePaymentSuccessful()}
                setVisible={false}
                style={styles.touchii}>
                <Text style={styles.title}>POST JOB</Text>
              </TouchableOpacity>
            </View>
          </DialogContent>
        </Dialog>
      </ScrollView>
    </View>
  );
};

export default PaymentContractorSubscriptionPlan;

const styles = StyleSheet.create({
  rejectTextss: {
    marginTop: 20,
    alignSelf: 'center',
    textAlign: 'center',
    marginHorizontal: 40,
    fontSize: 16,
    lineHeight: 20,
    fontFamily: 'Avenir-Heavy',
    color: '#000',
    fontWeight: '600',
  },
  title: {
    fontSize: 18,
    color: '#FFFFFF',
    fontFamily: 'Avenir-Heavy',
    textAlign: 'center',
  },
  touchii: {
    alignSelf: 'center',
    backgroundColor: '#6CBDFF',
    width: '45%',
    borderRadius: 12,
    marginTop: 20,
    marginBottom: 10,
    paddingVertical: 6,
  },
  plan: {
    width: 340,
    height: 270,
    backgroundColor: '#fff',
    borderRadius: 10,
    elevation: 5,
    marginTop: 20,
    marginBottom: 10,
    marginHorizontal: 25,
  },
  container: {
    marginHorizontal: 20,
    backgroundColor: '#F2AD4B',
    borderRadius: 10,
    marginTop: 20,
  },
  text: {
    fontFamily: 'Avenir-Heavy',
    fontWeight: 'bold',
    fontSize: 18,
    color: '#ffffff',
    marginHorizontal: 10,
  },
  subtext: {
    fontFamily: 'Avenir-Medium',
    fontWeight: '500',
    fontSize: 14,
    color: '#ffffff',
    marginHorizontal: 10,
  },
  sub2text: {
    fontFamily: 'Avenir-Heavy',
    fontWeight: 'bold',
    fontSize: 18,
    color: '#ffffff',
    marginHorizontal: 10,
    marginTop: 10,
  },
  subContainer: {
    width: '100%',
    padding: 10,
    backgroundColor: '#fff',
    marginTop: 20,
  },
  sub3text: {
    fontFamily: 'Avenir-Heavy',
    fontWeight: 'bold',
    fontSize: 16,
    color: '#263238',
    marginHorizontal: 20,
  },
  topv4_Style: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 20,
    marginHorizontal: 20,
    justifyContent: 'space-between',
  },
  text2_Style: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 15,
    color: '#263238',
    fontWeight: 'bold',
    marginHorizontal: 10,
  },
  subtext2_Style: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 14,
    color: '#9F9F9F',
    fontWeight: 'bold',
  },
  touch: {
    padding: 15,
    marginHorizontal: 30,
    borderRadius: 30,
    backgroundColor: '#6CBDFF',
    marginTop: '80%',
  },
  touchtext: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 16,
    fontWeight: 'bold',
    alignSelf: 'center',
    color: '#ffffff',
  },
});
