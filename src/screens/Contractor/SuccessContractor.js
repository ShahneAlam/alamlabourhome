import React, {useEffect, useState} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  Image,
  Dimensions,
  TouchableOpacity,
} from 'react-native';
import {StatusBarDark} from '../../Custom/CustomStatusBar';
const {height} = Dimensions.get('window');

const SuccessContractor = ({navigation}) => {
  return (
    <View style={{backgroundColor: '#FFFFFF', flex: 1}}>
      <StatusBarDark />
      <Image style={styles.image} source={require('../../images/check.png')} />
      <Text style={styles.text}>Success</Text>
      <Text style={styles.text2}>
        Work Approval Request Send{'\n'} Successfully.
      </Text>
      <TouchableOpacity
        style={styles.touch}
        onPress={() => navigation.replace('HomeContractor')}>
        <Text style={styles.touchtext}>GO TO HOME</Text>
      </TouchableOpacity>
    </View>
  );
};

export default SuccessContractor;

const styles = StyleSheet.create({
  image: {
    marginTop: height / 3,
    width: 100,
    height: 100,
    resizeMode: 'contain',
    alignSelf: 'center',
  },
  text: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 30,
    fontWeight: 'bold',
    color: '#454545',
    textAlign: 'center',
    marginTop: 10,
  },
  text2: {
    fontFamily: 'Avenir-Medium',
    fontSize: 16,
    fontWeight: '500',
    color: '#454545',
    textAlign: 'center',
    marginTop: 10,
    lineHeight: 25,
  },
  touch: {
    width: 200,
    height: 50,
    borderRadius: 25,
    backgroundColor: '#49535B',
    marginTop: 20,
    marginBottom: 10,
    alignSelf: 'center',
  },
  touchtext: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 16,
    fontWeight: 'bold',
    alignSelf: 'center',
    color: '#ffffff',
    marginTop: 15,
  },
});
