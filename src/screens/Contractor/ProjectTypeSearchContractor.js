import React, {useEffect, useState} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  Image,
  TouchableOpacity,
} from 'react-native';
import {StatusBarDark} from '../../Custom/CustomStatusBar';
import {Header} from '../../Custom/CustomView';

const ProjectTypeSearchContractor = ({navigation}) => {
  return (
    <View style={{backgroundColor: '#F4F4F4', flex: 1}}>
      <StatusBarDark />
      <Header
        onPress={() => navigation.goBack()}
        title={'Terms & Conditions'}
      />
      <ScrollView>
        <Text style={styles.subtext}>Lorem</Text>
      </ScrollView>
    </View>
  );
};

export default ProjectTypeSearchContractor;

const styles = StyleSheet.create({
  subtext: {
    fontFamily: 'Avenir-Medium',
    fontWeight: '500',
    fontSize: 16,
    lineHeight: 25,
    color: '#454545',
    textAlign: 'justify',
    marginTop: 10,
    marginHorizontal: 20,
  },
});
