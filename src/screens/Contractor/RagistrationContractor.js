import CheckBox from '@react-native-community/checkbox';
import React, {useEffect, useState} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  Image,
  Dimensions,
  TouchableOpacity,
  TextInput,
} from 'react-native';
import {StatusBarDark} from '../../Custom/CustomStatusBar';
import {Dropdown} from 'react-native-material-dropdown-v2-fixed';

let data = [
  {
    id: '1',
    value: 'I am a worker',
  },
  {
    id: '2',
    value: 'I am a contractor',
  },
  {
    id: '3',
    value: 'I am a customer',
  },
];
const navigateUser = (index, indexmove) => {
  if (index == 0) {
  } else if (index == 1) {
    navigation.navigate('Contractor');
  } else if (index == 2) {
  }
};
const RagistrationContractor = ({navigation, route}) => {
  const [toggleCheckBox, setToggleCheckBox] = useState(false);
  return (
    <View style={{backgroundColor: '#F4F4F4', flex: 1}}>
      <StatusBarDark />
      <Text style={styles.text}>Register</Text>
      <Text style={styles.subtext}>Create a account to start using</Text>
      <Dropdown
        style={styles.drops}
        underlineColor="transparent"
        label="Select User Type"
        // icon="cheveron-down"
        iconColor="#0000004d"
        //onPress={() => navigation.navigate('Contractor')}
        //  onValueChange={(index, indexmove) => navigateUser(index, indexmove)}
        icon={require('../../images/drop.png')}
        data={data}
      />
      <TextInput style={styles.textinput} placeholder="Name*" />
      <TextInput style={styles.textinput} placeholder="Company Name" />
      <TextInput style={styles.textinput} placeholder="Email Address" />
      <View style={{flexDirection: 'row', marginTop: 20, marginHorizontal: 20}}>
        <CheckBox
          disabled={false}
          value={toggleCheckBox}
          onValueChange={newValue => setToggleCheckBox(newValue)}
          tintColors={{
            true: '#6CBDFF',
            false: 'grey',
          }}
        />
        <Text
          style={{
            fontSize: 15,
            fontWeight: 'bold',
            color: '#606E87',
            // marginHorizontal: 5,
          }}>
          By Signing up i Agree to the
          <Text
            style={{
              fontSize: 15,
              fontWeight: 'bold',
              color: '#6CBDFF',
              marginHorizontal: 40,
              marginRight: 5,
            }}>
            Term and Condition & Privacy policy.
          </Text>
        </Text>
      </View>

      <TouchableOpacity
        style={styles.touch}
        //  onPress={() => navigation.navigate('TabNavigator')}
        onPress={() => navigation.navigate('RagistrationContractor')}>
        <Text style={styles.touchtext}>Submit</Text>
      </TouchableOpacity>
    </View>
  );
};

export default RagistrationContractor;

const styles = StyleSheet.create({
  text: {
    fontFamily: 'Avenir',
    fontSize: 30,
    fontWeight: 'bold',
    color: '#454545',
    marginTop: '30%',
    marginHorizontal: 30,
  },
  subtext: {
    fontFamily: 'Avenir-Medium',
    fontSize: 14,
    fontWeight: '500',
    color: '#606E87',
    marginTop: 10,
    marginHorizontal: 30,
    lineHeight: 20,
  },
  drops: {
    width: '90%',
    alignSelf: 'center',
    height: 60,
    backgroundColor: '#fff',
    marginHorizontal: 30,
    marginTop: 40,
    borderRadius: 20,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    borderBottomWidth: 0,
    elevation: 2,
  },
  textinput: {
    height: 60,
    width: '90%',
    alignSelf: 'center',
    borderRadius: 20,
    backgroundColor: '#fff',
    elevation: 2,
    marginTop: 40,
    marginHorizontal: 30,
    padding: 20,
  },
  touch: {
    padding: 15,
    marginHorizontal: 30,
    borderRadius: 25,
    backgroundColor: '#6CBDFF',
    marginTop: 40,
  },
  touchtext: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 16,
    fontWeight: 'bold',
    alignSelf: 'center',
    color: '#ffffff',
  },
});
