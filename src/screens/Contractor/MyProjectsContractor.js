import React, {useEffect, useState} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  Image,
  FlatList,
  Dimensions,
  TouchableOpacity,
  BackHandler,
  useWindowDimensions,
} from 'react-native';
import {StatusBarDark} from '../../Custom/CustomStatusBar';
import {Header} from '../../Custom/CustomView';
import {TabView, SceneMap, TabBar} from 'react-native-tab-view';
import Dash from 'react-native-dash';
import {useNavigation} from '@react-navigation/native';
import {TouchableRipple} from 'react-native-paper';
import MaterialTabs from 'react-native-material-tabs';
import Dialog, {DialogContent} from 'react-native-popup-dialog';
import OTPInputView from '@twotalltotems/react-native-otp-input';
import stringsoflanguages from '../../language';

const MyProjectsContractor = ({navigation}) => {
  const {_MyProjectsContractor} = stringsoflanguages;
  const [visible, setVisible] = useState(false);

  const [selectedTab, setSelectedTab] = useState(0);
  const [product, setProduct] = useState([
    {
      id: '1',
    },
  ]);
  const [completedList, setComplete] = useState([
    {
      id: '2',
    },
    {
      id: '3',
    },
  ]);

  function handleBackButtonClick() {
    setVisible(false);
    return true;
  }

  useEffect(() => {
    BackHandler.addEventListener('hardwareBackPress', handleBackButtonClick);
    return () => {
      BackHandler.removeEventListener(
        'hardwareBackPress',
        handleBackButtonClick,
      );
    };
  }, []);

  const onPressJobs = (item, index) => {
    navigation.navigate('MyProjectsContractorXYZJobs');
  };

  const renderItemCompleted = ({item, index}) => {
    //  console.log(JSON.stringify(item))
    return (
      <TouchableRipple
        // onPress={() => onPressJobs()}
        style={{flex: 1}}>
        <View style={styles.container}>
          <View
            style={{
              flexDirection: 'row',
              width: '90%',
              alignSelf: 'center',
              justifyContent: 'space-between',
            }}>
            <TouchableRipple
              rippleColor="#00000010"
              onPress={() => onPressJobs()}
              style={styles.jobView}>
              <Text style={styles.text}>{_MyProjectsContractor.xyzJob}</Text>
            </TouchableRipple>

            <View
              // onPress={() => navigation.navigate('XyzJob')}
              style={styles.redDaysView}>
              <Text style={styles.textdays}>
                {_MyProjectsContractor.ongoing}
              </Text>
            </View>
          </View>
          <Text style={styles.timeTextt}>
            {_MyProjectsContractor.loremtext}
          </Text>

          <DashLine />
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              width: '90%',
              alignSelf: 'center',
            }}>
            <View style={{width: '30%'}}>
              <Text style={styles.date}>{_MyProjectsContractor.startDate}</Text>
              <Text style={styles.subDate}>12/12/2021</Text>
            </View>

            <View style={{width: '30%'}}>
              <Text style={styles.date}>{_MyProjectsContractor.duration}</Text>
              <Text style={styles.subDate}>
                {_MyProjectsContractor.shortTerm}
              </Text>
            </View>

            <View style={{width: '25%'}}>
              <Text style={styles.date}>{_MyProjectsContractor.budget}</Text>
              <Text style={styles.subDate}>₹ 5000</Text>
            </View>
          </View>
          <DashLine />

          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignSelf: 'center',
              width: '90%',
            }}>
            <View style={{width: '48%'}}>
              <Text style={styles.date}>{_MyProjectsContractor.project}</Text>
              <Text style={styles.subDate}>
                {_MyProjectsContractor.interior}
              </Text>
            </View>

            <View style={{width: '48%'}}>
              <Text style={styles.date}>
                {_MyProjectsContractor.jobLocation}
              </Text>
              <Text style={styles.subDate}>
                {_MyProjectsContractor.address}
              </Text>
            </View>
          </View>

          <TouchableOpacity
            onPress={() => setVisible(true)}
            style={styles.rowDataButton}>
            <Text style={styles.textShortBlack}>
              {_MyProjectsContractor.endjob}
            </Text>
          </TouchableOpacity>
        </View>
      </TouchableRipple>
    );
  };
  const onPressingHandle = (item, index) => {
    navigation.navigate('MyProjectsContractorXYZJobs');
  };
  const renderItemMyJob = ({item, index}) => {
    //  console.log(JSON.stringify(item))
    return (
      <TouchableRipple
        rippleColor="#00000010"
        onPress={() => onPressingHandle()}>
        <View style={styles.container}>
          <View
            style={{
              flexDirection: 'row',
              width: '90%',
              alignSelf: 'center',
              justifyContent: 'space-between',
            }}>
            <View style={styles.jobViewcompleted}>
              <Text style={styles.text}>{_MyProjectsContractor.xyzJob}</Text>
            </View>
          </View>
          <Text style={styles.timeTextt}>
            {_MyProjectsContractor.loremtext}
          </Text>

          <DashLine />
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              width: '90%',
              alignSelf: 'center',
            }}>
            <View style={{width: '30%'}}>
              <Text style={styles.date}>{_MyProjectsContractor.startDate}</Text>
              <Text style={styles.subDate}>12/12/2021</Text>
            </View>

            <View style={{width: '30%'}}>
              <Text style={styles.date}>{_MyProjectsContractor.duration}</Text>
              <Text style={styles.subDate}>
                {_MyProjectsContractor.shortTerm}
              </Text>
            </View>

            <View style={{width: '25%'}}>
              <Text style={styles.date}>{_MyProjectsContractor.budget}</Text>
              <Text style={styles.subDate}>₹ 5000</Text>
            </View>
          </View>
          <DashLine />

          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignSelf: 'center',
              width: '90%',
            }}>
            <View style={{width: '48%'}}>
              <Text style={styles.date}>{_MyProjectsContractor.project}</Text>
              <Text style={styles.subDate}>
                {_MyProjectsContractor.interior}
              </Text>
            </View>

            <View style={{width: '48%'}}>
              <Text style={styles.date}>
                {_MyProjectsContractor.jobLocation}
              </Text>
              <Text style={styles.subDate}>
                {_MyProjectsContractor.address}
              </Text>
            </View>
          </View>
        </View>
      </TouchableRipple>
    );
  };

  const categorySelect = index => {
    setSelectedTab(index);
    if (index == 0) {
      setProduct('ongoing');
    } else if (index == 1) {
      setComplete('completed');
    }
  };
  const handleRatingPopUp = () => {
    navigation.navigate('RatingContractor');
    setVisible(false);
  };

  return (
    <View style={{backgroundColor: '#F4F4F4', flex: 1}}>
      <StatusBarDark />
      <Header
        onPress={() => navigation.goBack()}
        title={_MyProjectsContractor.title}
      />
      <MaterialTabs
        items={[_MyProjectsContractor.myJobs, _MyProjectsContractor.completed]}
        selectedIndex={selectedTab}
        onChange={index => categorySelect(index)}
        barColor="#F2AD4B"
        indicatorColor="#FFF"
        activeTextColor="#FFF"
        inactiveTextColor="#FFF"
        indicatorHeight={4}
      />
      <ScrollView showsVerticalScrollIndicator={false}>
        {selectedTab == 0 && (
          <FlatList
            style={{width: '100%', marginTop: 3}}
            data={completedList}
            showsVerticalScrollIndicator={false}
            renderItem={renderItemCompleted}
          />
        )}
        {selectedTab == 1 && (
          <FlatList
            style={{width: '100%', marginTop: 3}}
            data={product}
            showsVerticalScrollIndicator={false}
            renderItem={renderItemMyJob}
          />
        )}

        <Dialog
          visible={visible}
          onTouchOutside={() => {
            setVisible({visible: true});
          }}>
          <DialogContent>
            <View style={{width: 250, backgroundColor: '#FFF'}}>
              <Text style={styles.rejectTextss}>
                {_MyProjectsContractor.otp}
              </Text>
              <View style={styles.otpView}>
                <OTPInputView
                  style={styles.otpInput}
                  pinCount={4}
                  // code={state.otp}
                  autoFocusOnLoad
                  codeInputFieldStyle={styles.underlineStyleBase}
                  codeInputHighlightStyle={styles.underlineStyleHighLighted}
                />
              </View>
              <TouchableRipple
                rippleColor="#6CBDFF20"
                onPress={() => handleRatingPopUp()}
                setVisible={false}
                // onPress={() => navigation.navigate('RatingContractor')}
                style={styles.touchii}>
                <Text style={styles.title}>{_MyProjectsContractor.ok}</Text>
              </TouchableRipple>
            </View>
          </DialogContent>
        </Dialog>
      </ScrollView>
    </View>
  );
};

export default MyProjectsContractor;

const styles = StyleSheet.create({
  otpView: {
    height: 120,
  },
  otpInput: {
    margin: 50,
    marginHorizontal: 1,
    width: '85%',
    alignSelf: 'center',
    height: 50,
  },
  underlineStyleBase: {
    backgroundColor: '#F4F4F4',
    fontSize: 22,
    color: '#000',
    fontFamily: 'Nunito-Bold',
    fontWeight: '700',
    width: 45,
    height: 45,
    borderWidth: 1,
    borderRadius: 10,
    //borderBottomWidth: 1,
  },

  underlineStyleHighLighted: {
    borderColor: '#2C2627',
    fontSize: 20,
    fontFamily: 'Nunito-Bold',
    fontWeight: '700',
  },
  sub3text: {
    fontFamily: 'Avenir-Normal',
    fontWeight: '400',
    fontSize: 15,
    color: '#606E87',
    opacity: 0.6,
    marginLeft: 100,
  },
  title: {
    fontSize: 18,
    color: '#FFFFFF',
    fontFamily: 'Avenir-Heavy',
    textAlign: 'center',
  },
  touchii: {
    alignSelf: 'center',
    backgroundColor: '#6CBDFF',
    width: '45%',
    borderRadius: 12,
    marginBottom: 10,
    paddingVertical: 6,
  },
  rejectTextss: {
    marginTop: 20,
    alignSelf: 'center',
    textAlign: 'center',
    marginHorizontal: 20,
    fontSize: 16,
    lineHeight: 20,
    fontFamily: 'Avenir-Heavy',
    color: '#000',
    fontWeight: '600',
    marginBottom: -30,
  },
  timeTextt: {
    fontFamily: 'Avenir-Normal',
    fontSize: 12,
    fontWeight: '400',
    marginRight: 5,
    color: '#8A94A3',
    marginLeft: 12,
    marginTop: 5,
  },
  timeText: {
    fontFamily: 'Avenir-Normal',
    fontSize: 12,
    fontWeight: '400',
    marginRight: 5,
    color: '#8A94A3',
    marginTop: 8,
  },
  jobView: {
    justifyContent: 'center',
    width: '60%',
  },
  jobViewcompleted: {
    justifyContent: 'center',
    width: '90%',
  },
  textdays: {
    alignSelf: 'center',
    color: '#FFF',
    marginVertical: 5,
    marginHorizontal: 20,
  },
  redDaysView: {
    height: 32,
    justifyContent: 'center',
    backgroundColor: '#FA6400',
    borderRadius: 20,
  },
  textShortBlack: {
    alignSelf: 'center',
    color: '#FFF',
    marginVertical: 7,
  },
  textViewDetailsWhite: {
    alignSelf: 'center',
    color: '#FFF',
    marginVertical: 7,
  },
  rowDataButtonBlue: {
    justifyContent: 'center',
    width: '47%',
    backgroundColor: '#6CBDFF',
    borderRadius: 20,
  },
  rowDataButton: {
    justifyContent: 'center',
    alignSelf: 'center',
    width: '50%',
    backgroundColor: '#E02020',
    borderRadius: 20,
    marginTop: 20,
    marginBottom: 10,
  },
  container: {
    padding: 10,
    backgroundColor: '#fff',
    borderRadius: 8,
    elevation: 5,
    width: '90%',
    alignSelf: 'center',
    marginVertical: 10,
    marginBottom: 10,
  },
  style: {backgroundColor: '#F2AD4B'},
  labelStyle: {
    fontSize: 15,
    fontFamily: 'Avenir-Medium',
    fontWeight: '500',
  },
  indicatorStyle: {backgroundColor: '#fff', height: 3},
  text: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 18,
    fontWeight: 'bold',
  },
  subText: {
    fontFamily: 'Avenir-Medium',
    fontSize: 12,
    fontWeight: '500',
    marginHorizontal: 10,
    marginTop: 5,
    color: '#454545',
  },
  date: {
    fontFamily: 'Avenir-Normal',
    fontSize: 12,
    fontWeight: '400',
    color: '#8A94A3',
  },
  subDate: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 13,
    fontWeight: 'bold',
    color: '#454545',
  },
  booking: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 14,
    fontWeight: 'bold',
    color: '#454545',
  },
  subBooking: {
    fontFamily: 'Avenir-Medium',
    fontSize: 12,
    fontWeight: '500',
    color: '#A5A5A5',
    marginTop: 10,
  },
  otptext: {
    fontFamily: 'Avenir',
    fontSize: 22,
    fontWeight: 'bold',
    color: '#333333',
    marginHorizontal: 50,
    marginTop: 20,
  },
});
const DashLine = props => (
  <View style={{margin: 10}}>
    <Dash dashColor={'#C8C8D3'} dashLength={5} dashThickness={1} {...props} />
  </View>
);
