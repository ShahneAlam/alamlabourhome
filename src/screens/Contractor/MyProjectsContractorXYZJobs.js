import React, {useEffect, useState} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  Image,
  Dimensions,
  TouchableOpacity,
  ImageBackground,
} from 'react-native';
import {StatusBarDark} from '../../Custom/CustomStatusBar';
import {Header} from '../../Custom/CustomView';
import Dash from 'react-native-dash';

const MyProjectsContractorXYZJobs = ({navigation}) => {
  return (
    <View style={{backgroundColor: '#FFF', flex: 1}}>
      <StatusBarDark />
      <Header
        onPress={() => navigation.goBack()}
        title={'Subscription Plans'}
      />
      <ScrollView>
        <View style={{flex: 1, backgroundColor: '#F4F4F4'}}>
          <View style={styles.container}>
            <View style={styles.jobView}>
              <Text style={styles.text}>Specification</Text>
            </View>

            <Text style={styles.timeTextt}>
              Lorem Ipsum is simply dummy text of the printing and typesetting
              industry. Lorem Ipsum has been the industry's standard dummy text
              ever since the 1500s, when an unknown printer took a galley of
              type and scrambled it to make a type specimen book.
            </Text>

            <DashLine />
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                width: '90%',
                alignSelf: 'center',
              }}>
              <View style={{width: '30%'}}>
                <Text style={styles.date}>Start Date</Text>
                <Text style={styles.subDate}>12/12/2021</Text>
              </View>

              <View style={{width: '30%'}}>
                <Text style={styles.date}>Job Duration</Text>
                <Text style={styles.subDate}>Short term</Text>
              </View>

              <View style={{width: '25%'}}>
                <Text style={styles.date}>Budget</Text>
                <Text style={styles.subDate}>₹ 5000</Text>
              </View>
            </View>
            <DashLine />

            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignSelf: 'center',
                width: '90%',
              }}>
              <View style={{width: '48%'}}>
                <Text style={styles.date}>Project Type</Text>
                <Text style={styles.subDate}>Interior Designing</Text>
              </View>

              <View style={{width: '48%'}}>
                <Text style={styles.date}>Job Location</Text>
                <Text style={styles.subDate}>Rohini, New delhi</Text>
              </View>
            </View>
            <DashLine />
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignSelf: 'center',
                width: '90%',
              }}>
              <View style={{width: '48%'}}>
                <Text style={styles.date}>No of Professionals Required</Text>
                <Text style={styles.subDate}>More than 10</Text>
              </View>

              <View style={{width: '48%'}}>
                <Text style={styles.date}>Area in sq ft</Text>
                <Text style={styles.subDate}>500 sq ft</Text>
              </View>
            </View>
            <DashLine />
            <View style={styles.jobView}>
              <Text style={styles.text}>Specification</Text>
            </View>

            <Text style={styles.timeTextt}>
              Lorem Ipsum is simply dummy text of the printing and typesetting
              industry. Lorem Ipsum has been the industry's standard dummy text
              ever since the 1500s, when an unknown printer took a galley of
              type and scrambled it to make a type specimen book.
            </Text>
            <DashLine />
            <Text style={styles.textLeft}>Site Images/Videos</Text>
            <View style={styles.rowSecondData}>
              <Image
                style={{
                  width: 90,
                  height: 80,
                  resizeMode: 'contain',
                  borderRadius: 10,
                }}
                source={require('../../images/Group2.png')}
              />
              <ImageBackground
                imageStyle={styles.bgImageStyle}
                source={require('../../images/Group2.png')}>
                <Image
                  style={{height: 35, width: 35, marginLeft: 43, marginTop: 23}}
                  source={require('../../images/playbtn.png')}
                />
              </ImageBackground>
            </View>
            <DashLine />
          </View>
        </View>
      </ScrollView>
    </View>
  );
};

export default MyProjectsContractorXYZJobs;

const styles = StyleSheet.create({
  bgImageStyle: {
    width: 90,
    height: 80,
    marginLeft: 15,
    resizeMode: 'contain',
    borderRadius: 10,
  },
  imageDataa: {
    width: 50,
    alignSelf: 'center',
    height: 50,
    resizeMode: 'contain',
  },
  rowSeconddata: {
    flexDirection: 'row',
    width: '87%',
    alignSelf: 'center',
    marginVertical: 10,
    marginTop: 10,
  },
  rowSecondData: {
    flexDirection: 'row',
    width: '90%',
    alignSelf: 'center',
    marginTop: 10,
  },
  textLeft: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 14,
    fontWeight: 'bold',
    fontWeight: '800',
    lineHeight: 18,
    marginLeft: 15,
  },
  alreadyText: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 15,
    fontWeight: 'bold',
    fontWeight: '800',
    lineHeight: 20,
    marginTop: 14,
    marginLeft: 12,
  },
  touchGreen: {
    marginTop: 20,
    marginBottom: 30,
    alignSelf: 'center',
    justifyContent: 'center',
    width: '90%',
    backgroundColor: '#6DD400',
    borderRadius: 20,
  },
  text: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 18,
    fontWeight: 'bold',
  },
  subText: {
    fontFamily: 'Avenir-Medium',
    fontSize: 12,
    fontWeight: '500',
    marginHorizontal: 10,
    marginTop: 5,
    color: '#454545',
  },
  date: {
    fontFamily: 'Avenir-Normal',
    fontSize: 12,
    fontWeight: '400',
    color: '#8A94A3',
  },
  subDate: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 13,
    fontWeight: 'bold',
    color: '#454545',
  },
  booking: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 14,
    fontWeight: 'bold',
    color: '#454545',
  },
  timeTextt: {
    fontFamily: 'Avenir-Normal',
    fontSize: 12,
    fontWeight: '400',
    marginRight: 5,
    color: '#8A94A3',
    marginLeft: 15,
    lineHeight: 20,
    marginTop: 3,
    textAlign: 'justify',
  },
  timeText: {
    fontFamily: 'Avenir-Normal',
    fontSize: 12,
    fontWeight: '400',
    marginRight: 5,
    color: '#8A94A3',
    marginTop: 8,
  },
  jobView: {
    alignSelf: 'center',
    width: '92%',
  },
  textdays: {
    alignSelf: 'center',
    color: '#FFF',
    marginVertical: 7,
    marginHorizontal: 10,
  },
  redDaysView: {
    justifyContent: 'center',
    backgroundColor: '#E02020',
    borderRadius: 10,
    height: 36,
  },
  textShortBlack: {
    alignSelf: 'center',
    color: '#6D7278',
    marginVertical: 7,
  },
  textViewDetailsWhite: {
    alignSelf: 'center',
    color: '#FFF',
    marginVertical: 8,
  },
  rowDataButtonBlue: {
    justifyContent: 'center',
    width: '47%',
    backgroundColor: '#6CBDFF',
    borderRadius: 20,
  },
  rowDataButton: {
    justifyContent: 'center',
    width: '47%',
    backgroundColor: '#E02020',
    borderRadius: 20,
  },
  container: {
    padding: 10,
    backgroundColor: '#fff',
    borderRadius: 8,
    marginTop: 2,
  },
  touchtext: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 16,
    fontWeight: 'bold',
    alignSelf: 'center',
    color: '#ffffff',
    marginTop: 5,
  },
});
const DashLine = props => (
  <View style={{margin: 10}}>
    <Dash dashColor={'#C8C8D3'} dashLength={5} dashThickness={1} {...props} />
  </View>
);
