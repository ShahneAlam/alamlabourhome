import React, {useEffect, useState} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  FlatList,
  Linking,
  Platform,
  Image,
  Dimensions,
  TouchableOpacity,
} from 'react-native';
import {StatusBarDark} from '../Custom/CustomStatusBar';
import {Header} from '../Custom/CustomView';
import stringsoflanguages from '../language';
import {Api, LocalStorage} from '../services/Api';
const {height} = Dimensions.get('window');
import {useDispatch, useSelector} from 'react-redux';
import {Loader} from '../screens/AuthScreen/Loader';
import {useIsFocused} from '@react-navigation/native';

const CallList = ({navigation, route}) => {
  const {user_id} = useSelector(store => store);
  let user = JSON.stringify(LocalStorage.getUserId());
  let parsed = JSON.parse(user);

  const [product, setProduct] = useState([]);
  const isFocused = useIsFocused();
  const [state, setState] = useState({
    isLoading: false,
    ...route.params,
  });
  const {header} = route.params;

  const {_callList} = stringsoflanguages;

  useEffect(() => {
    GetJobsHandler();
  }, [isFocused]);

  const toggleLoader = isLoading => setState({...state, isLoading});
  const GetJobsHandler = async () => {
    const body = {
      id: route.params.id,
    };
    toggleLoader(true);
    const response = await Api.GetjobApi(body);
    const {status = false, worker_list = []} = response;
    setProduct(worker_list);
    toggleLoader(false);
    // setState({...state, worker_list, isLoading: false});
  };

  useEffect(() => {
    // alert(JSON.stringify(header));
  }, []);
  const bookingNowHandler = async (item, index) => {
    const body = {
      user_id: user_id,
      worker_id: item.id,
    };
    const response = await Api.BookingCustomerApi(body);
    // alert(JSON.stringify(response, null, 2));
    // console.log(JSON.stringify(response, null, 2));
    const {status = false} = response;
    if (status == true) {
      navigation.navigate('Success');
    } else {
      alert('Something Went Wrong');
    }
  };

  const renderItemListData = ({item, index}) => {
    return (
      <View>
        <View
          style={{
            padding: 10,
            marginHorizontal: 20,
            backgroundColor: '#fff',
            elevation: 5,
            marginTop: 20,
            borderRadius: 10,
          }}>
          <View style={{flexDirection: 'row'}}>
            <Image
              style={styles.imagebox}
              source={{uri: item.image}}
              // source={require('../images/photo.png')}
            />

            <View
              style={{
                width: '65%',
                flexDirection: 'column',
              }}>
              <Text style={styles.inputtxt}>{item.name}</Text>
              <View
                style={{
                  width: '60%',
                  marginTop: 5,
                  flexDirection: 'row',
                }}>
                <Image
                  style={styles.sub3image}
                  source={require('../images/star.png')}
                />
                <Image
                  style={styles.sub4image}
                  source={require('../images/star.png')}
                />
                <Image
                  style={styles.sub5image}
                  source={require('../images/star.png')}
                />
              </View>
              <Text style={styles.subtext}>
                {_callList.exp}
                {item.experience}
              </Text>
              <Text style={styles.subtext}>
                {_callList.daily}{' '}
                <Text style={{color: '#6CBDFF'}}>₹{item.daily_rate}</Text>
              </Text>
              <Text style={styles.subtext}>
                {_callList.hourlyRate}{' '}
                <Text style={{color: '#6CBDFF'}}>₹{item.hourly_rate}</Text>
              </Text>
            </View>
          </View>
          <View
            style={{
              flexDirection: 'row',
              marginBottom: 10,
              justifyContent: 'space-evenly',
            }}>
            <TouchableOpacity
              style={styles.touch}
              onPress={() => {
                let phoneNumber = item.mobile;
                if (Platform.OS === 'android') {
                  phoneNumber = `tel:${phoneNumber}`;
                } else {
                  phoneNumber = `telprompt:${phoneNumber}`;
                }
                Linking.openURL(phoneNumber);
              }}>
              <Text style={styles.touchtext}>{_callList.call}</Text>
            </TouchableOpacity>

            <TouchableOpacity
              style={styles.touch2}
              onPress={() => bookingNowHandler(item, index)}>
              <Text style={styles.touchtext}>{_callList.book}</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  };

  useEffect(() => {
    // alert(JSON.stringify(state));
  }, []);
  return (
    <>
      <View style={{backgroundColor: '#F4F4F4', flex: 1}}>
        <StatusBarDark />
        {/* <Loader status={state.isLoading} /> */}
        <Header onPress={() => navigation.goBack()} title={header} />
        <ScrollView>
          {/* <View style={styles.container}>
          <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
            <Text style={styles.text}>{_callList.callRemaining}</Text>
            <Text style={styles.text}>{_callList.calls}</Text>
          </View>
        </View> */}

          <ScrollView showsVerticalScrollIndicator={false}>
            <FlatList
              style={{width: '100%', marginTop: 3}}
              data={product}
              showsVerticalScrollIndicator={false}
              renderItem={renderItemListData}
            />
          </ScrollView>

          <Image
            style={{width: 141, height: 70, marginLeft: 'auto', marginTop: 20}}
            source={require('../images/support.png')}
          />
        </ScrollView>
      </View>
    </>
  );
};

export default CallList;

const styles = StyleSheet.create({
  container: {
    padding: 10,
    width: '100%',
    backgroundColor: '#F2AD4B',
    // marginTop: 10,
  },
  text: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 14,
    fontWeight: 'bold',
    color: '#fff',
    marginHorizontal: 20,
  },
  imagebox: {
    resizeMode: 'contain',
    // marginTop: 40,
    // marginHorizontal: 20,
    height: 100,
    width: 100,
  },
  inputtxt: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 18,
    fontWeight: 'bold',
    color: '#080040',
  },
  sub3image: {
    resizeMode: 'contain',
    height: 14,
    width: 14,
  },
  sub4image: {
    resizeMode: 'contain',
    height: 14,
    width: 14,
    marginHorizontal: 5,
  },
  sub5image: {
    resizeMode: 'contain',
    height: 14,
    width: 14,
  },
  subtext: {
    fontFamily: 'Avenir-Normal',
    fontSize: 12,
    fontWeight: '400',
    color: '#8A94A3',
    marginTop: 5,
  },
  touch: {
    paddingHorizontal: 50,
    borderRadius: 25,
    backgroundColor: '#6DD400',
    marginTop: 20,
    alignSelf: 'center',
  },
  touch2: {
    paddingHorizontal: 30,
    borderRadius: 25,
    backgroundColor: '#6CBDFF',
    marginTop: 20,
    alignSelf: 'center',
  },
  touchtext: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 16,
    fontWeight: 'bold',
    alignSelf: 'center',
    color: '#ffffff',
    marginTop: 5,
    marginBottom: 7,
  },
});
