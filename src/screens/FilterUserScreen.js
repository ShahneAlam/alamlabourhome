import React, {useEffect, useState} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  Image,
  TextInput,
  Dimensions,
  TouchableOpacity,
  FlatList,
} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

import {Header} from '../Custom/CustomView';
import stringsoflanguages from '../language';
const {height} = Dimensions.get('window');
import {Api} from '../services/Api';
import {StatusBarDark} from '../Custom/CustomStatusBar';

const FilterUserScreen = ({navigation}) => {
  const [search, setSearch] = useState('');
  const [list, setList] = useState([]);
  const {_SubscriptionPlanContractor, _privacyPolicy} = stringsoflanguages;
  const [state, setState] = useState({
    private_police: '',
    isLoading: false,
  });

  const searchHandeler = async text => {
    setSearch(text);
    if (text == '') {
      return;
    }
    const body = {
      key: text,
      offset: '0',
      limit: '50',
    };
    const response = await Api.SearchlistsUserApi(body);
    // alert(JSON.stringify(response, null, 2));
    // console.log(JSON.stringify(response, null, 2));
    const {status = false, all_trades = [], all_worker = []} = response;
    if (status == true) {
      setList(all_worker);
    } else {
      alert('Something went wrong');
    }
  };

  return (
    <View style={{backgroundColor: '#F4F4F4', flex: 1}}>
      <StatusBarDark />
      <Header onPress={() => navigation.goBack()} title={'Search'} />
      <ScrollView>
        <SafeAreaView style={[styles.plan]}>
          <View
            style={{
              borderBottomColor: '#C8C8D3',
              marginHorizontal: 20,
              borderBottomWidth: 0.4,
            }}>
            <TextInput
              placeholder="Search"
              keyboardType={'default'}
              style={styles.inputTypTextOffCss}
              onChangeText={text => searchHandeler(text)}
              value={search}
            />
          </View>

          <ScrollView>
            <FlatList
              numColumns={1}
              data={list}
              renderItem={({item, index}) => (
                <>
                  <TouchableOpacity
                    activeOpacity={0.9}
                    onPress={() =>
                      navigation.navigate('CallList', {
                        id: item.id,
                        header: item.name,
                      })
                    }>
                    <SafeAreaView style={styles.plan}>
                      <View
                        style={{
                          marginVertical: 20,
                          flexDirection: 'row',
                          alignItems: 'center',
                        }}>
                        <Image
                          style={styles.tickimg}
                          source={{uri: item.image}}
                          //source={require('../../images/tick.png')}
                        />
                        <View
                          style={{
                            flexDirection: 'column',
                            width: '80%',
                            marginLeft: 5,
                          }}>
                          <View
                            style={{
                              flexDirection: 'row',
                            }}>
                            <Text style={styles.ticktext}>Name:-</Text>
                            <Text
                              style={{
                                marginTop: 4,
                                fontSize: 15,
                                marginLeft: 10,
                                color: '#00000080',
                              }}>
                              {item.name}
                            </Text>
                          </View>
                          <View
                            style={{
                              flexDirection: 'row',
                            }}>
                            <Text style={styles.ticktext}>Work:- </Text>
                            <Text
                              style={{
                                marginTop: 4,
                                fontSize: 15,
                                marginLeft: 10,
                                color: '#00000080',
                              }}>
                              {item.trade_name}
                            </Text>
                          </View>
                        </View>
                      </View>
                    </SafeAreaView>
                  </TouchableOpacity>
                </>
              )}
            />
          </ScrollView>
        </SafeAreaView>
      </ScrollView>
    </View>
  );
};

export default FilterUserScreen;

const styles = StyleSheet.create({
  inputTypTextOffCss: {
    fontFamily: 'Avenir-Heavy',
    fontWeight: 'bold',
    fontSize: 20,
    color: '#000000',
  },
  plan: {
    width: '100%',
    marginVertical: 10,
    backgroundColor: '#fff',
    elevation: 5,
    marginTop: 10,
    marginBottom: 5,
  },
  subplan: {
    backgroundColor: '#ECEFF1',
    borderTopEndRadius: 10,
    borderTopLeftRadius: 10,
  },
  plantext: {
    fontFamily: 'Avenir-Heavy',
    fontWeight: 'bold',
    fontSize: 20,
    color: '#333333',
    textAlign: 'center',
    marginTop: 10,
  },
  subplantext: {
    fontFamily: 'Avenir-Heavy',
    fontWeight: 'bold',
    fontSize: 30,
    color: '#333333',
    textAlign: 'center',
    marginTop: 10,
  },
  tickimg: {
    marginLeft: 15,
    borderRadius: 20,
    resizeMode: 'contain',
    width: 40,
    height: 40,
  },
  ticktext: {
    fontFamily: 'Avenir-Medium',
    fontWeight: '800',
    fontSize: 19,
    color: '#000',
  },
  touch: {
    width: 141,
    height: 35,
    borderRadius: 25,
    backgroundColor: '#6CBDFF',
    marginTop: 30,
    marginBottom: 10,
    alignSelf: 'center',
  },
  touchtext: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 16,
    fontWeight: 'bold',
    alignSelf: 'center',
    color: '#ffffff',
    marginTop: 5,
  },
});
