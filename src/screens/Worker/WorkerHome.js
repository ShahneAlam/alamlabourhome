import React, {useEffect, useState, useCallback, useRef} from 'react';
import {
  ScrollView,
  ImageBackground,
  StyleSheet,
  Text,
  View,
  Image,
  Switch,
  TouchableOpacity,
  FlatList,
} from 'react-native';
import moment from 'moment';
import AppHeader from '../../Custom/CustomAppHeader';
import {StatusBarDark} from '../../Custom/CustomStatusBar';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {ButtonStyle} from '../../Custom/CustomView';
import CustomModal from '../../Custom/CustomModal';
import {Api} from '../../services/Api';
import {Loader} from '../AuthScreen/Loader';

const WorkerHome = ({navigation, route}) => {
  const [state, setState] = useState({
    switchValue: true ? 1 : 2,
    isLoading: false,
  });
  const toggleLoader = isLoading => setState({...state, isLoading});

  const [jobRequestsList, setJobRequestsList] = useState([]);
  const [profileData, setprofileData] = useState([]);
  const [isEnabled, setIsEnabled] = useState(true);
  const [modalOpen, setModalOpen] = useState(false);
  const [videoTutorialData, setVideoTutorialData] = useState([
    {title: 'How to find and submit quotation?', timeing: '10 min'},
    {title: 'How to find and submit quotation?', timeing: '15 min'},
    {title: 'How to find and submit quotation?', timeing: '10 min'},
    {title: 'How to find and submit quotation?', timeing: '15 min'},
  ]);

  useEffect(() => {
    const willFocusSubscription = navigation.addListener('focus', () => {});
    return willFocusSubscription;
  }, [navigation]);

  useEffect(() => {
    homePageHandler();
  }, []);

  const homePageHandler = async () => {
    toggleLoader(true);
    const response = await Api.WorkerHomeApi();
    toggleLoader(false);
    // alert(JSON.stringify(response, null, 2));
    //  console.log(JSON.stringify(response, null, 2));
    const {
      status = false,
      my_profile = [],
      my_booking = [],
      video_tutorial = [],
    } = response;
    // alert(JSON.stringify(my_booking, null, 2));
    console.log(JSON.stringify(my_booking, null, 2));
    if (status == true) {
      setJobRequestsList(my_booking);
      setprofileData(my_profile[0]);
    } else {
      alert('Something went wrong');
    }
  };

  const acceptJobRequestHandler = async (item, index) => {
    const body = {
      id: item.id,
      status: 1,
    };
    const response = await Api.AcceptButtonWorkerApi(body);
    // alert(JSON.stringify(response, null, 2));
    const {status = false} = response;
    if (status == true) {
      homePageHandler();
      acceptModal(true);
    } else {
      alert('Something went wrong');
    }
  };

  const toggleSwitch = async () => {
    const body = {
      online_status: isEnabled == false ? '1' : '2',
    };
    // alert(JSON.stringify(body, null, 2));
    //   console.log(JSON.stringify(body, null, 2));
    const response = await Api.SwitchButtonWorkerApi(body);
    // alert(JSON.stringify(response, null, 2));
    const {status = false} = response;
    if (status == true) {
      setIsEnabled(previousState => !previousState);
    } else {
      alert('Something went wrong');
    }
  };

  const acceptModal = visible => {
    setModalOpen(visible);
  };

  const jobRequestsAccept = () => {
    acceptModal(false);
    navigation.navigate('WorkerMyBooking');
  };

  const videosTutorialList = ({item, index}) => {
    return (
      <View style={{width: wp(40), marginLeft: 14}}>
        <TouchableOpacity activeOpacity={0.8}>
          <ImageBackground
            style={styles.videoIconOffCss}
            source={require('../../images/Group2.png')}>
            <Image
              style={styles.playIconOffCss}
              source={require('../../images/play.png')}
            />
          </ImageBackground>
        </TouchableOpacity>
        <Text style={styles.playtext}>{item.title}</Text>
        <Text style={styles.subplaytext}>{item.timeing}</Text>
      </View>
    );
  };

  const JobRequestslRender = ({item, index}) => {
    return (
      <>
        <View style={{width: '100%', alignSelf: 'center'}}>
          <View style={styles.jobRequestsViewCardOffCss}>
            <View
              style={{
                paddingVertical: 10,
                borderBottomColor: '#C8C8D3',
                borderBottomWidth: 0.3,
              }}>
              <Text style={styles.bookingIDTextOffCss}>Booking #{item.id}</Text>
              <Text style={styles.dateTimeTextOffCss}>
                {item.booking_date_time}
              </Text>
              {/* {moment(dateTime).format("MMMM Do YYYY, h:mm:ss a")} */}
            </View>
            <View
              style={{
                paddingVertical: 10,
                flexDirection: 'row',
                alignItems: 'center',
                borderBottomColor: '#C8C8D3',
                borderBottomWidth: 0.4,
              }}>
              <Image
                style={styles.jobUserImgOffCss}
                source={{uri: item.profile_pic}}
              />
              <Text style={styles.jobUserNameTextOffCss}>{item.user_name}</Text>
            </View>
            <View style={{paddingVertical: 10}}>
              <Text
                style={[
                  styles.jobUserNameTextOffCss,
                  {fontSize: hp(1.65), marginLeft: 0, marginBottom: 5},
                ]}>
                Site Address
              </Text>
              <Text style={styles.siteAddreTextOffCss}>
                1007, 10th Floor, Tower-1, Pearls Omaxe Building, Netaji Subhash
                Place, Pitam Pura, New Delhi 110034 (IN)
              </Text>
            </View>
            <View style={{paddingVertical: 10, marginBottom: 5}}>
              <ButtonStyle
                height={hp(4.1)}
                fontSize={hp(1.65)}
                title={'ACCEPT'}
                bgColor={'#6DD400'}
                marginHorizontal={wp(25)}
                onPress={() => acceptJobRequestHandler(item, index)}
              />
            </View>
          </View>
        </View>
      </>
    );
  };

  moment.locale('en');
  var dateTime = 'Monday, 2 Sep 21 10:30 am';
  return (
    <View style={styles.continer}>
      <StatusBarDark bg="#FFF" />
      {/* <Loader status={state.isLoading} /> */}
      <AppHeader
        title={profileData.name}
        smailTitle={profileData.project_type}
        titlePaddingHorizontal={8}
        elevation={0.1}
        onClickSmailTitle={() => {
          alert('Worker');
        }}
        leftIcon={require('../../images/home_logo.png')}
        alrm={require('../../images/notification.png')}
        alrmOnClick={() => {}}
        leftOnClick={() => {}}
      />
      <ScrollView
        nestedScrollEnabled={true}
        contentContainerStyle={{flexGrow: 1}}
        showsVerticalScrollIndicator={false}>
        {/* <View style={styles.newJobViewOffCss}>
          <Text
            style={[
              styles.newJobTextOffCss,
              {fontWeight: '900', fontSize: hp(2)},
            ]}>
            15+
            <Text style={styles.newJobTextOffCss}>
              {' '}
              Plumbers got the job today in your area
            </Text>
          </Text>
        </View> */}
        <View style={{flex: 1, paddingVertical: 15}}>
          <View style={styles.availabilityStatusViewOffCss}>
            <View
              style={{
                flex: 1,
                justifyContent: 'center',
                paddingHorizontal: 20,
              }}>
              <Text style={styles.availabliStatusTextOffCss}>
                Availability status
              </Text>
              <Text style={styles.availabliTextOffCss}>Available</Text>
            </View>
            <View
              style={{
                flex: 0.4,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Switch
                disabled={true} ///only for this time fix otherwise it will be remove
                trackColor={{false: '#767577', true: '#4DD865'}}
                thumbColor={isEnabled ? '#FFF' : '#FFF'}
                ios_backgroundColor="#3e3e3e"
                onValueChange={() => {
                  toggleSwitch();
                }}
                value={isEnabled}
                style={{transform: [{scaleX: 1.2}, {scaleY: 1.2}]}}
              />
            </View>
          </View>
          <Text style={styles.jobRequestsHeadingTextOffCss}>Job Requests</Text>
          <View style={{marginBottom: 10}}>
            <FlatList
              data={jobRequestsList}
              renderItem={JobRequestslRender}
              showsVerticalScrollIndicator={false}
            />
          </View>

          <CustomModal
            buttonTitle={'OK'}
            titleFontSize={22}
            buttonRadius={17.5}
            buttonHeight={hp(5)}
            openVisible={modalOpen}
            buttonWidth={wp(30.5)}
            buttonfontSize={hp(1.9)}
            title={'Request Accepted'}
            modalImg={require('../../images/mark.png')}
            openModal={value => {
              acceptModal(value);
            }}
            onclickPress={() => {
              jobRequestsAccept();
            }}
          />
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              marginHorizontal: 15,
            }}>
            <TouchableOpacity
              activeOpacity={0.7}
              onPress={() => {
                navigation.navigate('WorkerMyBooking');
              }}
              style={[styles.bookingViewOffCss, {marginRight: 10}]}>
              <Image
                style={styles.groupBookingImgOffCss}
                source={require('../../images/Group.png')}
              />
              <View style={{flex: 1, justifyContent: 'flex-end'}}>
                <Text style={styles.bookingTextOffCss}>My Bookings</Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity
              activeOpacity={0.7}
              onPress={() => {
                navigation.navigate('WorkerMyAccount');
              }}
              style={styles.bookingViewOffCss}>
              <Image
                style={styles.groupBookingImgOffCss}
                source={require('../../images/Group_3.png')}
              />
              <View style={{flex: 1, justifyContent: 'flex-end'}}>
                <Text style={styles.bookingTextOffCss}>My Account</Text>
              </View>
            </TouchableOpacity>
          </View>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              marginHorizontal: 15,
            }}>
            <Text style={styles.videoTutorialTextOffCss}>Video Tutorial</Text>
            <TouchableOpacity
              // onPress={() => navigation.navigate('')}
              activeOpacity={0.7}
              style={{flexDirection: 'row', alignItems: 'center'}}>
              <Text style={styles.showAllTextOffCss}>Show all</Text>
              <Image
                style={styles.showAllIconOffCss}
                source={require('../../images/video.png')}
              />
            </TouchableOpacity>
          </View>
          <View style={{marginBottom: 10}}>
            <FlatList
              horizontal
              data={videoTutorialData}
              renderItem={videosTutorialList}
              showsHorizontalScrollIndicator={false}
            />
          </View>
          <View style={styles.priceJobChangeViewOffCss}>
            <View style={styles.proceJovRowViewOffCss}>
              <View style={{flex: 0.1}}>
                <Image
                  style={styles.infoImgOffCss}
                  source={require('../../images/info.png')}
                />
              </View>
              <View style={{flex: 1, paddingLeft: 10}}>
                <Text
                  style={[
                    styles.newJobTextOffCss,
                    {color: '#F2AD4B', fontSize: hp(1.65)},
                  ]}>
                  The average price for a plumber job in your area is
                </Text>
                <Text
                  style={[
                    styles.bookingTextOffCss,
                    {color: '#F2AD4B', paddingVertical: 5},
                  ]}>
                  ₹500/day
                </Text>
              </View>
            </View>
            <Text
              style={[
                styles.newJobTextOffCss,
                {color: '#E02020', fontSize: hp(1.45)},
              ]}>
              *Change your price to get more jobs
            </Text>
          </View>
        </View>
      </ScrollView>
    </View>
  );
};

export default WorkerHome;

const styles = StyleSheet.create({
  continer: {
    flex: 1,
    backgroundColor: '#FFF',
  },
  newJobViewOffCss: {
    height: hp(5.5),
    backgroundColor: '#6CBDFF',
    paddingHorizontal: 30,
    justifyContent: 'center',
    alignItems: 'center',
  },
  newJobTextOffCss: {
    color: '#FFFFFF',
    fontSize: hp(1.9),
    fontFamily: 'Avenir-Meduim',
    fontWeight: '500',
  },
  availabilityStatusViewOffCss: {
    backgroundColor: '#F2AD4B',
    height: hp(11),
    borderRadius: 8,
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginHorizontal: 15,
  },
  availabliStatusTextOffCss: {
    color: '#FFFFFF',
    fontSize: hp(2.5),
    fontFamily: 'Avenir-Meduim',
    fontWeight: '500',
    marginBottom: 5,
  },
  availabliTextOffCss: {
    color: '#FFFFFF',
    fontSize: hp(2.2),
    fontFamily: 'Avenir-Meduim',
    fontWeight: '900',
  },
  jobRequestsHeadingTextOffCss: {
    color: '#454545',
    fontSize: hp(2.5),
    fontFamily: 'Avenir-Heavy',
    fontWeight: '900',
    marginVertical: 10,
    marginHorizontal: 15,
  },
  jobRequestsViewCardOffCss: {
    backgroundColor: '#FFF',
    elevation: 1.5,
    borderRadius: 8,
    paddingHorizontal: 15,
    marginVertical: 10,
    marginHorizontal: 15,
    shadowOffset: {
      width: 0,
      height: 10,
    },
    shadowOpacity: 0.7,
    //   shadowRadius: 60,
    shadowColor: Platform.OS == 'ios' ? '#00000060' : '#000000',
  },
  bookingIDTextOffCss: {
    color: '#454545',
    fontSize: hp(1.9),
    fontFamily: 'Avenir-Heavy',
    fontWeight: '900',
    marginBottom: 2,
  },
  dateTimeTextOffCss: {
    color: '#A5A5A5',
    fontSize: hp(1.4),
    fontFamily: 'Avenir-Meduim',
    fontWeight: '500',
  },
  jobUserImgOffCss: {
    width: wp(13.9),
    height: hp(6.9),
    borderRadius: 50 / 2,
    resizeMode: 'contain',
  },
  jobUserNameTextOffCss: {
    color: '#454545',
    fontSize: hp(1.9),
    fontFamily: 'Avenir-Heavy',
    fontWeight: '900',
    marginLeft: 20,
  },
  siteAddreTextOffCss: {
    color: '#333333',
    fontSize: hp(1.65),
    fontFamily: 'Avenir-Meduim',
    fontWeight: '500',
    opacity: 0.6,
  },
  bookingViewOffCss: {
    width: wp(44),
    height: 120,
    backgroundColor: '#FFF',
    elevation: 3,
    marginBottom: 5,
    borderRadius: 8,
    marginVertical: 10,
    paddingHorizontal: 15,
    paddingVertical: 10,
    shadowOffset: {
      width: 0,
      height: 10,
    },
    shadowOpacity: 0.7,
    //   shadowRadius: 60,
    shadowColor: Platform.OS == 'ios' ? '#00000060' : '#000000',
  },
  groupBookingImgOffCss: {
    width: wp(13.9),
    height: hp(6.9),
    resizeMode: 'contain',
  },
  bookingTextOffCss: {
    color: '#454545',
    fontSize: hp(2),
    fontFamily: 'Avenir-Heavy',
    fontWeight: '900',
  },
  videoTutorialTextOffCss: {
    color: '#454545',
    fontSize: hp(2.5),
    fontFamily: 'Avenir-Heavy',
    fontWeight: '900',
    marginVertical: 10,
  },
  showAllTextOffCss: {
    color: '#6CBDFF',
    fontSize: hp(1.9),
    fontFamily: 'Avenir-Meduim',
    fontWeight: '500',
  },
  showAllIconOffCss: {
    width: 6,
    height: 10,
    resizeMode: 'contain',
    marginLeft: 5,
  },
  priceJobChangeViewOffCss: {
    height: hp(11),
    backgroundColor: '#FDEED9',
    borderRadius: 8,
    borderWidth: 0.7,
    borderColor: '#F2AD4B',
    paddingHorizontal: 10,
    paddingVertical: 10,
    marginHorizontal: 15,
  },
  proceJovRowViewOffCss: {
    flexDirection: 'row',
  },
  infoImgOffCss: {
    width: wp(8),
    height: hp(3.6),
    resizeMode: 'contain',
    tintColor: '#F2AD4B',
  },
  videoIconOffCss: {
    width: wp(40),
    height: hp(15),
    backgroundColor: '#00000050',
    elevation: 5,
    resizeMode: 'contain',
    borderRadius: 15,
  },
  playIconOffCss: {
    width: wp(8),
    height: hp(4),
    resizeMode: 'contain',
    alignSelf: 'center',
    marginTop: hp(5),
  },
  playtext: {
    fontFamily: 'Avenir-Heavy',
    fontSize: hp(1.75),
    fontWeight: 'bold',
    color: '#333333',
    paddingVertical: 5,
  },
  subplaytext: {
    fontFamily: 'Avenir-Heavy',
    fontSize: hp(1.6),
    fontWeight: 'bold',
    color: '#33333390',
  },
});
