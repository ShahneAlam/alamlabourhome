import React, {useEffect, useState} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  Image,
  Alert,
  Dimensions,
  TouchableOpacity,
} from 'react-native';
import {StatusBarDark} from '../../Custom/CustomStatusBar';
import {ButtonStyle, HeaderLight} from '../../Custom/CustomView';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {Api, LocalStorage} from '../../services/Api';
import {_SetAuthToken, _RemoveAuthToken} from '../../services/ApiSauce';
import {useDispatch, useSelector, useStore} from 'react-redux';
import {useIsFocused} from '@react-navigation/native';

const WorkerMyAccount = ({navigation}) => {
  const {user_id} = useSelector(store => store);
  const isFocused = useIsFocused();
  const [profileData, setProfileData] = useState('');

  const onLogoutHandler = () => {
    Alert.alert(
      'Logout',
      `Do you want to logout.`,
      [
        {
          text: 'No',
          //  onPress: navigation.closeDrawer,
          style: 'cancel',
        },
        {text: 'Yes', onPress: logouthandler},
      ],
      {cancelable: false},
    );
  };

  const logouthandler = async () => {
    const response = await Api.LogoutApi();
    // alert(JSON.stringify(response));
    const {status = false} = response;
    if (status) {
      LocalStorage.setToken('');
      _RemoveAuthToken();
      navigation.reset({index: 0, routes: [{name: 'Splash'}]});
    }
  };

  useEffect(() => {
    GetJobsHandler();
  }, [isFocused]);

  const GetJobsHandler = async () => {
    const body = {
      user_id: user_id,
    };
    const response = await Api.GetWorkerRegisterDataApi(body);
    // alert(JSON.stringify(response, null, 2));
    console.log(JSON.stringify(response, null, 2));
    const {status = false, user, address_list = []} = response;
    if (status == true) {
      setProfileData(user);
      // setList(user.address_list);
    } else {
      alert('Something went wrong');
    }
  };

  return (
    <View style={{backgroundColor: '#fff', flex: 1}}>
      <StatusBarDark bg="#6CBDFF" barStyle={'light-dark'} />
      <HeaderLight
        onPress={() => {
          navigation.goBack();
        }}
      />
      <View style={styles.cardViewOffCss}>
        <View style={styles.userImgViewOffCss}>
          <Image
            style={styles.userImgOffCss}
            source={{uri: profileData.profile}}
          />
        </View>
        <Text style={styles.userNameTextOffCss}>{profileData.name}</Text>
        <Text style={styles.userNumberTextOffCss}>{profileData.mobile}</Text>
      </View>
      <View style={{paddingHorizontal: 20, marginVertical: 10}}>
        <TouchableOpacity
          activeOpacity={0.7}
          style={styles.rowViewOffCss}
          onPress={() => {
            navigation.navigate('WorkerProfile');
          }}>
          <View style={{flexDirection: 'row'}}>
            <Image
              style={{width: 20, height: 20, resizeMode: 'contain'}}
              source={require('../../images/profile.png')}
            />
            <Text style={[styles.userNumberTextOffCss, {marginLeft: 15}]}>
              My Profile
            </Text>
          </View>
          <Image
            style={{width: 10, height: 15, resizeMode: 'contain'}}
            source={require('../../images/arrow.png')}
          />
        </TouchableOpacity>
        <TouchableOpacity
          activeOpacity={0.7}
          style={styles.rowViewOffCss}
          onPress={() => {
            navigation.navigate('WorkerHelp');
          }}>
          <View style={{flexDirection: 'row'}}>
            <Image
              style={{width: 20, height: 20, resizeMode: 'contain'}}
              source={require('../../images/help.png')}
            />
            <Text style={[styles.userNumberTextOffCss, {marginLeft: 15}]}>
              Help
            </Text>
          </View>
          <Image
            style={{width: 10, height: 15, resizeMode: 'contain'}}
            source={require('../../images/arrow.png')}
          />
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => {
            navigation.navigate('WorkerPrivacy');
          }}
          activeOpacity={0.7}
          style={styles.rowViewOffCss}>
          <View style={{flexDirection: 'row'}}>
            <Image
              style={{width: 20, height: 20, resizeMode: 'contain'}}
              source={require('../../images/privacy.png')}
            />
            <Text style={[styles.userNumberTextOffCss, {marginLeft: 15}]}>
              Privacy Policy
            </Text>
          </View>
          <Image
            style={{width: 10, height: 15, resizeMode: 'contain'}}
            source={require('../../images/arrow.png')}
          />
        </TouchableOpacity>
        <TouchableOpacity
          activeOpacity={0.7}
          style={styles.rowViewOffCss}
          onPress={() => {
            navigation.navigate('WorkerTerm');
          }}>
          <View style={{flexDirection: 'row'}}>
            <Image
              style={{width: 20, height: 20, resizeMode: 'contain'}}
              source={require('../../images/terms.png')}
            />
            <Text style={[styles.userNumberTextOffCss, {marginLeft: 15}]}>
              Terms & Conditions
            </Text>
          </View>
          <Image
            style={{width: 10, height: 15, resizeMode: 'contain'}}
            source={require('../../images/arrow.png')}
          />
        </TouchableOpacity>
      </View>
      <View style={{marginVertical: 25}}>
        <ButtonStyle
          txtcolor={'#2A3B56'}
          bgColor={'#F6F4F4'}
          title={'Log Out'}
          onPress={() => {
            onLogoutHandler();
          }}
          //  onPress={() => { navigation.navigate('Login') }}
        />
      </View>
    </View>
  );
};

export default WorkerMyAccount;

const styles = StyleSheet.create({
  container: {
    width: '100%',
    height: 203,
    backgroundColor: '#6CBDFF',
  },
  cardViewOffCss: {
    height: 120,
    backgroundColor: '#fff',
    elevation: 5,
    marginTop: -hp(9),
    marginBottom: 20,
    borderRadius: 10,
    marginHorizontal: 20,
    justifyContent: 'center',
    alignItems: 'center',
  },
  userImgViewOffCss: {
    height: hp(11),
    width: wp(23),
    borderRadius: wp(21) / 2,
    marginTop: -hp(9),
    justifyContent: 'center',
    alignItems: 'center',
  },
  userImgOffCss: {
    height: '100%',
    width: '100%',
    resizeMode: 'contain',
  },
  userNameTextOffCss: {
    fontFamily: 'Avenir-Heavy',
    fontSize: hp(2.5),
    fontWeight: '900',
    color: '#000',
    paddingVertical: 5,
    letterSpacing: 0.7,
    paddingTop: 10,
  },
  userNumberTextOffCss: {
    fontFamily: 'Avenir-Medium',
    fontSize: hp(1.9),
    fontWeight: '500',
    color: '#454545',
  },
  rowViewOffCss: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingVertical: 15,
  },
});
