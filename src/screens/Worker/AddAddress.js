import React, {useState, useRef, useEffect, createRef} from 'react';
import {
  StyleSheet,
  View,
  Text,
  SafeAreaView,
  ScrollView,
  Image,
  FlatList,
  Picker,
  TouchableOpacity,
  Keyboard,
  TextInput,
  Alert,
  ActivityIndicator,
  ImageBackground,
  Platform,
  StatusBar,
} from 'react-native';
//import Geolocation from '@react-native-community/geolocation';
import Geolocation from 'react-native-geolocation-service';
import MapView from 'react-native-maps';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {ButtonStyle} from '../../Custom/CustomView';
import {Dropdown} from 'react-native-material-dropdown-v2-fixed';
import AppHeader from '../../Custom/CustomAppHeader';
import {StatusBarDark} from '../../Custom/CustomStatusBar';
import {Api} from '../../services/Api';
import {useDispatch, useSelector} from 'react-redux';
import {GooglePlacesAutocomplete} from 'react-native-google-places-autocomplete';
import {GOOGLE_MAPS_APIKEY} from '../../services/Config';
import * as actions from '../../redux/actions';

const ASPECT = hp(100) / wp(100);
const LATITUDE_DELTA = 0.9;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT;

const AddAddress = ({navigation, route}) => {
  const dispatch = useDispatch();
  const {
    user_id,
    phone,
    coords,
    addressLocation,
    latitudeLocation,
    longitudeLocation,
  } = useSelector(store => store);
  const [tcat, settcat] = useState('');
  const [state, setState] = useState({
    user_id: user_id,
    landmark: addressLocation || '',
    address_type: '',
    flat: '',
    area: '',
    lat: coords.latitude,
    lng: coords.longitude,
  });
  const [name, setName] = useState('');

  const [mobile, setMobile] = useState('');
  const [location, setLocation] = useState('');
  const [selectFlatANDBuil, setSelectFlatANDBuil] = useState('');
  const [LATITUDE, setLATITUDE] = useState(); // 30.7333148
  const [LONGITUDE, setLONGITUDE] = useState(); //76.7794179
  const [maxZoomLevel, setMaxZoomLevel] = useState(15);
  const [userAreaId, setUserAreaId] = useState('');
  const [userLocationId, setUserLocationId] = useState('');
  const [getLocationUser, setGetLocationUser] = useState('');
  const [selectArray, setSelectArray] = useState([
    {label: '', value: 0, lat: 26.9511369, lon: 75.7382615},
    {
      label: '41, Pocket 13, Sec 8, Rohini',
      value: 1,
      lat: 30.7333148,
      lon: 76.77941795,
    },
    {
      label: '42, Pocket 89, Sec 8, Rohini',
      value: 2,
      lat: 26.9511369,
      lon: 75.7382615,
    },
    {
      label: '41, Pocket 13, Sec 8, Rohini',
      value: 3,
      lat: 30.7333148,
      lon: 76.77941795,
    },
  ]);
  const [selectTypeIndex, setSelectTypeIndex] = useState(null);

  let mapRef = createRef();
  const input_flat = useRef(null);
  const input_name = useRef(null);
  const input_mobile = useRef(null);
  const pickerRef = useRef();

  const addNewWorkerHandler = async () => {
    const {user_type, mobile, name} = state;
    const {address_type, flat, area, landmark, lat, lng} = state;
    if (flat == '' && area == '') {
      alert('All Field are Requireds');
      return;
    }
    if (flat == '') {
      alert('Please ! Enter House/Flat/Block no');
      return;
    }
    if (area == '') {
      alert('Please ! Enter Area');
      return;
    }
    if (tcat == '') {
      alert('Please ! Select Any One Address');
      return;
    }

    const body = {
      user_id: user_id,
      address_type: tcat || '',
      flat,
      area,
      landmark: addressLocation,
      lat: lat || latitudeLocation,
      lng: lng || longitudeLocation,
    };
    // alert(JSON.stringify(body, null, 2));
    // return;
    const response = await Api.AddAddressApi(body);
    //  alert(JSON.stringify(response, null, 2));
    console.log(JSON.stringify(response, null, 2));
    const {status = false} = response;
    if (status) {
      alert('Successfully Data Insert');
    } else {
      alert('Something went wrong');
    }
  };

  useEffect(() => {
    getCurrentPosition();
  }, [navigation]);

  const getCurrentPosition = () => {
    Geolocation.getCurrentPosition(
      position => {
        // var lat = parseFloat(position.coords.latitude);
        // var long = parseFloat(position.coords.longitude);
        setLATITUDE(coords.latitude || latitudeLocation);
        setLONGITUDE(coords.longitude || longitudeLocation);
        // alert(JSON.stringify(position.coords, null, 2));
        console.log('-------getCurrentPosition :' + JSON.stringify(position));
      },
      error => {
        console.log('-------error :' + JSON.stringify(error));
      },
      {enableHighAccuracy: true, timeout: 20000, maximumAge: 3600000},
    );
  };

  const onLayout = () => {
    if (mapRef != null) {
      mapRef.fitToCoordinates(getLatitude(), {
        edgePadding: {top: 0, right: 0, bottom: 0, left: 0},
        animated: true,
      });
    }
  };

  const getLatitude = () => {
    let arr = [];
    // this.state.Markers.map((item) => {
    LONGITUDE_DELTA;
    let templatitude = LATITUDE;
    let templongitude = LONGITUDE;
    arr.push({latitude: templatitude, longitude: templongitude});
    // })
    return arr;
  };

  const onChangeDropdownValue = (value, index, data) => {
    setLocation(data[index].label);
    setUserAreaId(data[index].value);
    setLATITUDE(data[index].lat);
    setLONGITUDE(data[index].lon);
    setMaxZoomLevel(11);
    setTimeout(() => {
      onLayout();
    }, 700);
  };

  const onChangDropdownFlatBuil = (value, index, data) => {
    setSelectFlatANDBuil(data[index].label);
  };

  const selectAddType = (value, text) => {
    if (selectTypeIndex === value) {
      setSelectTypeIndex(null);
      setSelectFlatANDBuil('');
    } else {
      setSelectTypeIndex(value);
      setSelectFlatANDBuil(text);
      settcat(text);
      // alert(text);
    }
  };

  useEffect(() => {
    if (coords) {
      const {location = '', lat, lng} = coords;
      setDescription({...description, location, lat, lng});
    }
  }, [coords]);

  const [description, setDescription] = useState({
    location: '',
    lat: '',
    lng: '',
  });

  return (
    <SafeAreaView style={styles.container}>
      <StatusBarDark bg="#FFF" />
      <AppHeader
        backOnClick={() => {
          navigation.goBack();
        }}
        backIcon={require('../../images/e-remove.png')}
        title={'Add Address'}
        // searchOnClick={() => { }}
        // search={require('../../images/search.png')}
      />
      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={{width: wp(100), height: hp(50)}}>
          {LATITUDE && LONGITUDE ? (
            <MapView
              style={styles.mapstyle}
              // region={{
              // latitude: Number(LATITUDE),
              // longitude: Number(LONGITUDE),
              // }}
              maxZoomLevel={maxZoomLevel}
              minZoomLevel={1}
              showsUserLocation={false}
              showsMyLocationButton={true}
              onMapReady={() => onLayout()}
              ref={ref => {
                mapRef = ref;
              }}>
              <MapView.Marker
                coordinate={{
                  latitude: Number(LATITUDE),
                  longitude: Number(LONGITUDE),
                }}
              />
            </MapView>
          ) : null}
        </View>

        <View style={{paddingHorizontal: 15, paddingVertical: 20}}>
          <TouchableOpacity
            onPress={() => navigation.navigate('AutoCompleteGoogle')}>
            <Text style={styles.inputTextOffCss}>Your Location</Text>

            {addressLocation ? (
              <Text
                style={{
                  fontSize: 18,
                  marginBottom: 10,
                  color: '#00000060',
                  marginTop: 15,
                  fontWeight: '600',
                  fontFamily: 'Avenir-Medium',
                }}>
                {addressLocation}
              </Text>
            ) : (
              <Text
                style={{
                  marginBottom: 10,
                  fontSize: 18,
                  color: '#00000060',
                  marginTop: 15,
                  fontWeight: '600',
                  fontFamily: 'Avenir-Medium',
                }}>
                Search
              </Text>
            )}
          </TouchableOpacity>

          <Text style={styles.inputTextOffCss}>House/Flat/Block no.</Text>
          <View style={styles.inputViewOffCss}>
            <TextInput
              style={styles.inputStylesOffCss}
              keyboardType={'default'}
              returnKeyType={'next'}
              onChangeText={flat => setState({...state, flat})}
              value={state.flat}
            />
          </View>
          <Text style={styles.inputTextOffCss}>Road/Area</Text>
          <View style={styles.inputViewOffCss}>
            <TextInput
              style={styles.inputStylesOffCss}
              keyboardType={'default'}
              returnKeyType={'done'}
              onChangeText={area => setState({...state, area})}
              value={state.area}
            />
          </View>

          <Text style={[styles.inputTextOffCss, {color: '#353535'}]}>
            Save this address as{' '}
          </Text>
          <View
            style={{
              paddingVertical: 10,
              flexDirection: 'row',
              marginBottom: 5,
            }}>
            <TouchableOpacity
              onPress={() => {
                selectAddType(0, 'Home');
              }}
              style={[
                styles.homeViewOffCss,
                {
                  backgroundColor: selectTypeIndex === 0 ? '#6CBDFF' : '#FFF',
                  borderColor: selectTypeIndex === 0 ? '#6CBDFF' : '#8A94A3',
                },
              ]}>
              <Text
                style={[
                  styles.homeTextOffCss,
                  {color: selectTypeIndex === 0 ? '#FFF' : '#8A94A3'},
                ]}>
                Home
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => {
                selectAddType(1, 'Work');
              }}
              style={[
                styles.homeViewOffCss,
                {
                  backgroundColor: selectTypeIndex === 1 ? '#6CBDFF' : '#FFF',
                  borderColor: selectTypeIndex === 1 ? '#6CBDFF' : '#8A94A3',
                },
              ]}>
              <Text
                style={[
                  styles.homeTextOffCss,
                  {color: selectTypeIndex === 1 ? '#FFF' : '#8A94A3'},
                ]}>
                Work
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => {
                selectAddType(2, 'Other');
              }}
              style={[
                styles.homeViewOffCss,
                {
                  backgroundColor: selectTypeIndex === 2 ? '#6CBDFF' : '#FFF',
                  borderColor: selectTypeIndex === 2 ? '#6CBDFF' : '#8A94A3',
                },
              ]}>
              <Text
                style={[
                  styles.homeTextOffCss,
                  {color: selectTypeIndex === 2 ? '#FFF' : '#8A94A3'},
                ]}>
                Other
              </Text>
            </TouchableOpacity>
          </View>
          <View style={{marginVertical: 30, marginBottom: 0}}>
            <ButtonStyle
              title={'SAVE'}
              marginHorizontal={1}
              onPress={() => addNewWorkerHandler()}
              // onClick={() => {}}
            />
          </View>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFFFFF',
  },
  mapstyle: {
    flex: 1,
    height: hp(100),
    width: wp(100),
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  locationTextOffCss: {
    marginBottom: 10,
    paddingVertical: 5,
  },
  inputViewOffCss: {
    borderBottomWidth: 0.3,
    borderBottomColor: '#8A94A3',
    marginBottom: 20,
  },
  inputTextOffCss: {
    color: '#8A94A3',
    fontSize: hp(2.05),
    fontWeight: '400',
    fontFamily: 'Avenir-Normal',
  },
  inputStylesOffCss: {
    width: wp(100),
    height: 42,
    fontSize: hp(2),
    color: 'black',
  },
  homeViewOffCss: {
    flexDirection: 'row',
    borderRadius: 30,
    borderWidth: 0.6,
    marginRight: 10,
    height: 35,
    width: 90,
    paddingHorizontal: 7,
    backgroundColor: 'white',
    justifyContent: 'space-around',
    alignItems: 'center',
  },
  locationLogoOffCss: {
    width: 14,
    height: 18,
    resizeMode: 'contain',
  },
  homeTextOffCss: {
    fontSize: hp(1.8),
    fontFamily: 'Avenir-Medium',
  },
  drops: {
    height: hp(6.1), // 60,
    backgroundColor: '#FFF',
    borderBottomWidth: 0.3,
    borderBottomColor: '#8A94A3',
    paddingHorizontal: 20,
  },
});

export default AddAddress;
