import React, {useEffect, useState} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  Modal,
  Linking,
  View,
  Image,
  Dimensions,
  TouchableOpacity,
  useWindowDimensions,
  FlatList,
} from 'react-native';
import moment from 'moment';
import {StatusBarDark} from '../../Custom/CustomStatusBar';
import {TabView, SceneMap, TabBar} from 'react-native-tab-view';
import Dash from 'react-native-dash';
import {useNavigation} from '@react-navigation/native';
import AppHeader from '../../Custom/CustomAppHeader';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {ButtonStyle} from '../../Custom/CustomView';
import OTPInputView from '@twotalltotems/react-native-otp-input';
import MaterialTabs from 'react-native-material-tabs';
import stringsoflanguages from '../../language';
import {useDispatch, useSelector} from 'react-redux';
import {Api} from '../../services/Api';

const WorkerMyBooking = () => {
  const [modalVisible, setModalVisible] = useState(false);
  const [otpCompare, setOtpCompare] = useState();
  const [idItem, setIdItem] = useState();
  const {_myBooking} = stringsoflanguages;
  const navigation = useNavigation();
  const {user_id} = useSelector(store => store);
  const layout = useWindowDimensions();
  const [selectedTab, setSelectedTab] = useState(0);
  const [jobsData, setJobsData] = useState([]);
  const [completedList, setCompletedList] = useState([]);

  const [state, setState] = useState({
    isLoading: true,
    init: true,
    code: '',
  });

  useEffect(() => {
    // alert(JSON.stringify(otpCompare));
    GetJobsHandler();
  }, []);

  const GetJobsHandler = async () => {
    const body = {
      worker_id: user_id,
    };
    const response = await Api.OngoingJobsWorkerApi(body);
    // alert(JSON.stringify(response, null, 2));
    const {status = false, data = []} = response;
    if (status == true) {
      setJobsData(data);
    } else {
      alert('Something went wrong');
    }
  };

  useEffect(() => {
    GetCompleteHandler();
  }, []);

  const GetCompleteHandler = async () => {
    const body = {
      worker_id: user_id,
    };
    const response = await Api.CompletedJobsWorkerApi(body);
    // alert(JSON.stringify(response.data, null, 2));
    // console.log(JSON.stringify(response, null, 2));
    const {status = false, data = []} = response;
    if (status == true) {
      setCompletedList(data);
    } else {
      alert('Something went wrong');
    }
  };
  const otpHandler = (item, index) => {
    verifyOtpHandler(item, index);
  };
  const verifyOtpHandler = async (item, index) => {
    setOtpCompare(item.otp);
    if (state.code == '') {
      setModalVisible(true);
      alert('Please! Enter Otp');
    } else if (state.code !== otpCompare) {
      setModalVisible(true);
      alert('Please! Enter Valid Otp');
    } else if (state.code == otpCompare) {
      // startJobRequestHandler
      // GetJobsHandler();
      setModalVisible(false);
    }
  };

  const startJobRequestHandler = async (item, index) => {
    const body = {
      id: item.id,
      status: 2,
    };

    const response = await Api.AcceptButtonWorkerApi(body);
    // alert(JSON.stringify(response, null, 2));
    const {status = false} = response;
    if (status == true) {
      GetJobsHandler();
      setIdItem(item.id);
      setOtpCompare(item.otp);
    } else {
      alert('Something went wrong');
    }
  };

  const endJobRequestHandler = async (item, index) => {
    const body = {
      id: item.id,
      status: 3,
    };
    // alert(JSON.stringify(body));
    // return;

    const response = await Api.AcceptButtonWorkerApi(body);
    // alert(JSON.stringify(response));
    const {status = false} = response;
    if (status == true) {
      setModalVisible(false);
      GetJobsHandler();
      // navigation.goBack();
    } else {
      alert('Something went wrong');
    }
  };

  const callRecordingHandler = async (item, index) => {
    const body = {
      worker_id: item.worker_id,
      user_id: item.user_id,
      booking_id: item.id,
    };
    const response = await Api.CallBookingRecordingWorkerApi(body);
    // alert(JSON.stringify(response.data, null, 2));
    // console.log(JSON.stringify(response, null, 2));
    const {status = false} = response;
    if (status == true) {
      let phoneNumber = item.user_mobile;
      if (Platform.OS === 'android') {
        phoneNumber = `tel:${phoneNumber}`;
      } else {
        phoneNumber = `telprompt:${phoneNumber}`;
      }
      Linking.openURL(phoneNumber);
      // setCompletedList(data);
    } else {
      alert('Something went wrong');
    }
  };

  const renderItemCompleted = ({item, index}) => {
    const {_myBooking} = stringsoflanguages;
    return (
      <View style={styles.jobRequestsViewCardOffCss}>
        <View
          style={{
            paddingVertical: 10,
            borderBottomColor: '#C8C8D3',
            borderBottomWidth: 0.3,
          }}>
          <Text style={styles.bookingIDTextOffCss}>Booking #{item.id}</Text>
          <Text style={styles.dateTimeTextOffCss}>
            {item.booking_date_time}
          </Text>
          {/* {moment(dateTime).format("MMMM Do YYYY, h:mm:ss a")} */}
        </View>
        <View
          style={{
            paddingVertical: 10,
            flexDirection: 'row',
            alignItems: 'center',
            borderBottomColor: '#C8C8D3',
            borderBottomWidth: 0.4,
          }}>
          <Image
            style={styles.jobUserImgOffCss}
            source={require('../../images/pic.png')}
          />
          <Text style={styles.jobUserNameTextOffCss}>{item.user_name}</Text>
          <View style={{flex: 1, alignItems: 'flex-end'}}>
            <TouchableOpacity
              activeOpacity={0.7}
              onPress={() => {
                callRecordingHandler(item, index);
              }}>
              <Image
                style={{width: 30, height: 30, resizeMode: 'contain'}}
                source={require('../../images/phone.png')}
              />
            </TouchableOpacity>
          </View>
        </View>
        <View
          style={{
            paddingVertical: 10,
            borderBottomColor: '#C8C8D3',
            borderBottomWidth: 0.4,
          }}>
          <Text
            style={[
              styles.jobUserNameTextOffCss,
              {fontSize: hp(1.65), marginLeft: 0, marginBottom: 5},
            ]}>
            Site Address
          </Text>
          <Text style={styles.siteAddreTextOffCss}>
            {' '}
            '1007, 10th Floor, Tower-1, Pearls Omaxe Building, Netaji Subhash
            Place, Pitam Pura, New Delhi 110034 (IN)',
          </Text>
        </View>
        <View style={{paddingVertical: 10}}>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              paddingVertical: 5,
            }}>
            <Text
              style={[
                styles.jobUserNameTextOffCss,
                {fontSize: hp(1.9), marginLeft: 0, marginBottom: 5},
              ]}>
              Customer Rating
            </Text>
            <View style={{flexDirection: 'row'}}>
              <Text style={[styles.bookingIDTextOffCss, {marginRight: 5}]}>
                4.0
              </Text>
              <FlatList
                horizontal
                data={['', '', '', '', '']}
                renderItem={(item, index) => {
                  return (
                    <Image
                      style={{
                        width: 16.54,
                        height: 16.54,
                        resizeMode: 'contain',
                      }}
                      source={require('../../images/star.png')}
                    />
                  );
                }}
              />
            </View>
          </View>
          <Text style={styles.siteAddreTextOffCss}>{item.customerMess}</Text>
        </View>
      </View>
    );
  };

  const callRecordingOngoingHandler = async (item, index) => {
    const body = {
      worker_id: item.worker_id,
      user_id: item.user_id,
      booking_id: item.id,
    };
    const response = await Api.CallBookingRecordingWorkerApi(body);
    // alert(JSON.stringify(response.data, null, 2));
    // console.log(JSON.stringify(response, null, 2));
    const {status = false} = response;
    if (status == true) {
      let phoneNumber = item.user_mobile;
      if (Platform.OS === 'android') {
        phoneNumber = `tel:${phoneNumber}`;
      } else {
        phoneNumber = `telprompt:${phoneNumber}`;
      }
      Linking.openURL(phoneNumber);
      // setCompletedList(data);
    } else {
      alert('Something went wrong');
    }
  };

  const renderItemOngoing = ({item, index}) => {
    return (
      <View style={styles.jobRequestsViewCardOffCss}>
        <View
          style={{
            paddingVertical: 10,
            flexDirection: 'row',
            borderBottomColor: '#C8C8D3',
            borderBottomWidth: 0.3,
          }}>
          <View style={{flex: 0.8}}>
            <Text style={styles.bookingIDTextOffCss}>Booking #{item.id}</Text>
            <Text style={styles.dateTimeTextOffCss}>
              {item.booking_date_time}
            </Text>
          </View>
          {item.status === 'ongoing' && (
            <View style={{flex: 0.3}}>
              <View style={styles.onGoingViewOffCss}>
                <Text style={styles.onGoingTextOffCss}>Ongoing</Text>
              </View>
            </View>
          )}
          {/* {moment(dateTime).format("MMMM Do YYYY, h:mm:ss a")} */}
        </View>
        <View
          style={{
            paddingVertical: 10,
            flexDirection: 'row',
            alignItems: 'center',
            borderBottomColor: '#C8C8D3',
            borderBottomWidth: 0.4,
          }}>
          <Image
            style={styles.jobUserImgOffCss}
            source={require('../../images/pic.png')}
          />
          <Text style={styles.jobUserNameTextOffCss}>{item.user_name}</Text>
          <View style={{flex: 1, alignItems: 'flex-end'}}>
            <TouchableOpacity
              activeOpacity={0.7}
              onPress={() => {
                callRecordingOngoingHandler(item, index);
              }}>
              <Image
                style={{width: 30, height: 30, resizeMode: 'contain'}}
                source={require('../../images/phone.png')}
              />
            </TouchableOpacity>
          </View>
        </View>
        <View style={{paddingVertical: 10}}>
          <Text
            style={[
              styles.jobUserNameTextOffCss,
              {fontSize: hp(1.65), marginLeft: 0, marginBottom: 5},
            ]}>
            Site Address
          </Text>
          <Text style={styles.siteAddreTextOffCss}>
            '1007, 10th Floor, Tower-1, Pearls Omaxe Building, Netaji Subhash
            Place, Pitam Pura, New Delhi 110034 (IN)',
          </Text>
        </View>
        <View style={{paddingVertical: 10, marginBottom: 5}}>
          {item.status === 'new' && (
            <ButtonStyle
              height={hp(4.8)}
              fontSize={hp(1.9)}
              title={'START JOB'}
              bgColor={'#6DD400'}
              marginHorizontal={wp(22)}
              onPress={() => verifyOtpHandler(item, index)}
            />
          )}
          {item.status === 'ongoing' && (
            <ButtonStyle
              height={hp(4.8)}
              fontSize={hp(1.9)}
              title={'END JOB'}
              bgColor={'#E02020'}
              marginHorizontal={wp(22)}
              onPress={() => endJobRequestHandler(item, index)}
            />
          )}
        </View>
      </View>
    );
  };

  const categorySelect = index => {
    setSelectedTab(index);
    if (index == 0) {
      // setOngoingList();
    } else if (index == 1) {
      //  setShortlist('shortlist');
    }
  };

  return (
    <View style={styles.container}>
      <StatusBarDark />
      <AppHeader
        elevation={0.1}
        backOnClick={() => {
          navigation.goBack();
        }}
        backIcon={require('../../images/e-remove.png')}
        title={'My Bookings'}
      />

      <MaterialTabs
        items={[_myBooking.ongoing, _myBooking.completed]}
        selectedIndex={selectedTab}
        onChange={index => categorySelect(index)}
        barColor="#F2AD4B"
        indicatorColor="#FFF"
        activeTextColor="#FFF"
        inactiveTextColor="#FFF"
        indicatorHeight={4}
      />

      <ScrollView showsVerticalScrollIndicator={false}>
        {selectedTab == 1 && (
          <FlatList
            style={{width: '100%', marginTop: 3}}
            data={completedList}
            showsVerticalScrollIndicator={false}
            renderItem={renderItemCompleted}
          />
        )}
        {selectedTab == 0 && (
          <FlatList
            style={{width: '100%', marginTop: 3}}
            data={jobsData}
            showsVerticalScrollIndicator={false}
            renderItem={renderItemOngoing}
          />
        )}

        <Modal
          visible={modalVisible}
          transparent={true}
          onRequestClose={() => {
            // Alert.alert("Modal has been closed.");
            // openOtpJobModal(false);
          }}>
          <View style={styles.modal_View}>
            <View style={styles.mdtop}>
              <Text style={styles.modalTitleText}>Enter OTP to end job</Text>
              <OTPInputView
                style={styles.otpInput}
                pinCount={4}
                // onCodeChanged={text => {
                //   setOtp(text.replace(/[^0-9]/g, ''));
                // }}
                code={state.code}
                // onCodeChanged={code => setState(replace(/[^0-9]/g, '')({...state, code}))}
                onCodeChanged={code => setState({...state, code})}
                autoFocusOnLoad
                codeInputFieldStyle={styles.underlineStyleBase}
                codeInputHighlightStyle={styles.underlineStyleHighLighted}
                // onCodeFilled={completeJobRequestHandler}
              />
              <View style={{marginVertical: 10}}>
                <ButtonStyle
                  height={hp(5)}
                  fontSize={hp(1.9)}
                  title={'OK'}
                  bgColor={'#6CBDFF'}
                  marginHorizontal={wp(29)}
                  onPress={() => otpHandler()}
                  // onPress={() => {
                  //   jobverfyfunction(selctModalJob);
                  // }}
                />
              </View>
            </View>
          </View>
        </Modal>
      </ScrollView>

      {/* 
      <TabView
        navigationState={{index, routes}}
        renderScene={renderScene}
        onIndexChange={setIndex}
        initialLayout={{width: layout.width}}
        renderTabBar={props => (
          <TabBar
            style={styles.tabstyle}
            labelStyle={styles.labelStyle}
            scrollEnabled={false}
            activeColor={'#fff'}
            inactiveColor={'#fff'}
            inactiveOpacity={0.5}
            {...props}
            indicatorStyle={styles.indicatorStyle}
          />
        )}
      /> */}
    </View>
  );
};

export default WorkerMyBooking;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F4F4F4',
  },
  tabstyle: {backgroundColor: '#F2AD4B'},
  labelStyle: {
    fontSize: hp(1.9),
    fontFamily: 'Avenir-Heavy',
    fontWeight: '900',
  },
  labelStyle2: {
    fontSize: hp(1.9),
    fontFamily: 'Avenir-Meduim',
    fontWeight: '500',
  },
  indicatorStyle: {backgroundColor: '#fff', height: 3},
  jobRequestsViewCardOffCss: {
    backgroundColor: '#FFF',
    elevation: 1.5,
    borderRadius: 8,
    paddingHorizontal: 15,
    marginVertical: 5,
    marginHorizontal: 15,
    shadowOffset: {
      width: 0,
      height: 10,
    },
    shadowOpacity: 0.7,
    //   shadowRadius: 60,
    shadowColor: Platform.OS == 'ios' ? '#00000060' : '#000000',
  },
  bookingIDTextOffCss: {
    color: '#454545',
    fontSize: hp(1.9),
    fontFamily: 'Avenir-Heavy',
    fontWeight: '900',
    marginBottom: 2,
  },
  dateTimeTextOffCss: {
    color: '#A5A5A5',
    fontSize: hp(1.4),
    fontFamily: 'Avenir-Meduim',
    fontWeight: '500',
  },
  jobUserImgOffCss: {
    width: wp(13.9),
    height: hp(6.9),
    borderRadius: 50 / 2,
    resizeMode: 'contain',
  },
  jobUserNameTextOffCss: {
    color: '#454545',
    fontSize: hp(1.9),
    fontFamily: 'Avenir-Heavy',
    fontWeight: '900',
    marginLeft: 20,
  },
  siteAddreTextOffCss: {
    color: '#333333',
    fontSize: hp(1.65),
    fontFamily: 'Avenir-Meduim',
    fontWeight: '500',
    opacity: 0.6,
  },
  onGoingViewOffCss: {
    padding: 4,
    backgroundColor: '#FA6400',
    borderRadius: 12.5,
    justifyContent: 'center',
    alignItems: 'center',
  },
  onGoingTextOffCss: {
    color: '#FFFFFF',
    fontSize: hp(1.65),
    fontFamily: 'Avenir-Meduim',
    fontWeight: '500',
  },
  modal_View: {
    backgroundColor: '#000000bb',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  mdtop: {
    width: 330,
    backgroundColor: '#FFFFFF',
    marginHorizontal: 20,
    borderRadius: 20,
    paddingVertical: 10,
  },
  modalTitleText: {
    fontSize: hp(2.5),
    fontFamily: 'Avenir-Heavy',
    fontWeight: '900',
    color: '#333333',
    textAlign: 'center',
    paddingVertical: 10,
  },
  otpInput: {
    color: '#000000',
    height: 60,
    marginVertical: 20,
    marginHorizontal: 30,
  },
  underlineStyleBase: {
    width: 60,
    height: 60,
    borderWidth: 1,
    borderColor: '#000',
    fontWeight: 'bold',
    color: '#100C08',
    backgroundColor: '#00000019',
    borderRadius: 20,
    fontFamily: 'Avenir-Heavy',
    fontSize: 24,
  },
  underlineStyleHighLighted: {
    width: 60,
    height: 60,
    borderRadius: 20,
    fontFamily: 'Avenir-Heavy',
    fontSize: 24,
    fontWeight: 'bold',
    color: 'white',
    backgroundColor: '#00000019',
  },
});

const DashLine = props => (
  <View style={{margin: 10}}>
    <Dash dashColor={'#C8C8D3'} dashLength={5} dashThickness={1} {...props} />
  </View>
);
