import React, {useEffect, useState} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  Image,
  Dimensions,
  TouchableOpacity,
  FlatList,
} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {StatusBarDark} from '../../Custom/CustomStatusBar';
import {Header} from '../../Custom/CustomView';
import stringsoflanguages from '../../language';
const {height} = Dimensions.get('window');
import {Api} from '../../services/Api';
import HTML from 'react-native-render-html';

const WorkerTerm = ({navigation}) => {
  const {_SubscriptionPlanContractor, _privacyPolicy} = stringsoflanguages;
  const [state, setState] = useState({
    terms_conditions: '',
    isLoading: false,
  });

  useEffect(() => {
    GetJobsHandler();
  }, []);

  const GetJobsHandler = async () => {
    const response = await Api.SettingsApi();
    // alert(JSON.stringify(response, null, 2));
    //  console.log(JSON.stringify(response, null, 2));
    const {status = false, terms_conditions} = response;
    setState({...state, terms_conditions});
  };
  return (
    <View style={{backgroundColor: '#F4F4F4', flex: 1}}>
      <StatusBarDark />
      <Header
        onPress={() => navigation.goBack()}
        title={_privacyPolicy._privacyPolicy}
      />
      <ScrollView>
        <SafeAreaView style={[styles.plan]}>
          <View
            style={{
              marginTop: 10,
              alignSelf: 'center',
              width: '85%',
              marginBottom: 20,
            }}>
            <HTML source={{html: state.terms_conditions}} />
          </View>
        </SafeAreaView>
      </ScrollView>
    </View>
  );
};

export default WorkerTerm;

const styles = StyleSheet.create({
  plan: {
    height: '100%',
    width: '100%',
    backgroundColor: '#fff',
    elevation: 5,
    marginTop: 10,
    marginBottom: 5,
  },
  subplan: {
    backgroundColor: '#ECEFF1',
    borderTopEndRadius: 10,
    borderTopLeftRadius: 10,
  },
  plantext: {
    fontFamily: 'Avenir-Heavy',
    fontWeight: 'bold',
    fontSize: 20,
    color: '#333333',
    textAlign: 'center',
    marginTop: 10,
  },
  subplantext: {
    fontFamily: 'Avenir-Heavy',
    fontWeight: 'bold',
    fontSize: 30,
    color: '#333333',
    textAlign: 'center',
    marginTop: 10,
  },
  tickimg: {
    alignSelf: 'center',
    borderRadius: 35,
    resizeMode: 'contain',
    width: 70,
    height: 70,
    marginTop: 10,
  },
  ticktext: {
    fontFamily: 'Avenir-Medium',
    fontWeight: '500',
    fontSize: 13,
    color: '#8A8A8A',
    marginTop: 8,
  },
  touch: {
    width: 141,
    height: 35,
    borderRadius: 25,
    backgroundColor: '#6CBDFF',
    marginTop: 30,
    marginBottom: 10,
    alignSelf: 'center',
  },
  touchtext: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 16,
    fontWeight: 'bold',
    alignSelf: 'center',
    color: '#ffffff',
    marginTop: 5,
  },
});
