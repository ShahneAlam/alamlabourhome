import React, {useState, useEffect} from 'react';
import {Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {GooglePlacesAutocomplete} from 'react-native-google-places-autocomplete';
import {useDispatch, useSelector} from 'react-redux';
import {GOOGLE_MAPS_APIKEY} from '../../services/Config';
import * as actions from '../../redux/actions';

const AutoCompleteGoogle = ({navigation, route}) => {
  const {coords} = useSelector(state => state);
  const dispatch = useDispatch();
  const [state, setState] = useState({
    latitude: coords.latitude,
    longitude: coords.longitude,
  });

  useEffect(() => {
    if (coords) {
      const {location = '', lat, lng} = coords;
      setDescription({...description, location, lat, lng});
    }
  }, [coords]);

  const [description, setDescription] = useState({
    location: '',
    lat: '',
    lng: '',
  });

  const onPressHandler = (data, details) => {
    // alert(JSON.stringify(data));
  };
  useEffect(() => {
    // console.log('cos', coords);
    // console.log('location', data);
  }, []);

  return (
    <View style={styles.container}>
      <TouchableOpacity onPress={() => navigation.goBack()}>
        <Image
          source={require('../../images/e-remove.png')}
          style={styles.topimage}
        />
      </TouchableOpacity>
      <Text style={styles.title}>Search Location</Text>
      <GooglePlacesAutocomplete
        placeholder="Search"
        fetchDetails={true}
        onPress={(data, details = null) => {
          dispatch(actions.AddressLocation(data.description));
          dispatch(actions.LatitudeLocation(details.geometry.location.lat));
          dispatch(actions.LongitudeLocation(details.geometry.location.lng));

          // alert(JSON.stringify(data.description, null, 2));
          // 'details' is provided when fetchDetails = true
          // console.log(details, null, 2);
        }}
        query={{
          key: GOOGLE_MAPS_APIKEY,
          language: 'en',
          location: `${state.latitude},${state.longitude}`,
          radius: '150000',
        }}
        onFail={error => console.log(error)}
        styles={{
          container: {
            marginHorizontal: 20,
            marginTop: 50,
          },
          textInputContainer: {
            overflow: 'hidden',
            paddingHorizontal: 10,
            borderRadius: 15,
            borderColor: '#242E4280',
            borderWidth: 1,
          },
          textInput: {
            color: '#5d5d5d',
            fontSize: 16,
          },
          predefinedPlacesDescription: {
            color: '#1faadb',
          },
        }}
      />
    </View>
  );
};

export default AutoCompleteGoogle;

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#FFFFFF',
    flex: 1,
  },
  topimage: {
    width: 18,
    height: 16,
    marginTop: 50,
    marginHorizontal: 30,
    resizeMode: 'contain',
  },
  title: {
    marginTop: -25,
    fontFamily: 'Avenir-Heavy',
    fontSize: 24,
    fontWeight: 'bold',
    alignSelf: 'center',
  },
});
