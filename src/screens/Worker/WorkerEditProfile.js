
import React, { useState, useEffect, useRef } from 'react';
import {
    SafeAreaView,
    ScrollView,
    StatusBar,
    StyleSheet,
    Text,
    useColorScheme,
    View,
    Image,
    Dimensions,
    TouchableOpacity,
    TextInput,
    FlatList,
    ImageBackground
} from 'react-native';
import { StatusBarDark } from '../../Custom/CustomStatusBar';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import AppHeader from '../../Custom/CustomAppHeader';
import InputView from '../../Custom/CustomTextInput';
import { ButtonStyle } from '../../Custom/CustomView';
import CustomCropImagePicker from '../../Custom/CustomCropImagePicker';

const WorkerEditProfile = ({ navigation }) => {
    const [userName, setUserName] = useState('')
    const [userSkill, setUserSkill] = useState('')
    const [userDailyRate, setUserDailyRate] = useState('')
    const [userHourlyRate, setUserHourlyRate] = useState('')
    const [userExperi, setUserExperi] = useState('')
    const [userBio, setUserBio] = useState('')
    const [uploadPhotoVideo, setUploadPhotoVideo] = useState([]);
    const [refreshArray, setRefreshArray] = useState(false)

    const input_name = useRef(null)
    const input_dailyRate = useRef(null)
    const input_hourlyRate = useRef(null)
    const input_experience = useRef(null)
    const input_bio = useRef(null)

    useEffect(() => {
        const unsubscribe = navigation.addListener('focus', () => {
        });
        return unsubscribe;
    }, [navigation]);

    const launchPhotosVideos = (arry) => {
        for (let index = 0; index < arry.length; index++) {
            const element = arry[index];
            uploadPhotoVideo.push(element)
            setRefreshArray({ refreshArray: !refreshArray })
        }
    }

    const removePhotoVideoArray = (index) => {
        var array = [...uploadPhotoVideo];
        array.splice(index, 1);
        setUploadPhotoVideo(array)
    }

    const photosVideosFunction = ({ item, index }) => {
        return (
            <>
                {item.type === 'video/mp4' ?
                    <View style={styles.photoVideoViewOffCss}>
                        <ImageBackground
                            imageStyle={{ borderRadius: 5 }}
                            style={[styles.photoVideoImgOffCss, {}]}
                            source={{ uri: item.uri }}>
                            <TouchableOpacity
                                activeOpacity={0.7}
                                onPress={() => { removePhotoVideoArray(index) }}
                                style={{ paddingVertical: 5, left: wp(35) }}>
                                <Image
                                    style={{ width: 20, height: 20, marginLeft: 5, resizeMode: 'contain' }}
                                    source={require('../../images/multiply.png')} />
                            </TouchableOpacity>
                            <Image
                                style={{ width: 30, height: 40, marginTop: hp(2), alignSelf: 'center', resizeMode: 'contain' }}
                                source={require('../../images/play-button.png')}
                            />
                        </ImageBackground>
                    </View>
                    :
                    <View style={styles.photoVideoViewOffCss}>
                        <ImageBackground
                            imageStyle={{ borderRadius: 5 }}
                            style={[styles.photoVideoImgOffCss, {}]}
                            source={{ uri: item.uri }}>
                            <TouchableOpacity
                                activeOpacity={0.7}
                                onPress={() => { removePhotoVideoArray(index) }}
                                style={{ paddingVertical: 5, left: wp(35) }}>
                                <Image
                                    style={{ width: 20, height: 20, marginLeft: 5, resizeMode: 'contain' }}
                                    source={require('../../images/multiply.png')} />
                            </TouchableOpacity>
                        </ImageBackground>
                    </View>
                }
            </>
        )
    }

    return (
        <View style={styles.continer}>
            <StatusBarDark bg='#FFF' />
            <AppHeader
                backOnClick={() => { navigation.goBack() }}
                backIcon={require('../../images/e-remove.png')}
                title={'Edit Profile'}
            />
            <ScrollView contentContainerStyle={{ flexGrow: 1, paddingHorizontal: 20, paddingVertical: 10, }} showsVerticalScrollIndicator={false}>
                <View style={{ marginVertical: 5 }}>
                    <Text style={styles.inputfiledNameTextOffCss}>Name</Text>
                    <InputView
                        keyboardType='default'
                        placeholder="Name"
                        returnKeyType={'next'}
                        value={userName}
                        onChangeText={(text) => { setUserName(text) }}
                        getFocus={() => input_dailyRate.current.focus()}
                        setFocus={input_name}
                    />
                </View>
                <View style={{ marginVertical: 5 }}>
                    <Text style={styles.inputfiledNameTextOffCss}>Skill</Text>
                    <TouchableOpacity
                        activeOpacity={0.7}
                        onPress={() => { }} // navigation.navigate('SelectTrade')
                        style={[styles.drops, { flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', marginVertical: 10 }]}>
                        <Text style={styles.tradeTextOffCss}>Select Trade</Text>
                        <Image style={styles.dropIconOffCss} source={require('../../images/drop.png')} />
                    </TouchableOpacity>
                </View>
                <View style={{ marginVertical: 5 }}>
                    <Text style={styles.inputfiledNameTextOffCss}>Daily Rate (₹)</Text>
                    <InputView
                        keyboardType='numeric'
                        placeholder="Name"
                        returnKeyType={'next'}
                        value={userDailyRate}
                        onChangeText={(text) => { setUserDailyRate(text) }}
                        getFocus={() => input_hourlyRate.current.focus()}
                        setFocus={input_dailyRate}
                    />
                </View>
                <View style={{ marginVertical: 5 }}>
                    <Text style={styles.inputfiledNameTextOffCss}>Hourly Rate (₹)</Text>
                    <InputView
                        keyboardType='numeric'
                        placeholder="Name"
                        returnKeyType={'next'}
                        value={userHourlyRate}
                        onChangeText={(text) => { setUserHourlyRate(text) }}
                        getFocus={() => input_experience.current.focus()}
                        setFocus={input_hourlyRate}
                    />
                </View>
                <View style={{ marginVertical: 5 }}>
                    <Text style={styles.inputfiledNameTextOffCss}>Experience (yrs)</Text>
                    <InputView
                        keyboardType='default'
                        placeholder="Name"
                        returnKeyType={'next'}
                        value={userExperi}
                        onChangeText={(text) => { setUserExperi(text) }}
                        getFocus={() => input_bio.current.focus()}
                        setFocus={input_experience}
                    />
                </View>
                <View style={{ marginVertical: 5 }}>
                    <Text style={styles.inputfiledNameTextOffCss}>Bio</Text>
                    <View style={styles.boxes}>
                        <TextInput
                            placeholder=""
                            keyboardType='default'
                            returnKeyType={'done'}
                            multiline={true}
                            value={userBio}
                            ref={input_bio}
                            style={styles.textinput}
                            onChangeText={(text) => { setUserBio(text) }} />
                    </View>
                </View>
                <View style={{ marginVertical: 5 }}>
                    <Text style={styles.inputfiledNameTextOffCss}>Upload Images/Videos</Text>
                    {uploadPhotoVideo.length > 0 &&
                        <FlatList
                            numColumns={2}
                            data={uploadPhotoVideo}
                            renderItem={photosVideosFunction}
                            showsHorizontalScrollIndicator={false}
                        />
                    }
                    <CustomCropImagePicker
                        dataGet={(arry) => { launchPhotosVideos(arry) }}
                    />
                </View>
                <View style={{ marginVertical: 20 }}>
                    <ButtonStyle
                        title={'SAVE'}
                        marginHorizontal={1}
                        onPress={() => { navigation.goBack() }}
                    />
                </View>
            </ScrollView>
        </View>
    );
};

export default WorkerEditProfile;

const styles = StyleSheet.create({
    continer: {
        flex: 1,
        backgroundColor: '#F4F4F4',
    },
    inputfiledNameTextOffCss: {
        fontSize: hp(2.20), fontFamily: 'Avenir-Meduim', fontWeight: '500', color: '#454545'
    },
    drops: {
        height: hp(8.10), backgroundColor: '#fff', borderRadius: 15, borderTopLeftRadius: 20,
        borderTopRightRadius: 20, borderBottomWidth: 0, paddingHorizontal: 20 // elevation: 2,
    },
    tradeTextOffCss: {
        fontSize: hp(2.10),
        fontWeight: '500',
        color: '#7A7A7A',
        fontFamily: 'Avenir-Medium',
    },
    dropIconOffCss: {
        width: wp(7),
        height: hp(5),
        resizeMode: 'contain',
        transform: [{ rotate: '270deg' }]
    },
    boxes: {
        height: hp(16.30), borderRadius: 20, borderWidth: 1, borderColor: '#FFF',
        backgroundColor: '#fff', marginVertical: 10
    },
    textinput: {
        fontSize: hp(2), color: '#000', fontWeight: '500', fontFamily: 'Avenir-Medium', paddingHorizontal: 15,
    },
    fileImgOffCss: {
        width: 162, height: 115, resizeMode: 'contain', marginVertical: 10
    },
    photoVideoViewOffCss: {
        width: wp(43), height: hp(16), marginRight: 10, marginVertical: 10, borderRadius: 5,
        justifyContent: 'center', alignItems: 'center'
    },
    photoVideoImgOffCss: {
        width: wp(43), height: hp(16), resizeMode: 'stretch', borderRadius: 5
        // stretch
    }
});
