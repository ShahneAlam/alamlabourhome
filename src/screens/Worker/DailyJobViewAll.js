import React, {useEffect, useState} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  Image,
  Dimensions,
  TouchableOpacity,
  FlatList,
} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {StatusBarDark} from '../../Custom/CustomStatusBar';
import {Header} from '../../Custom/CustomView';
import stringsoflanguages from '../../language';
const {height} = Dimensions.get('window');
import {Api} from '../../services/Api';

const DailyJobViewAll = ({navigation}) => {
  const {_SubscriptionPlanContractor, _dailyJobsViewAll} = stringsoflanguages;
  const [data, setData] = useState([]);

  let colors = [
    '#EDE7F6',
    '#E1F5FE',
    '#F3E5F5',
    '#FFF8E1',
    '#FBE9E7',
    '#FCE4EC',
    '#E8EAF6',
  ];

  useEffect(() => {
    GetJobsHandler();
  }, []);

  const GetJobsHandler = async () => {
    const response = await Api.GetDailyJobViewAllApi();
    // alert(JSON.stringify(response, null, 2));
    // console.log(JSON.stringify(response, null, 2));
    const {status = false, job_request = []} = response;
    if (status == true) {
      setData(job_request);
    } else {
      alert('Something went wrong');
    }
  };
  return (
    <View style={{backgroundColor: '#F4F4F4', flex: 1}}>
      <StatusBarDark />
      <Header
        onPress={() => navigation.goBack()}
        title={_dailyJobsViewAll._categoriesList}
      />
      <ScrollView>
        <View>
          <FlatList
            style={{
              width: '95%',
              alignSelf: 'center',
            }}
            numColumns={3}
            keyExtractor={item => item.id}
            data={data}
            renderItem={({item, index}) => (
              <TouchableOpacity
                style={[
                  stylesDailyJobs.touch,
                  {backgroundColor: colors[index % colors.length]},
                ]}
                onPress={() =>
                  navigation.navigate('CallList', {
                    id: item.id,
                  })
                }>
                <Image
                  source={{uri: item.image_url}}
                  style={stylesDailyJobs.images}
                />
                <Text style={stylesDailyJobs.title}>{item.name}</Text>
              </TouchableOpacity>
            )}
          />
        </View>
      </ScrollView>
    </View>
  );
};

export default DailyJobViewAll;

const styles = StyleSheet.create({
  plan: {
    backgroundColor: '#fff',
    borderRadius: 10,
    elevation: 5,
    marginTop: 10,
    marginBottom: 5,
    margin: 20,
  },
  subplan: {
    backgroundColor: '#ECEFF1',
    borderTopEndRadius: 10,
    borderTopLeftRadius: 10,
  },
  plantext: {
    fontFamily: 'Avenir-Heavy',
    fontWeight: 'bold',
    fontSize: 20,
    color: '#333333',
    textAlign: 'center',
    marginTop: 10,
  },
  subplantext: {
    fontFamily: 'Avenir-Heavy',
    fontWeight: 'bold',
    fontSize: 30,
    color: '#333333',
    textAlign: 'center',
    marginTop: 10,
  },
  tickimg: {
    alignSelf: 'center',
    borderRadius: 35,
    resizeMode: 'contain',
    width: 70,
    height: 70,
    marginTop: 10,
  },
  ticktext: {
    fontFamily: 'Avenir-Medium',
    fontWeight: '500',
    fontSize: 13,
    color: '#8A8A8A',
    marginTop: 8,
  },
  touch: {
    width: 141,
    height: 35,
    borderRadius: 25,
    backgroundColor: '#6CBDFF',
    marginTop: 30,
    marginBottom: 10,
    alignSelf: 'center',
  },
  touchtext: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 16,
    fontWeight: 'bold',
    alignSelf: 'center',
    color: '#ffffff',
    marginTop: 5,
  },
});

const stylesDailyJobs = StyleSheet.create({
  container: {
    backgroundColor: '#F4F4F4',
    flex: 1,
  },
  sub2text: {
    fontFamily: 'Avenir',
    fontWeight: 'bold',
    fontSize: 18,
    color: '#454545',
    marginBottom: 20,
    marginHorizontal: 20,
    marginTop: 20,
  },
  sub3text: {
    fontFamily: 'Avenir-Medium',
    fontWeight: '500',
    fontSize: 14,
    color: '#6CBDFF',
    marginTop: 20,
  },
  videoimg: {
    width: 6,
    height: 10,
    marginLeft: 5,
    marginTop: 25,
  },
  touch: {
    width: '30.4%',
    height: 90,
    borderRadius: 10,
    marginVertical: 10,
    marginHorizontal: 5,
  },
  images: {
    width: 35,
    height: 35,
    alignSelf: 'center',
    marginTop: 15,
  },
  title: {
    fontFamily: 'Avenir-Heavy',
    fontWeight: 'bold',
    fontSize: 10,
    color: '#454545',
    textAlign: 'center',
    marginTop: 8,
  },
});
