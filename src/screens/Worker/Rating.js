import CheckBox from '@react-native-community/checkbox';
import React, {useState} from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  TouchableOpacity,
  TextInput,
} from 'react-native';
import {FlatList, ScrollView} from 'react-native-gesture-handler';
import AppHeader from '../../Custom/CustomAppHeader';
import {StatusBarDark} from '../../Custom/CustomStatusBar';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {ButtonStyle} from '../../Custom/CustomView';

export default function Rating({navigation}) {
  const [feedbackValue, setFeedbackValue] = useState('');
  const [rating, setRating] = useState(2);
  const [maxrating, setmaxRating] = useState([1, 2, 3, 4, 5]);
  const [perfectForYouData, setPerfectForYouData] = useState([
    {jobTitle: 'Service Pricing', selectJob: false},
    {jobTitle: 'Service Quality', selectJob: false},
    {jobTitle: 'Behavior', selectJob: false},
    {jobTitle: 'Cleanliness', selectJob: false},
    {jobTitle: 'Punctuality', selectJob: false},
    {jobTitle: 'Skills', selectJob: false},
  ]);

  const starImgFilled =
    'https://github.com/tranhonghan/images/blob/main/star_filled.png?raw=true';
  const starImgCorner =
    'https://github.com/tranhonghan/images/blob/main/star_corner.png?raw=true';

  const CustomRating = () => {
    return (
      <View style={styles.customRatingStyle}>
        {maxrating.map((item, key) => {
          return (
            <TouchableOpacity
              activeOpacity={0.7}
              key={item}
              onPress={() => setRating(item)}>
              <Image
                style={styles.starImgStyle}
                source={
                  item <= rating ? {uri: starImgFilled} : {uri: starImgCorner}
                }
              />
            </TouchableOpacity>
          );
        })}
      </View>
    );
  };

  const selectJob = index => {
    let teamData = [...perfectForYouData];
    teamData[index].selectJob = !teamData[index].selectJob;
    setPerfectForYouData(teamData);
  };

  const perfectJobList = ({item, index}) => {
    return (
      <View style={styles.jobTitleViewOffCss}>
        <Text style={styles.jobTitleTextOffCss}>{item.jobTitle}</Text>
        <CheckBox
          disabled={false}
          value={item.selectJob}
          onValueChange={newValue => {
            selectJob(index);
          }}
          tintColors={{
            true: '#F2AD4B',
            false: 'grey',
          }}
        />
      </View>
    );
  };

  return (
    <View style={styles.topview}>
      <StatusBarDark />
      <AppHeader
        elevation={0.1}
        backOnClick={() => {
          navigation.goBack();
        }}
        backIcon={require('../../images/e-remove.png')}
        title={'Rating'}
      />
      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={styles.container}>
          <View style={styles.userImgViewOffCss}>
            <Image
              style={styles.userImgOffCss}
              source={require('../../images/pic.png')}
            />
          </View>
          <Text style={styles.userNameTextOffCss}>Pramod Kumar</Text>
          <Text style={styles.containersubtext}>Plumber</Text>
        </View>
        <View style={{paddingVertical: 30}}>
          <Text style={styles.rateExpriTextOffCss}>Rate Your Experience</Text>
          <Text style={styles.tellExpriTextOffCss}>
            Tell us how was your experience
          </Text>
          <CustomRating />
        </View>
        <View style={{paddingHorizontal: 20}}>
          <Text style={styles.feedbackTextOffCss}>
            What went perfect for you?
          </Text>
          <View style={{paddingVertical: 5}}>
            <FlatList
              data={perfectForYouData}
              nestedScrollEnabled={true}
              renderItem={perfectJobList}
              showsVerticalScrollIndicator={false}
            />
          </View>
          <Text style={[styles.feedbackTextOffCss, {paddingVertical: 5}]}>
            Write your feedback:
          </Text>
          <View style={styles.boxes}>
            <TextInput
              placeholder=""
              multiline={true}
              value={feedbackValue}
              style={styles.textinput}
              onChangeText={text => {
                setFeedbackValue(text);
              }}
            />
          </View>
          <View style={{marginVertical: 20}}>
            <ButtonStyle
              height={hp(7)}
              title={'SUBMIT'}
              marginHorizontal={1}
              onPress={() => {
                navigation.navigate('WorkerHome');
              }}
            />
          </View>
        </View>
      </ScrollView>
    </View>
  );
}
const styles = StyleSheet.create({
  topview: {
    backgroundColor: '#fff',
    flex: 1,
  },
  container: {
    paddingVertical: 25,
    backgroundColor: '#F2AD4B',
    justifyContent: 'center',
    alignItems: 'center',
  },
  userImgViewOffCss: {
    height: hp(11.7),
    width: wp(24),
    borderRadius: wp(24) / 2,
    borderWidth: 2,
    borderColor: '#FFF',
    justifyContent: 'center',
    alignItems: 'center',
  },
  userImgOffCss: {
    height: '100%',
    width: '100%',
    resizeMode: 'contain',
  },
  userNameTextOffCss: {
    fontFamily: 'Avenir-Heavy',
    fontSize: hp(3),
    fontWeight: '900',
    color: '#fff',
    paddingVertical: 5,
    letterSpacing: 0.7,
  },
  containersubtext: {
    fontFamily: 'Avenir-Medium',
    fontWeight: '500',
    fontSize: hp(2.25),
    color: '#fff',
  },
  rateExpriTextOffCss: {
    fontFamily: 'Avenir-Heavy',
    fontSize: hp(2.75),
    fontWeight: '900',
    textAlign: 'center',
    color: '#000000',
  },
  tellExpriTextOffCss: {
    fontFamily: 'Avenir-Medium',
    fontWeight: '500',
    fontSize: hp(2.2),
    marginTop: 10,
    textAlign: 'center',
    color: '#333333',
    opacity: 0.6,
    marginBottom: 20,
  },
  customRatingStyle: {
    justifyContent: 'center',
    flexDirection: 'row', // marginLeft: -40,
  },
  starImgStyle: {
    height: 36,
    width: 36,
    margin: 5,
    resizeMode: 'cover',
  },
  feedbackTextOffCss: {
    fontFamily: 'Avenir-Heavy',
    fontSize: hp(2.45),
    fontWeight: '900',
    color: '#000000',
    letterSpacing: 0.5,
    marginBottom: 10,
  },
  jobTitleViewOffCss: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingVertical: 15,
    borderBottomColor: '#C8C8D3',
    borderBottomWidth: 0.3,
  },
  jobTitleTextOffCss: {
    fontFamily: 'Avenir-Medium',
    fontWeight: '500',
    fontSize: hp(2.2),
    color: '#000',
  },
  boxes: {
    height: 100,
    borderRadius: 20,
    borderWidth: 1,
    borderColor: '#BFC4D1',
    backgroundColor: '#fff',
    marginBottom: 20,
  },
  textinput: {
    fontSize: hp(2),
    color: '#000',
    fontWeight: '500',
    fontFamily: 'Avenir-Medium',
    paddingHorizontal: 15,
  },

  // sub2text: {
  //   fontFamily: 'Avenir-Heavy',
  //   fontWeight: 'bold',
  //   fontSize: 18,
  //   marginTop: 30,
  //   color: '#000000',
  //   marginHorizontal: 30,
  // },
  // subview: {
  //   backgroundColor: '#b9dcf1',
  //   height: 150,
  //   width: 360,
  //   marginTop: 120,
  //   marginHorizontal: 15,
  //   borderTopRightRadius: 40,
  //   borderTopLeftRadius: 40,
  //   flexDirection: 'row',
  // },

  // subtxt: {
  //   color: 'grey',
  //   fontWeight: 'bold',
  //   marginLeft: -10,
  // },

  // txted: {
  //   marginHorizontal: -90,
  //   marginTop: 20,
  //   borderWidth: 0.5,
  //   borderColor: 'lightgrey',
  //   borderRadius: 10,
  //   height: 310,
  //   marginLeft: -110,
  // },
  // btn: {
  //   marginTop: 400,
  //   backgroundColor: 'dodgerblue',
  //   height: 70,
  //   alignItems: 'center',
  //   justifyContent: 'center',
  //   borderRadius: 15,
  //   marginHorizontal: 20,
  // },
  // btntxt: {
  //   color: 'white',
  //   marginHorizontal: 20,
  //   fontWeight: 'bold',
  //   fontSize: 18,
  // },
  // Line: {
  //   height: 1,
  //   borderRadius: 5,
  //   backgroundColor: '#dadce0aa',
  //   marginTop: 10,
  //   marginHorizontal: 30,
  //   // marginLeft: 25,
  // },

  // touch: {
  //   padding: 15,
  //   marginHorizontal: 30,
  //   borderRadius: 25,
  //   backgroundColor: '#6CBDFF',
  //   marginTop: 30,
  //   marginBottom: 20,
  // },
  // touchtext: {
  //   fontFamily: 'Avenir-Heavy',
  //   fontSize: 16,
  //   fontWeight: 'bold',
  //   alignSelf: 'center',
  //   color: '#ffffff',
  // },
});
