import React, {useEffect, useState} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  Image,
  Dimensions,
  TouchableOpacity,
  LogBox,
} from 'react-native';
import {StatusBarDark} from '../Custom/CustomStatusBar';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {_SetAuthToken, _RemoveAuthToken} from '../services/ApiSauce';
import {LocalStorage} from '../services/Api';
import store from '../redux/store';
import {PermissionsAndroid} from 'react-native';
import * as actions from '../redux/actions';
import {useDispatch, useStore} from 'react-redux';

LogBox.ignoreAllLogs();

const Splash = ({navigation}) => {
  const dispatch = useDispatch();
  const screenHandler = async () => {
    const user_id = JSON.parse(await LocalStorage.getUserId()) || '';
    dispatch(actions.SetUser_id(user_id));
    const userType = JSON.parse(await LocalStorage.getUserTypes()) || '';
    dispatch(actions.SetUserType(userType));
    const token = (await LocalStorage.getToken()) || '';
    // console.log(JSON.stringify(token));
    // alert(JSON.stringify(token));
    if (token.length !== 0) {
      _SetAuthToken(token);
      if (userType == 3) {
        navigation.replace('TabNavigator');
      } else if (userType == 2) {
        navigation.replace('HomeContractor');
      } else if (userType == 1) {
        navigation.replace('WorkerHome');
      }
    } else {
      _RemoveAuthToken();
      LocalStorage.setToken('');
      navigation.replace('Language');
    }
  };
  useEffect(() => {
    permissionHandlers();
  }, []);

  const permissionHandlers = async () => {
    const granted = await PermissionsAndroid.requestMultiple([
      PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
    ]);
    if (
      granted['android.permission.ACCESS_FINE_LOCATION'] ===
      PermissionsAndroid.RESULTS.GRANTED
    ) {
      screenHandler();
      console.log('You can use the cameras & mic');
    } else {
      console.log('Permission denied');
    }
  };

  // useEffect(() => {
  //   setTimeout(() => {
  //     navigation.reset({
  //       index: 0,
  //       routes: [{name: 'Language'}],
  //     });
  //   }, 3000);
  // }, [navigation]);
  return (
    <View style={styles.container}>
      <StatusBarDark bg={'#FFFFFF'} />
      <Image style={styles.image} source={require('../images/logo.png')} />
    </View>
  );
};

export default Splash;

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#FFFFFF',
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
  },
  image: {
    width: wp(61), // 219,
    height: hp(50), // 232,
    resizeMode: 'contain',
  },
});
