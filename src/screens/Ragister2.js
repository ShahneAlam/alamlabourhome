import React, {useEffect, useState, useCallback, useRef} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  Image,
  Dimensions,
  TouchableOpacity,
  TextInput,
  FlatList,
} from 'react-native';
import {StatusBarDark} from '../Custom/CustomStatusBar';
import {Dropdown} from 'react-native-material-dropdown-v2-fixed';
import InputView from '../Custom/CustomTextInput';
import {RadioButton} from 'react-native-paper';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {ButtonStyle} from '../Custom/CustomView';
import CheckBox from '@react-native-community/checkbox';
import stringsoflanguages from '../language';
import {_SetAuthToken} from '../services/ApiSauce';
import DeviceInfo from 'react-native-device-info';

import {useDispatch, useSelector} from 'react-redux';
import store from '../redux/store';
import {LocalStorage, Api} from '../services/Api';
import * as actions from '../redux/actions';
import * as EmailValidator from 'email-validator';

let selectJob = [
  {value: 'I am a worker'},
  {value: 'I am a contractor'},
  {value: 'I am a customer'},
];

const Ragister2 = ({navigation, route}) => {
  const dispatch = useDispatch();
  const {_ragister} = stringsoflanguages;
  const {phone, user_id, deviceInfo} = useSelector(store => store);

  const onChangeDropdownUserType = (value, index, data) => {
    setUserType(data[index].value);
    console.log('-----setUserType: ', JSON.stringify(data[index].value));
  };

  const [state, setState] = useState({
    user_type: 3,
    mobile: phone,
    name: '',
    company_name: '',
    email: '',
    term: false,
  });

  const registerHandleCustomerrPress = async () => {
    const {user_type, mobile, term, name, company_name, email} = state;

    if (name == '') {
      alert('Please ! Enter Your Name');
      return;
    }
    if (term == false) {
      alert('Please Check Term and Condition');
      return;
    }
    if (email == '') {
      alert('Please ! Enter Your Email');
      return;
    }
    if (email.length != 0) {
      if (EmailValidator.validate(email) == false) {
        alert('Please Enter Valid Email');
        return;
      }
    }

    const body = {
      user_id: user_id,
      user_type: user_type,
      mobile: mobile,
      name: name,
      company_name: company_name,
      email: email,
      device_id: DeviceInfo.getUniqueId(),
      device_token: 'static',
      device_type: Platform.OS,
      model_name: DeviceInfo.getModel(),
    };
    // alert(JSON.stringify(body, null, 2));
    // return;
    const response = await Api.customerRegisterApi(body);
    const {status = false, token, user_detail} = response;
    if (status == true) {
      _SetAuthToken(token);
      LocalStorage.setToken(token);
      LocalStorage.setUserTypes(user_detail.user_type);
      navigation.replace('TabNavigator');
      dispatch(actions.SetUserDetail(user_detail));
    } else {
      alert('Something went wrong');
    }
  };
  useEffect(() => {
    // alert(JSON.stringify(user_id));
  });
  return (
    <View style={styles.continer}>
      <View style={{flex: 1, paddingVertical: 0}}>
        <View style={{marginVertical: 5}}>
          <InputView
            keyboardType="default"
            placeholder={_ragister.name}
            returnKeyType={'next'}
            onChangeText={name => setState({...state, name})}
            value={state.name}
          />
        </View>
        <View style={{marginVertical: 5}}>
          <InputView
            keyboardType="default"
            placeholder={_ragister.companyName}
            returnKeyType={'next'}
            //   getFocus={() => input_companyname.current.focus()}
            onChangeText={company_name => setState({...state, company_name})}
            value={state.company_name}
            // setFocus={input_name}
          />
        </View>
        <View style={{marginVertical: 5}}>
          <InputView
            keyboardType="default"
            placeholder={_ragister.emailAddress}
            returnKeyType={'next'}
            onChangeText={email => setState({...state, email})}
            value={state.email}
          />
        </View>

        <View style={{flexDirection: 'row', width: wp(90), marginVertical: 10}}>
          <View style={{width: wp(8)}}>
            <CheckBox
              disabled={false}
              value={state.term}
              onValueChange={term => setState({...state, term})}
              tintColors={{true: '#F5B04C', false: 'grey'}}
            />
          </View>
          <View style={{width: wp(82)}}>
            <Text style={[styles.agreekatlegoOffCss, {paddingVertical: 3}]}>
              {_ragister.bySigning}
              <Text style={styles.privacyOffCss}>
                {' '}
                {_ragister.termCondition}{' '}
              </Text>
              <Text style={styles.agreekatlegoOffCss}>{_ragister.and}</Text>
              <Text style={styles.privacyOffCss}>
                {' '}
                {_ragister.privacyPolicy}
              </Text>
            </Text>
          </View>
        </View>
        <View style={{marginVertical: 10}}>
          <ButtonStyle
            title={_ragister.register}
            marginHorizontal={1}
            // onPress={() => {
            //   navigation.navigate('TabNavigator');
            // }}
            onPress={() => registerHandleCustomerrPress()}
          />
        </View>
      </View>
    </View>
  );
};

export default Ragister2;

const styles = StyleSheet.create({
  continer: {
    flex: 1,
    backgroundColor: '#F4F4F4',
    // paddingHorizontal: 20,
  },
  text: {
    fontFamily: 'Avenir',
    fontSize: hp(4.1), // 30,
    fontWeight: 'bold',
    color: '#454545',
    paddingVertical: 10,
  },
  subtext: {
    fontFamily: 'Avenir-Medium',
    fontSize: hp(1.9), // 14,
    fontWeight: '500',
    color: '#606E87',
    lineHeight: 20,
  },
  drops: {
    height: hp(8.1), // 60,
    backgroundColor: '#fff',
    borderRadius: 15,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    borderBottomWidth: 0,
    paddingHorizontal: 20,
    // elevation: 2,
  },
  tradeTextOffCss: {
    fontSize: hp(2.1),
    fontWeight: '500',
    color: '#7A7A7A',
    fontFamily: 'Avenir-Medium',
  },
  dropIconOffCss: {
    width: wp(7),
    height: hp(5),
    resizeMode: 'contain',
    transform: [{rotate: '270deg'}],
  },
  textinput: {
    height: hp(8.1), // 60,
    width: 330,
    borderRadius: 20,
    backgroundColor: '#fff',
    elevation: 2,
    // marginTop: 40,
    // marginHorizontal: 30,
    padding: 20,
  },
  workLocationViewOffCss: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginVertical: 5,
  },
  workLocaTextOffCss: {
    fontSize: hp(2.2),
    fontFamily: 'Avenir-Heavy',
    fontWeight: 'bold',
    color: '#454545',
  },
  addLocaViewOffCss: {
    width: wp(25),
    height: hp(4),
    backgroundColor: '#F2AD4B',
    borderRadius: 8,
    justifyContent: 'center',
    alignItems: 'center',
  },
  addNewTextOffCss: {
    fontSize: hp(1.6),
    fontFamily: 'Avenir-Heavy',
    fontWeight: 'bold',
    color: '#fff',
  },
  selectAddViewOffCss: {
    backgroundColor: '#fff',
    paddingHorizontal: 10,
    flexDirection: 'row',
    paddingVertical: 10,
    borderRadius: 15,
    marginVertical: 5,
  },
  addTitleTextOffCss: {
    fontSize: hp(2.1),
    fontFamily: 'Avenir-Heavy',
    fontWeight: '900',
    color: '#080040',
    marginBottom: 5,
  },
  addAddressTextOffCss: {
    fontSize: hp(1.7),
    fontFamily: 'Avenir-Meduim',
    fontWeight: '500',
    color: '#8A94A3',
    marginBottom: 5,
  },
  agreekatlegoOffCss: {
    color: '#2D2627',
    fontSize: hp(1.75),
    fontFamily: 'Avenir-Meduim',
    paddingHorizontal: 2,
    letterSpacing: 0.5,
  },
  privacyOffCss: {
    color: '#6CBDFF',
    paddingHorizontal: 5,
    fontSize: hp(1.7),
    fontFamily: 'Avenir-Meduim',
    letterSpacing: 0.5,
  },
});
