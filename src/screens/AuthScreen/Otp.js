import React, {useEffect, useState} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  Image,
  Dimensions,
  TouchableOpacity,
  TextInput,
} from 'react-native';
import {StatusBarDark} from '../../Custom/CustomStatusBar';
import OTPInputView from '@twotalltotems/react-native-otp-input';
import {ButtonStyle} from '../../Custom/CustomView';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import stringsoflanguages from '../../language';
import {useDispatch, useStore} from 'react-redux';
import {LocalStorage, Api} from '../../services/Api';
import {act} from 'react-test-renderer';
import * as actions from '../../redux/actions';
import Toast from 'react-native-simple-toast';
import {_SetAuthToken} from '../../services/ApiSauce';
import Loader from '../../screens/AuthScreen/Loader';

const Otp = ({navigation, route}) => {
  const dispatch = useDispatch();
  const {_otp} = stringsoflanguages;
  const [state, setState] = useState({
    isLoading: false,
    otp: '',
    mobile: '',
    code: '',
    user_type: route.params.user_type,
    user_id: route.params.user_id,
  });

  const toggleLoader = isLoading => setState({...state, isLoading});

  const strData = JSON.stringify(route.params.otp);
  const verifyOtpHandler = async otp => {
    const {mobile} = route.params;
    if (state.code.length != 4) {
      alert('Please Enter Four Digit OTP');
      return;
    }
    if (state.code != route.params.otp) {
      alert('Please Enter Valid OTP');
      return;
    }
    const body = {
      otp,
      mobile: route.params.mobile,
    };
    toggleLoader(false);
    const response = await Api.verfiyOtpApi(body);
    toggleLoader(true);
    const {status = false, msg, token, user_id} = response;
    if (status) {
      _SetAuthToken(token);
      LocalStorage.setToken(token);
      LocalStorage.setUserId(user_id);
      dispatch(actions.SetUser_id(user_id));
      dispatch(actions.SetUser(response));
      setTimeout(
        function () {
          if (state.user_type == 0) {
            navigation.replace('Ragister');
          } else if (state.user_type != 0) {
            if (state.user_type == 1) {
              navigation.replace('ProfileData');
            } else if (state.user_type == 2) {
              navigation.replace('HomeContractor');
            } else if (state.user_type == 3) {
              navigation.replace('UserRegisterData');
            }
          }
        }.bind(this),
        1000,
      );

      // navigation.replace('Ragister');
    } else {
      alert('verify error');
    }
  };
  useEffect(() => {
    alert(JSON.stringify(route.params.otp));
    //  Toast.showWithGravity('Your Otp is :-', Toast.LONG, Toast.TOP);
    //  alert(JSON.stringify(state.user_id));
  }, []);
  return (
    <View style={{backgroundColor: '#F4F4F4', flex: 1}}>
      <Loader status={state.isLoading} />
      <StatusBarDark />
      <ScrollView>
        <Image style={styles.image} source={require('../../images/otp.png')} />
        <Text style={styles.text}>{_otp.title}</Text>
        <Text style={styles.subtext}>
          {_otp.subtitle}
          {'\n'} {route.params.mobile}
        </Text>
        <OTPInputView
          style={styles.otpInput}
          pinCount={4}
          code={state.code}
          onCodeChanged={code => setState({...state, code})}
          // onCodeChanged={text => {
          //   setOtp(text.replace(/[^0-9]/g, ''));
          //   console.log('--------otp: ', +text);
          // }}
          autoFocusOnLoad
          codeInputFieldStyle={styles.underlineStyleBase}
          codeInputHighlightStyle={styles.underlineStyleHighLighted}
        />

        <View style={{flexDirection: 'row', justifyContent: 'center'}}>
          <Text style={styles.sub3text}>{_otp.recieveCode}</Text>
          <TouchableOpacity>
            <Text style={styles.sub4text}> {_otp.resendOtp}</Text>
          </TouchableOpacity>
        </View>

        <View style={{marginVertical: 30}}>
          <ButtonStyle
            title={_otp.verify}
            marginHorizontal={30}
            onPress={() => verifyOtpHandler()}
            //  onPress={() => navigation.navigate('Ragister')}
          />
        </View>
      </ScrollView>
    </View>
  );
};

export default Otp;

const styles = StyleSheet.create({
  image: {
    marginTop: hp(10),
    width: wp(42), // 150,
    height: hp(20), // 135,
    resizeMode: 'contain',
    alignSelf: 'center',
  },
  text: {
    fontFamily: 'Avenir',
    fontSize: hp(4.1), // 30,
    fontWeight: '900',
    color: '#454545',
    marginTop: 60,
    marginHorizontal: 30,
  },
  subtext: {
    fontFamily: 'Avenir-Medium',
    fontSize: hp(1.8), // 14,
    fontWeight: '500',
    color: '#606E87',
    marginTop: 10,
    marginHorizontal: 30,
    lineHeight: 20,
  },
  otpInput: {
    height: 60,
    marginVertical: 20,
    marginHorizontal: 30,
  },
  underlineStyleBase: {
    width: 60,
    height: 60,
    borderWidth: 1,
    borderColor: '#f5f5f5',
    backgroundColor: '#fff',
    borderRadius: 20,
    fontFamily: 'Avenir-Heavy',
    fontSize: 24,
    fontWeight: 'bold',
    color: '#100C08',
  },
  underlineStyleHighLighted: {
    width: 60,
    height: 60,
    borderRadius: 20,
    fontFamily: 'Avenir-Heavy',
    fontSize: 24,
    fontWeight: 'bold',
    color: 'white',
    backgroundColor: '#fff',
  },
  sub3text: {
    fontFamily: 'Avenir-Medium',
    fontWeight: '500',
    fontSize: hp(2.07), // 15,
    color: '#606E87',
    opacity: 0.6,
  },
  sub4text: {
    fontFamily: 'Avenir-Medium',
    fontWeight: '500',
    fontSize: hp(1.9), //14,
    color: '#6CBDFF',
  },
});
