import React, {useEffect, useState} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  Image,
  Dimensions,
  TouchableOpacity,
  Platform,
  TextInput,
} from 'react-native';
import stringsoflanguages from '../../language';
import {StatusBarDark} from '../../Custom/CustomStatusBar';
import InputView from '../../Custom/CustomTextInput';
import {BottomView, ButtonStyle} from '../../Custom/CustomView';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {Api, LocalStorage} from '../../services/Api';
import DeviceInfo from 'react-native-device-info';
import * as actions from '../../redux/actions';
import {useDispatch, useStore} from 'react-redux';
import Geolocation from 'react-native-geolocation-service';
import {useIsFocused} from '@react-navigation/native';
import Loader from '../../screens/AuthScreen/Loader';

const Login = ({navigation}) => {
  const isFocused = useIsFocused();
  const dispatch = useDispatch();
  const [state, setState] = useState({
    mobile: '',
    isLoading: false,
  });

  useEffect(() => {
    userAndUserType();
  }, [isFocused]);
  const toggleLoader = isLoading => setState({...state, isLoading});
  const userAndUserType = async () => {
    const user_id = JSON.parse(await LocalStorage.getUserId()) || '';
    dispatch(actions.SetUser_id(user_id));

    const userType = JSON.parse(await LocalStorage.getUserTypes()) || '';
    dispatch(actions.SetUserType(userType));
  };

  useEffect(() => {
    Geolocation.getCurrentPosition(
      position => {
        dispatch(actions.SetCoords(position.coords));
        // alert(JSON.stringify(position.coords, null, 2));
        console.log(position);
      },
      error => {
        // See error code charts below.
        console.log(error.code, error.message);
      },
      {
        enableHighAccuracy: false,
        timeout: 20000,
        maximumAge: 10000,
        forceLocationManager: false,
      },
    );

    Geolocation.watchPosition(
      position => {
        // alert(position.coords.latitude);
        // this.setState({
        // latitude: position.coords.latitude,
        // longitude: position.coords.longitude,
        // coordinates: this.state.coordinates.concat({
        // latitude: position.coords.latitude,
        // longitude: position.coords.longitude
        // })
        // });
      },
      error => {
        console.log(error);
      },
      {
        showLocationDialog: true,
        enableHighAccuracy: true,
        timeout: 20000,
        maximumAge: 0,
        distanceFilter: 0,
      },
    );
  }, []);
  useEffect(() => {
    // getCurrentPosition();
  }, [navigation]);

  const getCurrentPosition = () => {
    Geolocation.getCurrentPosition(
      position => {
        // var lat = parseFloat(position.coords.latitude);
        // var long = parseFloat(position.coords.longitude);
        dispatch(actions.SetCoords(position));

        // alert(JSON.stringify(position));
        console.log('-------getCurrentPosition :' + JSON.stringify(position));
      },
      error => {
        console.log('-------error :' + JSON.stringify(error));
      },
      {enableHighAccuracy: true, timeout: 20000, maximumAge: 3600000},
    );
  };
  const loginButtonPress = async () => {
    const {mobile} = state;

    if (mobile.length !== 10) {
      alert('Please Enter Valid Mobile Number');
      return;
    }
    const body = {
      mobile: state.mobile,
      device_id: DeviceInfo.getUniqueId(),
      device_token: 'byFirebase',
      device_type: Platform.OS,
      loginTime: 'left',
      model_name: DeviceInfo.getModel(),
      carrier_name: DeviceInfo.getModel(),
      device_country: 'cntry',
      device_memory: 'memory',
      have_notch: 'have  otch',
      manufacture: 'manufacture',
    };
    toggleLoader(false);
    const response = await Api.LoginApi(body);
    const {
      status = false,
      user_id = '',
      flag = 0,
      otp,
      user_type,
      token,
    } = response;
    toggleLoader(true);
    // alert(JSON.stringify(response, null, 2));
    //  console.log(JSON.stringify(response, null, 2));

    if (status && user_id !== '') {
      LocalStorage.setUserTypes(user_type);
      actions.Phone(mobile);
      dispatch(actions.SetUser_id(user_id));
      LocalStorage.setUserId(user_id);
      setTimeout(
        function () {
          navigation.replace('Otp', {user_id, user_type, mobile, otp});
        }.bind(this),
        1000,
      );
    } else {
      alert('Something went wrong');
    }
  };

  const {_login} = stringsoflanguages;
  return (
    <View style={{backgroundColor: '#F4F4F4', flex: 1}}>
      <Loader status={state.isLoading} />
      <StatusBarDark />
      <ScrollView>
        <Image style={styles.image} source={require('../../images/logo.png')} />
        <Text style={styles.text}>{_login.title}</Text>
        <Text style={styles.subtext}>{_login.subtitle}</Text>
        <View style={{marginHorizontal: 30}}>
          <InputView
            maxLength={10}
            keyboardType="numeric"
            placeholder={_login.mobile}
            onChangeText={mobile => setState({...state, mobile})}
            value={state.mobile}
          />
        </View>
        <BottomView />
        <ButtonStyle
          title={_login.continue}
          marginHorizontal={30}
          onPress={() => loginButtonPress()}
          // onPress={() => {
          //   navigation.navigate('Otp');
          // }}
        />
        <Text style={styles.sub2text}>{_login.or}</Text>

        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-evenly',
            padding: 10,
            marginHorizontal: 80,
          }}>
          <TouchableOpacity style={styles.facebook}>
            <Image style={styles.fb} source={require('../../images/fb.png')} />
          </TouchableOpacity>

          <TouchableOpacity style={styles.facebook}>
            <Image
              style={styles.google}
              source={require('../../images/g.png')}
            />
          </TouchableOpacity>
        </View>
      </ScrollView>
    </View>
  );
};

export default Login;

const styles = StyleSheet.create({
  image: {
    marginTop: hp(11),
    width: wp(42), // 150,
    height: hp(20), // 135,
    resizeMode: 'contain',
    alignSelf: 'center',
  },
  text: {
    fontFamily: 'Avenir',
    fontSize: hp(4.1), // 30,
    fontWeight: '900',
    color: '#454545',
    marginTop: 60,
    marginHorizontal: 30,
  },
  subtext: {
    fontFamily: 'Avenir-Medium',
    fontSize: hp(1.9), // 14,
    fontWeight: '500',
    color: '#606E87',
    marginTop: 10,
    marginHorizontal: 30,
    marginBottom: 20,
  },
  sub2text: {
    fontFamily: 'Avenir-Medium',
    fontSize: hp(1.9), // 14,
    fontWeight: '500',
    color: '#454545',
    marginTop: 30,
    textAlign: 'center',
  },
  facebook: {
    width: wp(17), // 60,
    height: 60,
    borderRadius: 20,
    backgroundColor: '#fff',
    elevation: 2,
    alignSelf: 'center',
    marginTop: 20,
  },
  fb: {
    width: 20,
    height: 40,
    alignSelf: 'center',
    marginTop: 10,
  },
  google: {
    width: 35,
    height: 40,
    alignSelf: 'center',
    marginTop: 10,
  },
});
