const data = {
  userDetail: {},
  user_id: '',
  user: {},
  coords: {},
  latitudeLocation: '',
  longitudeLocation: '',
  addressLocation: '',
  phone: '',
  trade_id_worker: '',
  deviceInfo: {
    id: '',
    token: '',
    model: '',
    os: '',
  },
  userType: '',
  threeSelect: '',
  ftoken: '',
  customerHomeJob: '',
  subscriptionPlan: '',
  activeSubscriptionPlan: '',
  videoTutorial: '',

  netInfo: {
    details: {},
    isConnected: false,
    isInternetReachable: false,
    isWifiEnabled: false,
    type: '',
  },
  language: 'en',
};
const reducer = (state = data, action) => {
  switch (action.type) {
    case 'setUserDetail':
      return {
        ...state,
        userDetail: action.payload,
        isLogin: true,
      };
    case 'language':
      return {...state, language: action.payload};
    case 'setDeviceInfo':
      return {
        ...state,
        deviceInfo: action.payload,
      };
    case 'phone':
      return {
        ...state,
        phone: action.payload,
      };
    case 'customerHomeJob':
      return {
        ...state,
        customerHomeJob: action.payload,
      };
    case 'subscriptionPlan':
      return {
        ...state,
        subscriptionPlan: action.payload,
      };
    case 'activeSubscriptionPlan':
      return {
        ...state,
        activeSubscriptionPlan: action.payload,
      };
    case 'videoTutorial':
      return {
        ...state,
        videoTutorial: action.payload,
      };
    case 'ftoken':
      return {
        ...state,
        ftoken: action.paylod,
      };
    case 'setNetInfo':
      return {
        ...state,
        netInfo: action.payload,
      };
    case 'user_id':
      return {
        ...state,
        user_id: action.payload,
      };

    case 'trade_id_worker':
      return {
        ...state,
        trade_id_worker: action.payload,
      };
    case 'coords':
      return {
        ...state,
        coords: action.payload,
      };
    case 'addressLocation':
      return {
        ...state,
        addressLocation: action.payload,
      };
    case 'latitudeLocation':
      return {
        ...state,
        latitudeLocation: action.payload,
      };
    case 'longitudeLocation':
      return {
        ...state,
        longitudeLocation: action.payload,
      };
    case 'userType':
      return {
        ...state,
        userType: action.payload,
      };

    default:
      return state;
  }
};
export default reducer;
