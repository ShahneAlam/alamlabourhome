import store from '../redux/store';
export const Token = user =>
  store.dispatch({type: 'token', paylod: user ? user : ''});
export const Phone = payload => store.dispatch({type: 'phone', payload});
export const Firebase = user =>
  store.dispatch({type: 'ftoken', paylod: user ? user : ''});
export const SetUser = payload => ({type: 'user', payload});
export const SetDeviceInfo = deviceInfo => ({
  type: 'SetDeviceInfo',
  payload: deviceInfo,
});
export const SetUserDetail = payload => ({type: 'setUserDetail', payload});
export const Language = payload => ({type: 'language', payload});
export const CustomerHomeJob = payload => ({
  type: 'customerHomeJob',
  payload,
});
export const SubscriptionPlanCustomer = payload => ({
  type: 'subscriptionPlan',
  payload,
});
export const activeSubscriptionPlanCustomer = payload => ({
  type: 'activeSubscription',
  payload,
});
export const VideoTutorialCustomer = payload => ({
  type: 'videoTutorial',
  payload,
});
export const SetNetInfo = netInfo => ({type: 'setNetInfo', payload: netInfo});
export const SetUser_id = netInfo => ({type: 'user_id', payload: netInfo});

export const TradeIdWorker = payload => ({
  type: 'trade_id_worker',
  payload,
});

export const SetCoords = coords => ({type: 'coords', payload: coords});
export const AddressLocation = addressLocation => ({
  type: 'addressLocation',
  payload: addressLocation,
});
export const LatitudeLocation = latitudeLocation => ({
  type: 'latitudeLocation',
  payload: latitudeLocation,
});
export const LongitudeLocation = longitudeLocation => ({
  type: 'longitudeLocation',
  payload: longitudeLocation,
});
export const SetUserType = userType => ({
  type: 'userType',
  payload: userType,
});
