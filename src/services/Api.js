import AsyncStorage from '@react-native-async-storage/async-storage';
import {request, requestGet, requestMultipart} from './ApiSauce';
import {Dimensions} from 'react-native';
const {width, height} = Dimensions.get('window');

const AspectRatio = () => width / height;

const Api = {
  LoginApi: json => request('/login', json),
  verfiyOtpApi: json => request('/verfiyOtp', json),
  ContractorRegisterApi: json => request('/contractorRegister', json),
  WorkerRegisterApi: json => request('/workerRegister', json),

  customerRegisterApi: json => request('/customerRegister', json),
  getTradeApi: json => requestGet('/getTrade', json),
  getProjectApi: json => requestGet('/getProject', json),
  GetWorkerApi: json => requestGet('/getWorker', json),

  HomepageCustomerApi: json => requestGet('/homepage_customer', json),

  AddAddressApi: json => request('/add_address', json),
  GetAddressesApi: json => request('/get_addresses', json),
  GetjobApi: json => request('/getjob', json),

  GetDailyJobViewAllApi: json => requestGet('/get_MasterTrade', json),
  BookingCustomerApi: json => request('/booking', json),
  GetUserOngoingBookingApi: json => request('/get_user_ongoing_booking', json),
  GetUserCompleteBookingApi: json =>
    request('/get_user_complete_booking', json),
  SwitchButtonWorkerApi: json => request('/online_status_update', json),
  WorkerHomeApi: json => requestGet('/homepage_worker', json),
  AcceptButtonWorkerApi: json => request('/booking_update', json),
  OngoingJobsWorkerApi: json => request('/get_worker_booking', json),
  CompletedJobsWorkerApi: json => request('/get_worker_complete_booking', json),

  GetWorkerRegisterDataApi: json => request('/get_worker_profile', json),
  GetUserProfileRegisterDataApi: json => request('/get_user_profile', json),

  LogoutApi: json => requestGet('/logout', json),
  SettingsApi: json => requestGet('/fetch_settings', json),

  WorkerHelpApi: json => requestGet('/help', json),
  SearchlistsUserApi: json => request('/search_lists', json),

  CallBookingRecordingWorkerApi: json => request('/call_booking', json),
  MyBookingOtpMatchApi: json => request('/booking_otp_match ', json),
};

const LocalStorage = {
  setToken: token => AsyncStorage.setItem('Authorization', token),
  getToken: () => AsyncStorage.getItem('Authorization'),
  setLanguage: language => AsyncStorage.setItem('language', language),
  getLanguage: () => AsyncStorage.getItem('language'),
  setUserTypes: userType =>
    AsyncStorage.setItem('userType', JSON.stringify(userType)),
  getUserTypes: () => AsyncStorage.getItem('userType'),
  setFirstTimeOpen: () => AsyncStorage.setItem('firstTimeOpen', 'false'),
  getFirstTimeOpen: () => AsyncStorage.getItem('firstTimeOpen'),
  setSelectTrade: selectTrade =>
    AsyncStorage.setItem('selectTrade', selectTrade),
  getSelectTrade: () => AsyncStorage.getItem('selectTrade'),

  setSelectUserType: selectUserType =>
    AsyncStorage.setItem('selectUserType', selectUserType),
  getSelectUserType: () => AsyncStorage.getItem('selectUserType'),

  setSelectTradeWorker: selectTradeWorker =>
    AsyncStorage.setItem('selectTradeWorker', selectTradeWorker),
  getSelectTradeWorker: () => AsyncStorage.getItem('selectTradeWorker'),

  setUserId: user_id =>
    AsyncStorage.setItem('user_id', JSON.stringify(user_id)),
  getUserId: () => AsyncStorage.getItem('user_id'),

  setUser_Type: user_Type =>
    AsyncStorage.setItem('user_id', JSON.stringify(user_Type)),
  getUserType: () => AsyncStorage.getItem('user_Type'),
  clear: AsyncStorage.clear,
};

//const onSearchEvent = new Subject().pipe(debounceTime(500));

export {width, height, Api, LocalStorage};
