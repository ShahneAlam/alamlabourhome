import React, { useState } from 'react';
import { StyleSheet, View, TouchableOpacity, Image, TextInput } from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

function InputView(navigation) {

    const {
        placeholder, keyboardType, returnKeyType, maxLength, value, onChangeText, getFocus, leftIconClick, selectInputImage,
        editable, setFocus, width, height, multiline, textAlignVertical, eyeIconShow = false, imageEye, secureTextEntry
    } = navigation
    return (
        <View style={styles.inputViewOffCss}>
            {/* <View style={{flex: 1, justifyContent: 'center',paddingHorizontal: 15 }}> */}
                <TextInput
                    style={styles.inputStyle}
                    value={value}
                    ref={setFocus}
                    autoCorrect={false}
                    editable={editable}
                    maxLength={maxLength}
                    placeholder={placeholder}
                    onSubmitEditing={getFocus}
                    onChangeText={onChangeText}
                    keyboardType={keyboardType}
                    returnKeyType={returnKeyType}
                    secureTextEntry={secureTextEntry}
                    multiline={multiline}
                    underlineColorAndroid="transparent"
                    textAlignVertical={textAlignVertical}
                    placeholderTextColor={"#7A7A7A"}
                />
            {/* </View> */}
        </View>
    );
}

const styles = StyleSheet.create({
    inputViewOffCss: {
        backgroundColor: '#FFF', height: hp(8.10),  flexDirection: 'row',
        alignItems: 'center', borderRadius: 20, marginVertical: 8, elevation: 0, paddingHorizontal: 15
    },
    inputStyle: {
        flex: 1, color: '#454545', fontSize: hp(2.20), fontFamily: 'Avenir-Medium', fontWeight: '500',
    },
    eyeIconViewCss: { paddingVertical: 13, paddingHorizontal: 10 },
})

export default InputView;