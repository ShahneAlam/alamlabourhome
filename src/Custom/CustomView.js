import React from 'react';
import {
  Dimensions,
  Image,
  ImageBackground,
  SafeAreaView,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {TextField} from 'react-native-material-textfield';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

const {height} = Dimensions.get('window');

export const MainView = props => (
  <SafeAreaView style={{backgroundColor: '#FFFFFF', flex: 1}} {...props} />
);

export const BottomView = props => (
  <View style={{marginBottom: 30}} {...props} />
);

export const ButtonStyle = props => {
  const {
    title,
    bgColor,
    txtcolor,
    marginHorizontal,
    onPress,
    height,
    fontSize,
  } = props;
  return (
    <TouchableOpacity
      activeOpacity={0.6}
      onPress={onPress}
      style={[
        styles.facebookButton,
        {
          height: height ? height : 45,
          backgroundColor: bgColor ? bgColor : '#6CBDFF',
          marginHorizontal: marginHorizontal ? marginHorizontal : 15,
        },
      ]}>
      <Text
        style={[
          styles.facebooktext,
          {
            fontSize: fontSize ? fontSize : hp(2.2),
            color: txtcolor ? txtcolor : 'white',
          },
        ]}>
        {title}
      </Text>
    </TouchableOpacity>
  );
};

// export const ButtonStyle = (
//   title,
//   bgColor = '#ED6E1E',
//   txtcolor = '#FFFFFF',
//   onPress,
// ) => (
//   <TouchableOpacity
//     activeOpacity={0.6}
//     onPress={onPress}
//     style={[styles.facebookButton, {backgroundColor: bgColor}]}>
//     <Text style={[styles.facebooktext, {color: txtcolor}]}>{title}</Text>
//   </TouchableOpacity>
// );

const styles = StyleSheet.create({
  facebookButton: {
    borderRadius: 25,
    justifyContent: 'center',
    alignItems: 'center',
    elevation: 4,
  },
  facebooktext: {
    fontFamily: 'Avenir-heavy',
    fontWeight: '900',
    alignSelf: 'center',
  },
  containerStyle: {
    width: '90%',
    borderRadius: 10,
    alignSelf: 'center',
    backgroundColor: '#1B172C',
    marginTop: 40,
  },
  inputContainerStyle: {
    marginHorizontal: 20,
    backgroundColor: '#1B172C',
  },
});

export const Header = props => (
  <View style={headerStyle.viewHeader}>
    <View style={headerStyle.flexView}>
      <TouchableOpacity style={headerStyle.touchBack} onPress={props.onPress}>
        <Image
          source={require('../images/remove.png')}
          style={headerStyle.imageBack}
        />
      </TouchableOpacity>
      <Text style={headerStyle.textTitle}>{props.title}</Text>
    </View>
  </View>
);

export const Header2 = props => (
  <View style={headerStyle.viewHeader}>
    <View style={headerStyle.flexView}>
      <TouchableOpacity style={headerStyle.touchBack1} onPress={props.onPress}>
        <Image
          // source={require('../images/remove.png')}
          style={headerStyle.imageBack}
        />
      </TouchableOpacity>
      <Text style={headerStyle.textTitle}>{props.title}</Text>
    </View>
  </View>
);

const headerStyle = StyleSheet.create({
  viewHeader: {
    width: '100%',
    backgroundColor: '#fff',
    padding: 15,
  },
  imageBack: {
    width: 12,
    height: 22,
    resizeMode: 'contain',
  },
  touchBack: {
    width: 35,
    // position: 'absolute',
    left: 0,
  },
  touchBack1: {
    width: 5,
    // position: 'absolute',
    left: 0,
  },
  textTitle: {
    fontFamily: 'Avenir-Medium',
    fontWeight: '500',
    fontSize: 20,
    color: '#454545',
    marginRight: 20,
  },
  flexView: {
    flexDirection: 'row',
    // marginTop: 10,
    marginLeft: 10,
    alignItems: 'center',
  },
});

export const HeaderLight = props => (
  <View style={subheader.viewHeader}>
    <View style={subheader.flexView}>
      <TouchableOpacity style={subheader.touchBack} onPress={props.onPress}>
        <Image
          source={require('../images/back.png')}
          style={subheader.imageBack}
        />
      </TouchableOpacity>
      <Text style={subheader.textTitle}>{props.title}</Text>
    </View>
  </View>
);

const subheader = StyleSheet.create({
  viewHeader: {
    width: '100%',
    // height: 219,
    height: hp(30),
    backgroundColor: '#6CBDFF',
    // padding: 15,
    paddingHorizontal: 10,
  },
  imageBack: {
    width: 12,
    height: 22,
    resizeMode: 'contain',
  },
  touchBack: {
    position: 'absolute',
    left: 0,
  },
  textTitle: {
    fontFamily: 'Avenir-Medium',
    fontWeight: '500',
    fontSize: 20,
    color: '#fff',
    marginHorizontal: 40,
  },
  flexView: {
    flexDirection: 'row',
    marginTop: 30,
    marginLeft: 10,
    alignItems: 'center',
  },
});

export const BottomButton = props => (
  <TouchableOpacity
    activeOpacity={0.8}
    style={bottomStyle.bottomView}
    onPress={props.onPress}>
    <Text style={bottomStyle.textTitle}>{props.bottomtitle}</Text>
  </TouchableOpacity>
);

export const bottomStyle = StyleSheet.create({
  bottomView: {
    width: '95%',
    alignSelf: 'center',
    backgroundColor: '#ED6E1E',
    borderRadius: 20,
    padding: 10,
    marginTop: 60,
    marginBottom: 10,
  },
  textTitle: {
    fontFamily: 'Avenir-Heavy',
    fontWeight: 'bold',
    fontSize: 18,
    color: '#FFFFFF',
    textAlign: 'center',
  },
});

export const CustomTextField = props => (
  <TextField
    fontSize={18}
    textColor={'#1E2432'}
    tintColor={'grey'}
    containerStyle={{
      backgroundColor: '#FFFFFF',
      marginTop: 20,
      marginHorizontal: 20,
      borderRadius: 10,
      shadowColor: '#000',
      shadowOffset: {
        width: 0,
        height: 2,
      },
      shadowOpacity: 0.23,
      shadowRadius: 2.62,
      marginBottom: 10,
      elevation: 4,
    }}
    inputContainerStyle={{marginHorizontal: 20, height: 48}}
    {...props}
  />
);
