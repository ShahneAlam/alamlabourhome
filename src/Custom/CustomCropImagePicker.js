import React from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Image } from 'react-native';
import ImagePicker from 'react-native-image-crop-picker';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

export default class CustomCropImagePicker extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {};
  }

  launchLibrary = (uploadImage, selectScreen) => {
    ImagePicker.openPicker({
      // width: 300,
      // height: 400,
      // // cropping: true,
      // mediaType: 'any',
      // showCropFrame: true,
      multiple: true,
      isVideo: true,
      compressImageQuality: 0.6,
    }).then((images) => {
      // // if (selectScreen === 'CreateEvent') {
      //   let arry = [...uploadImage];
      //   for (let index = 0; index < images.length; index++) {
      //     let temData = images[index];
      //     arry.push({
      //       uri: temData.path,
      //       name: Math.round(new Date().getTime() / 1000) + '----test.jpg',
      //       type: temData.mime,
      //     });
      //   }
      //   this.props.dataGet(arry);
      //     console.log('======>uploadImage<=====' + JSON.stringify(arry));
      // // }
      let arry = [];
      for (let index = 0; index < images.length; index++) {
        let temData = images[index];
        let type = 'jpg';
        if (temData.mime == 'video/mp4') {
          type = 'mp4';
        }
        arry.push({
          uri: temData.path,
          name:
            Math.round(new Date().getTime() / 1000) + '----test.' + type,
          type: temData.mime,
        });
      }
      this.props.dataGet(arry);
      this.forceUpdate();
    });
  };

  render() {
    const { uploadImage } = this.props;
    return (
      <TouchableOpacity
        style={styles.touchOffCss}
        onPress={() => {
          this.launchLibrary(uploadImage);
        }}>
        <Image style={styles.fileImgOffCss} source={require('../images/choose-file.png')} />
      </TouchableOpacity>
    );
  }
}
const styles = StyleSheet.create({
  touchOffCss: { justifyContent: 'center' },
  fileImgOffCss: {
    width: wp(43), height: hp(16), marginRight: 10, resizeMode: 'contain', marginVertical: 10
  }
});
